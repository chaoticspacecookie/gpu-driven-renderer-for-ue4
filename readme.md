# Custom UE4 Renderer

## Overview

The Custom UE4 Renderer is designed to enhance the rendering performance of Unreal Engine 4 by improving on-device redundant draw calls management and automating command buffer optimizations. This renderer is tailored to provide better performance and efficiency for complex scenes.

## Features

__Improved On-Device Draws Management__: Enhanced algorithms to efficiently manage draw calls on the device, reducing overhead and increasing frame rates.

__Automated Command Buffer Optimizations__: Automated processes to optimize command buffers, ensuring smooth and efficient rendering without manual intervention.

__Performance Metrics__: Improved UE4 performance tracking and analysis tools to monitor and optimize rendering performance.

### Installation

Clone the repository or download the latest release from the GitHub page.
Extract the contents of the downloaded file into the Plugins folder of your UE4 project. If the Plugins folder does not exist, create it in the root of your project directory.
### Enable the Plugin:

Open your project in Unreal Engine 4.
Navigate to Edit -> Plugins.
Find the Custom UE4 Renderer in the list and check the box to enable it.
