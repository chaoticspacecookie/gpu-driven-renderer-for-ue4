
using UnrealBuildTool;
using System.Collections.Generic;

public class UE4EditorTarget : TargetRules
{
	public UE4EditorTarget( TargetInfo Target ) : base(Target)
	{
		Type = TargetType.Editor;
// CUSTOM_RENDERER BEGIN
        bEnforceIWYU = true;
        BuildEnvironment = TargetBuildEnvironment.Shared;
		WindowsPlatform.bUseBundledDbgHelp = false;
// CUSTOM_RENDERER END
		bBuildAllModules = true;
		ExtraModuleNames.Add("UE4Game");
	}
}
