#include "CustomRendererTrafficLightGPUProfiler.h"

#if ENABLE_CUSTOM_RENDERER_PERFORMANCE_MONITORING
static TAutoConsoleVariable<int32> CVarCustomRendererTrafficLightGPUProfilerEnabled(
	TEXT("r.CustomRendererTrafficLightGPUProfilerEnabled"),
	1,
	TEXT("Whether CUSTOM_RENDERER GPU profiler events are enabled"),
	ECVF_ReadOnly);

bool IsCustomRendererTrafficLightGPUProfilerEnabled()
{
	return CVarCustomRendererTrafficLightGPUProfilerEnabled.GetValueOnRenderThread();
}

#endif