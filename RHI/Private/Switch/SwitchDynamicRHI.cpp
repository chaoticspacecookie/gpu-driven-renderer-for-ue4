
#include "RHI.h"
#include "Modules/ModuleManager.h"

FDynamicRHI* PlatformCreateDynamicRHI()
{
	// Load the dynamic RHI module (only NVN supported on Switch OS)
	IDynamicRHIModule* DynamicRHIModule = &FModuleManager::LoadModuleChecked<IDynamicRHIModule>(TEXT("NVNRHI"));

	// Create the dynamic RHI
	FDynamicRHI* DynamicRHI = DynamicRHIModule->CreateRHI();

	return DynamicRHI;
}
