
/*=============================================================================
	GPUProfiler.h: Hierarchical GPU Profiler.
=============================================================================*/

#pragma once

#if ENABLE_CUSTOM_RENDERER_PERFORMANCE_MONITORING

#include "GPUProfiler.h"
#include "Misc/ScopeLock.h"
#include "CustomRenderer/CustomRendererLLM.h"

//
//
// TTrafficLightTimerBlock
//		A block of timers used by the traffic light GPU systems
//
//

template<class TIMER_CLASS>
class TTrafficLightTimerBlock : public TIntrusiveLinkedList<TTrafficLightTimerBlock<TIMER_CLASS>>
{

	static const uint32 NUM_ELEMENTS = 32;

public:

	typedef TIMER_CLASS	FTrafficLightTimer;

	TTrafficLightTimerBlock()
		: mNumAllocated(0)
	{
	}

	bool							CanAllocate()
	{
		return (mNumAllocated < NUM_ELEMENTS);
	}
	FTrafficLightTimer*				Allocate()
	{
		check(CanAllocate());
		FTrafficLightTimer*	alloc = &mTimerBlock[mNumAllocated];
		mNumAllocated++;
		return alloc;
	}

	void							DeallocateAll()
	{
		mNumAllocated = 0;
	}

	int32							Num()
	{
		return mNumAllocated;
	}
	FTrafficLightTimer*				operator[](int32 InIndex)
	{
		return &mTimerBlock[InIndex];
	}

private:

	typedef TStaticArray<FTrafficLightTimer, NUM_ELEMENTS> FTimerArray;

	FTimerArray		mTimerBlock;
	int32			mNumAllocated;

};

//
//
// TTrafficLightEventTracking
//		A container class for event tracking by the traffic light GPU systems
//
//

template<class TIMER_CLASS>
class TTrafficLightEventTracking
{
public:

	typedef TIMER_CLASS	FTrafficLightTimer;

	TTrafficLightEventTracking()
	{
	}

	bool					Contains(FName InName)
	{
		return EventTracking.Contains(InName);
	}
	void					Add(FName InName)
	{
		check(!EventTracking.Contains(InName));
		EventTracking.Add(InName);
	}
	FTrafficLightTimer*		GetTrackingTimer(FName InName)
	{
		return EventTracking[InName];
	}
	void					SetTrackingTimer(FName InName, FTrafficLightTimer* InTimer)
	{
		EventTracking[InName] = InTimer;
	}

private:

	TMap<FName, FTrafficLightTimer*> EventTracking;
};

//
//
// TTrafficLightTimerBlockPool
//		A pool of timer blocks used by traffic light GPU systems for recycling timers
//
//

template<class TIMER_CLASS>
class TTrafficLightTimerBlockPool
{
public:

	typedef TTrafficLightTimerBlock<TIMER_CLASS>	FTrafficLigthTimerBlock;

	TTrafficLightTimerBlockPool()
		: FreeTimerBlocks(nullptr)
	{

	}

	void ReleaseFreeBlocks()
	{
		FScopeLock Lock(&BlockListMutex);
		FTrafficLigthTimerBlock* FreeTimerBlock = FreeTimerBlocks;
		while (FreeTimerBlock != nullptr)
		{
			ensure(FreeTimerBlock->Num() == 0);
			FTrafficLigthTimerBlock* nextBlock = FreeTimerBlock->Next();

			FreeTimerBlock->Unlink();
			delete FreeTimerBlock;

			FreeTimerBlock = nextBlock;
		}
		FreeTimerBlocks = nullptr;

	}

	FTrafficLigthTimerBlock*	AllocateBlock()
	{
		FScopeLock Lock(&BlockListMutex);

		if (FreeTimerBlocks != nullptr)
		{
			FTrafficLigthTimerBlock* FreeTimerBlock = FreeTimerBlocks;
			FreeTimerBlock->Unlink();
			return FreeTimerBlock;
		}

		return new FTrafficLigthTimerBlock;
	}
	void						FreeBlock(FTrafficLigthTimerBlock* InBlock)
	{
		FScopeLock Lock(&BlockListMutex);

		InBlock->LinkHead(FreeTimerBlocks);
	}

private:

	FTrafficLigthTimerBlock*	FreeTimerBlocks;
	FCriticalSection			BlockListMutex;
};

//
//
// FGPUTrafficLightFrame
//		Abstract base class for a frames worth of traffic light GPU timers
//
//

class FGPUTrafficLightFrame
{
public:

	virtual ~FGPUTrafficLightFrame()
	{

	}

	virtual void RecordSamples(int32 FrameIndex) = 0;
	virtual void ClearSamples() = 0;
};

//
//
// TTrafficLightFrame
//		A frames worth of traffic light GPU timers
//
//

template<class TIMER_CLASS>
class TTrafficLightFrame : public FGPUTrafficLightFrame
{
public:

	typedef TIMER_CLASS									FTrafficLightTimer;
	typedef TTrafficLightTimerBlock<TIMER_CLASS>		FTrafficLightTimerBlock;
	typedef TTrafficLightTimerBlockPool<TIMER_CLASS>	FTrafficLightTimerBlockPool;
	typedef TTrafficLightEventTracking<TIMER_CLASS>		FTrafficLightEventTracking;

	TTrafficLightFrame(FTrafficLightTimerBlockPool* InTimerBlockPool, FTrafficLightEventTracking* InEventTracking)
		: TimerBlockPool(InTimerBlockPool)
		, EventTracking(InEventTracking)
		, TimerBlockList(nullptr)
	{

	}

	virtual ~TTrafficLightFrame()
	{
		while (TimerBlockList != nullptr)
		{
			FTrafficLightTimerBlock* TimerBlock = TimerBlockList;

			TimerBlock->Unlink();
			TimerBlock->DeallocateAll();
			TimerBlockPool->FreeBlock(TimerBlock);
		}

		TimerBlockPool->ReleaseFreeBlocks();
	}

	void BeginEvent(FName InName, CUSTOM_RENDERER_BE be = CUSTOM_RENDERER_BE::NULLMETRIC)
	{
		if (!EventTracking->Contains(InName))
		{
			EventTracking->Add(InName);
		}

		if (EventTracking->GetTrackingTimer(InName) == nullptr)
		{
			FTrafficLightTimer* NewTimer = AllocateTimer(InName, be);
			EventTracking->SetTrackingTimer(InName, NewTimer);

			NewTimer->BeginTiming();
		}
	}
	void EndEvent(FName InName)
	{
		if (EventTracking->Contains(InName))
		{
			FTrafficLightTimer* TrackingTimer = EventTracking->GetTrackingTimer(InName);
			if (TrackingTimer != nullptr)
			{
				TrackingTimer->EndTiming();
				EventTracking->SetTrackingTimer(InName, nullptr);
			}
		}
	}

	virtual void				BeginFrame()
	{

	}

	virtual void				EndFrame()
	{
	}

	virtual void				RecordSamples(int32 FrameIndex)
	{
		for (typename FTrafficLightTimerBlock::TIterator BlockIt(TimerBlockList); BlockIt; BlockIt.Next())
		{
			FTrafficLightTimerBlock& TimerBlock = *BlockIt;
			for (int32 TimerCount = 0; TimerCount < TimerBlock.Num(); TimerCount++)
			{
				FTrafficLightTimer* Timer = TimerBlock[TimerCount];

				FCustomRendererDiagnosticsCore::RecordGPUSample(Timer->Name(), Timer->Timing(), FrameIndex);
				//
				if(Timer->BudgetEventMetric() != CUSTOM_RENDERER_BE::NULLMETRIC){
					CUSTOM_RENDERER_BUDGETEVENTS_SAMPLE(Timer->BudgetEventMetric(),Timer->Timing() );
				}
			}
		}
	}

	virtual void				ClearSamples()
	{
		while (TimerBlockList != nullptr)
		{
			FTrafficLightTimerBlock* TimerBlock = TimerBlockList;

			TimerBlock->Unlink();
			TimerBlock->DeallocateAll();
			TimerBlockPool->FreeBlock(TimerBlock);
		}
	}

private:

	FTrafficLightTimer*			AllocateTimer(FName InName, CUSTOM_RENDERER_BE be = CUSTOM_RENDERER_BE::NULLMETRIC)
	{
		FTrafficLightTimerBlock* AllocateFromBlock = nullptr;

		if (TimerBlockList != nullptr)
		{
			if (TimerBlockList->CanAllocate())
			{
				AllocateFromBlock = TimerBlockList;
			}
		}

		if (AllocateFromBlock == nullptr)
		{
			AllocateFromBlock = TimerBlockPool->AllocateBlock();
			AllocateFromBlock->LinkHead(TimerBlockList);
		}

		check(AllocateFromBlock != nullptr);
		check(AllocateFromBlock->CanAllocate());

		FTrafficLightTimer* NewTimer = AllocateFromBlock->Allocate();
		NewTimer->SetName(InName);
		NewTimer->SetBudgetEventMetric(be);
		return NewTimer;
	}

	FTrafficLightTimerBlockPool*	TimerBlockPool;
	FTrafficLightEventTracking*		EventTracking;
	FTrafficLightTimerBlock*		TimerBlockList;
};

//
//
// TTrafficLightFramePool
//		A pool of frames used by traffic light GPU systems for recycling timers
//
//

template<class FRAME_CLASS>
class TTrafficLightFramePool
{
public:

	TTrafficLightFramePool()
	{
	}

	TSharedPtr<FRAME_CLASS>	AllocateFrame(
		typename FRAME_CLASS::FTrafficLightTimerBlockPool* InTimerBlockPool,
		typename FRAME_CLASS::FTrafficLightEventTracking* InEventTracking)
	{
		for (int32 FrameCount = 0; FrameCount < Frames.Num(); FrameCount++)
		{
			if (Frames[FrameCount].GetSharedReferenceCount() == 1)
			{
				return Frames[FrameCount];
			}
		}

		TSharedPtr<FRAME_CLASS>	NewFrame = MakeShareable(new FRAME_CLASS(InTimerBlockPool, InEventTracking));
		Frames.Add(NewFrame);
		return NewFrame;
	}

private:

	TArray<TSharedPtr<FRAME_CLASS>>	Frames;
};

//
//
// TTrafficLightGPUProfiler
//		The actual traffic light GPU profiler
//
//

bool IsCustomRendererTrafficLightGPUProfilerEnabled();

template<class TIMER_CLASS, class FRAME_CLASS = TTrafficLightFrame<TIMER_CLASS>>
class TTrafficLightGPUProfiler
{
public:

	typedef TTrafficLightEventTracking<TIMER_CLASS>		FTrafficLightEventTracking;
	typedef TTrafficLightTimerBlockPool<TIMER_CLASS>	FTrafficLightTimerBlockPool;
	typedef FRAME_CLASS									FTrafficLightFrame;
	typedef TTrafficLightFramePool<FRAME_CLASS>			FTrafficLightFramePool;

	TTrafficLightGPUProfiler()
	{
	}
	~TTrafficLightGPUProfiler()
	{
		check(!CurrentFrame.IsValid());
	}

	void BeginFrame()
	{
		LLM_SCOPE_CUSTOM_RENDERER(ELLMTagCUSTOM_RENDERER::CustomRendererPerformanceTracking);

		if (CurrentFrameCount == 0)
		{
			check(!CurrentFrame.IsValid());
			CurrentFrame = FramePool.AllocateFrame(&TimerBlockPool, &EventTracking);
			CurrentFrame->BeginFrame();
			CurrentFrameCount = 1;
		}
		else
		{
			CurrentFrameCount++;
		}
	}
	void EndFrame()
	{
		LLM_SCOPE_CUSTOM_RENDERER(ELLMTagCUSTOM_RENDERER::CustomRendererPerformanceTracking);

		if (CurrentFrameCount == 0)
		{
			check(false);
		}
		else if (CurrentFrameCount == 1)
		{
			check(CurrentFrame.IsValid());
			CurrentFrame->EndFrame();

			FCustomRendererDiagnosticsCore::RecordGPUFrame(CurrentFrame);

			CurrentFrame.Reset();
			CurrentFrameCount = 0;
		}
		else
		{
			CurrentFrameCount--;
		}
	}

	void BeginEvent(FName InName, CUSTOM_RENDERER_BE be)
	{
		LLM_SCOPE_CUSTOM_RENDERER(ELLMTagCUSTOM_RENDERER::CustomRendererPerformanceTracking);
		check(CurrentFrame.IsValid());
		CurrentFrame->BeginEvent(InName,be);
	}
	void EndEvent(FName InName)
	{
		LLM_SCOPE_CUSTOM_RENDERER(ELLMTagCUSTOM_RENDERER::CustomRendererPerformanceTracking);
		check(CurrentFrame.IsValid());
		CurrentFrame->EndEvent(InName);
	}

private:

	FTrafficLightEventTracking			EventTracking;
	FTrafficLightTimerBlockPool			TimerBlockPool;
	FTrafficLightFramePool				FramePool;
	TSharedPtr<FTrafficLightFrame>		CurrentFrame;
	int									CurrentFrameCount = 0;
};

#endif