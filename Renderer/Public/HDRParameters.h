// CUSTOM_RENDERER Parameters and settings for Parameterised ACES RRT/ODT
#pragma once
#include "RenderCore/Public/ShaderParameterMacros.h"

BEGIN_SHADER_PARAMETER_STRUCT(FCustomRendererHDRTonemapParameters, )
	// Parameterized ACES curve data
	SHADER_PARAMETER_ARRAY(float, AcesCoefsLow, [10])
	SHADER_PARAMETER_ARRAY(float, AcesCoefsHigh, [10])
	SHADER_PARAMETER(FVector2D, AcesMin)
	SHADER_PARAMETER(FVector2D, AcesMid)
	SHADER_PARAMETER(FVector2D, AcesMax)
	SHADER_PARAMETER(FVector2D, AcesSlope)
	SHADER_PARAMETER(FVector4, AcesParams)
END_SHADER_PARAMETER_STRUCT()

RENDERER_API FCustomRendererHDRTonemapParameters GetACESParameters();
