#include "VPLGI.h"
#include "GlobalShader.h" 
#include "RenderGraphUtils.h"
#include "ShadowRendering.h"
#include "ScenePrivate.h"
#include "SceneRenderTargets.h"
#include "SceneTextureParameters.h"
#include "PostProcess/PostProcessing.h"
#include "PostProcess/SceneFilterRendering.h"
#include "DeferredShadingRenderer.h"

static TAutoConsoleVariable<int32> CVarVPLGISupported(
	TEXT("r.VPLGI.Supported"),
	0,
	TEXT("Whether to compile VPLGI shaders (cannot be changed at runtime)."),
	ECVF_ReadOnly
);

bool GVPLGIModulateBySSAO = true;
FAutoConsoleVariableRef CVarVPLGIModulateBySSAO(
	TEXT("r.VPLGI.ModulateBySSAO"),
	GVPLGIModulateBySSAO,
	TEXT("Modulate VPL intensity by SSAO strength to avoid flickering in corners"),
	ECVF_Scalability | ECVF_RenderThreadSafe
);

int32 GVPLGISampleCount = 32;
FAutoConsoleVariableRef CVarVPLGISampleCount(
	TEXT("r.VPLGI.SampleCount"),
	GVPLGISampleCount,
	TEXT("How many VPLs to sample per pixel"),
	ECVF_RenderThreadSafe
);

bool GVPLGIGenerateSampleLocationsEveryFrame = false;
FAutoConsoleVariableRef CVarVPLGIGenerateSampleLocationsEveryFrame(
	TEXT("r.VPLGI.GenerateSampleLocationsEveryFrame"),
	GVPLGIGenerateSampleLocationsEveryFrame,
	TEXT("Generate sample locations every frame for frame captures"),
	ECVF_RenderThreadSafe | ECVF_Cheat
);

int32 GVPLGIType = 0;
FAutoConsoleVariableRef CVarVPLGIType(
	TEXT("r.VPLGI.Type"),
	GVPLGIType,
	TEXT("0 is temporal checkerboard, 1 is fullscreen")
);

bool GVPLGICacheVPLs = true;
FAutoConsoleVariableRef CVarVPLGICacheVPLs(
	TEXT("r.VPLGI.CacheVPLs"),
	GVPLGICacheVPLs,
	TEXT("Keep existing VPLs alive until they go out of the light's influence to avoid flickering"),
	ECVF_RenderThreadSafe
);

bool GVPLGIUniformSamplePattern = true;
FAutoConsoleVariableRef CVarVPLGIUnifromSamplePattern(
	TEXT("r.VPLGI.UniformSamplePattern"),
	GVPLGIUniformSamplePattern,
	TEXT("Generate a octaweb shaped ring pattern for sampling"),
	ECVF_RenderThreadSafe
);

bool GVPLGICullLights = false;
FAutoConsoleVariableRef CVarVPLGICullLights(
	TEXT("r.VPLGI.CullLights"),
	GVPLGICullLights,
	TEXT("Generate a per-tile list of culled lights that only contain relevant lights"),
	ECVF_RenderThreadSafe
);

int32 GVPLGITileSize = 16;
FAutoConsoleVariableRef CVarVPLGITileSize(
	TEXT("r.VPLGI.TileSize"),
	GVPLGITileSize,
	TEXT("Tile size of the lighting pass"),
	ECVF_RenderThreadSafe | ECVF_ReadOnly
);

bool FVirtualPointLight::IsSupported()
{
	return false;//Romain Disbled to fix the steam build
	return CVarVPLGISupported.GetValueOnAnyThread() != 0;
}

bool FVirtualPointLight::ShouldCacheVPLs()
{
	return GVPLGICacheVPLs;
}

void FVirtualPointLight::SetupCommonParameters(FRDGBuilder& GraphBuilder, const FViewInfo& View, const FProjectedShadowInfo& ProjectedShadowInfo, FVirtualPointLight::FCommonParameters &OutParameters)
{
	FDynamicGlobalIllumination::SetupRSMParameters(GraphBuilder, View, ProjectedShadowInfo, OutParameters.RSMParameters);
	OutParameters.SampleCount = GVPLGISampleCount;
}

class FVPLGIUniformSamplePatternDim : SHADER_PERMUTATION_BOOL("UNIFORM_SAMPLE_PATTERN");

/** Updates VPLs based on the changes in Reflective Shadow Maps */
class FVPLGIUpdateCS : public FGlobalShader
{
	DECLARE_GLOBAL_SHADER(FVPLGIUpdateCS)
	SHADER_USE_PARAMETER_STRUCT(FVPLGIUpdateCS, FGlobalShader)

	BEGIN_SHADER_PARAMETER_STRUCT(FParameters, )
		SHADER_PARAMETER_STRUCT_INCLUDE(FVirtualPointLight::FCommonParameters,			CommonParameters)
		SHADER_PARAMETER_STRUCT_INCLUDE(FDynamicGlobalIllumination::FLightParameters,	LightParameters)
		SHADER_PARAMETER_STRUCT_REF(FShadowDepthPassUniformParameters,					ShadowDepthParameters)
		SHADER_PARAMETER_RDG_BUFFER_SRV(Buffer<uint2>,									VPLSampleLocationsBuffer)
		SHADER_PARAMETER_RDG_BUFFER_UAV(RWStructuredBuffer<FVirtualPointLight>,			RWVPLList)
	END_SHADER_PARAMETER_STRUCT()
	
	using FPermutationDomain = TShaderPermutationDomain<FVPLGIUniformSamplePatternDim>;

	static bool ShouldCompilePermutation(const FGlobalShaderPermutationParameters& Parameters)
	{
		return IsFeatureLevelSupported(Parameters.Platform, ERHIFeatureLevel::SM5) && FVirtualPointLight::IsSupported();
	}

	static uint32 GetThreadGroupSize()
	{
		return 1024;
	}

	static void ModifyCompilationEnvironment(const FGlobalShaderPermutationParameters& Parameters, FShaderCompilerEnvironment& OutEnvironment)
	{
		FGlobalShader::ModifyCompilationEnvironment(Parameters, OutEnvironment);
		OutEnvironment.SetDefine(TEXT("THREADGROUP_SIZE_X"), GetThreadGroupSize());
	}
};

IMPLEMENT_GLOBAL_SHADER(FVPLGIUpdateCS, "/Engine/Private/CustomRenderer/VPLGI.usf", "UpdateCS", SF_Compute);

class FVPLGIApplyCS : public FGlobalShader
{
	DECLARE_GLOBAL_SHADER(FVPLGIApplyCS)
	SHADER_USE_PARAMETER_STRUCT(FVPLGIApplyCS, FGlobalShader)
	
	enum class EAlgorithmType
	{
		CheckerboardEven,
		CheckerboardOdd,
		NoCheckerboard,

		MAX
	};

	class FVPLGIAlgorithmTypeDim : SHADER_PERMUTATION_ENUM_CLASS("ALGORITHM_TYPE", EAlgorithmType);
	class FVPLGICullDim : SHADER_PERMUTATION_BOOL("USE_CULLED_LIGHTS");
	
	using FPermutationDomain = TShaderPermutationDomain<FVPLGIAlgorithmTypeDim, FVirtualPointLight::FUseCachedDim, FVPLGICullDim>;

	BEGIN_SHADER_PARAMETER_STRUCT(FParameters, )
		SHADER_PARAMETER_STRUCT_INCLUDE(FVirtualPointLight::FCommonParameters,			CommonParameters)
		SHADER_PARAMETER_STRUCT_INCLUDE(FDynamicGlobalIllumination::FLightParameters,	LightParameters)
		SHADER_PARAMETER_RDG_BUFFER_SRV(StructuredBuffer<FVirtualPointLight>,			VPLList)
		SHADER_PARAMETER_RDG_BUFFER_SRV(Buffer<uint2>,									VPLSampleLocationsBuffer)
		SHADER_PARAMETER_RDG_BUFFER_SRV(Buffer<uint>,									CulledVPLs)
		SHADER_PARAMETER_RDG_TEXTURE_UAV(RWTexture2D<float3>,							RWSceneColorTexture)
		SHADER_PARAMETER(float,															VPLScale)
		SHADER_PARAMETER(FIntPoint,														CulledVPLSize)
	END_SHADER_PARAMETER_STRUCT()

	static bool ShouldCompilePermutation(const FGlobalShaderPermutationParameters& Parameters)
	{
		if (!IsFeatureLevelSupported(Parameters.Platform, ERHIFeatureLevel::SM5) || !FVirtualPointLight::IsSupported())
		{
			return false;
		}

		FPermutationDomain PermutationVector(Parameters.PermutationId);

		// Culling only works with cached VPLs
		if (PermutationVector.Get<FVPLGICullDim>() &&
			!PermutationVector.Get<FVirtualPointLight::FUseCachedDim>())
		{
			return false;
		}

		return true;
	}

	static FIntPoint GetThreadGroupSize()
	{
		return FIntPoint(GVPLGITileSize, GVPLGITileSize);
	}
};

IMPLEMENT_GLOBAL_SHADER(FVPLGIApplyCS, "/Engine/Private/CustomRenderer/VPLGIApply.usf", "ApplyCS", SF_Compute);

class FVPLGICopyColorCS : public FGlobalShader
{
	DECLARE_GLOBAL_SHADER(FVPLGICopyColorCS)
	SHADER_USE_PARAMETER_STRUCT(FVPLGICopyColorCS, FGlobalShader)

	BEGIN_SHADER_PARAMETER_STRUCT(FParameters, )
		SHADER_PARAMETER_RDG_TEXTURE_UAV(RWTexture2D<float3>,	RWSceneColorCopyTexture)
		SHADER_PARAMETER_RDG_TEXTURE(Texture2D,					SceneColorTexture)
	END_SHADER_PARAMETER_STRUCT()

	static bool ShouldCompilePermutation(const FGlobalShaderPermutationParameters& Parameters)
	{
		return IsFeatureLevelSupported(Parameters.Platform, ERHIFeatureLevel::SM5) && FVirtualPointLight::IsSupported();
	}

	static FIntPoint GetThreadGroupSize()
	{
		return FIntPoint(FComputeShaderUtils::kGolden2DGroupSize, FComputeShaderUtils::kGolden2DGroupSize);
	}

	static void ModifyCompilationEnvironment(const FGlobalShaderPermutationParameters& Parameters, FShaderCompilerEnvironment& OutEnvironment)
	{
		FGlobalShader::ModifyCompilationEnvironment(Parameters, OutEnvironment);
		OutEnvironment.SetDefine(TEXT("THREADGROUP_SIZE_X"), GetThreadGroupSize().X);
		OutEnvironment.SetDefine(TEXT("THREADGROUP_SIZE_Y"), GetThreadGroupSize().Y);
	}	
};

IMPLEMENT_GLOBAL_SHADER(FVPLGICopyColorCS, "/Engine/Private/CustomRenderer/VPLGI.usf", "CopyBufferColorInterleavedCS", SF_Compute);

class FVPLGIModulateRSMCS : public FGlobalShader
{
	DECLARE_GLOBAL_SHADER(FVPLGIModulateRSMCS)
	SHADER_USE_PARAMETER_STRUCT(FVPLGIModulateRSMCS, FGlobalShader)

	BEGIN_SHADER_PARAMETER_STRUCT(FParameters, )
		SHADER_PARAMETER_STRUCT_INCLUDE(FVirtualPointLight::FCommonParameters,			CommonParameters)
		SHADER_PARAMETER_STRUCT_INCLUDE(FDynamicGlobalIllumination::FLightParameters,	LightParameters)
		SHADER_PARAMETER_RDG_TEXTURE_UAV(RWTexture2D<float3>,							RWRSMBaseColorTexture)
	END_SHADER_PARAMETER_STRUCT()

	static bool ShouldCompilePermutation(const FGlobalShaderPermutationParameters& Parameters)
	{
		return FVirtualPointLight::IsSupported();
	}

	static FIntPoint GetThreadGroupSize()
	{
		return FIntPoint(FComputeShaderUtils::kGolden2DGroupSize, FComputeShaderUtils::kGolden2DGroupSize);
	}

	static void ModifyCompilationEnvironment(const FGlobalShaderPermutationParameters& Parameters, FShaderCompilerEnvironment& OutEnvironment)
	{
		FGlobalShader::ModifyCompilationEnvironment(Parameters, OutEnvironment);
		OutEnvironment.SetDefine(TEXT("THREADGROUP_SIZE_X"), GetThreadGroupSize().X);
		OutEnvironment.SetDefine(TEXT("THREADGROUP_SIZE_Y"), GetThreadGroupSize().Y);
	}
};

IMPLEMENT_GLOBAL_SHADER(FVPLGIModulateRSMCS, "/Engine/Private/CustomRenderer/VPLGI.usf", "ModulateRSMCS", SF_Compute);

class FCompositeVPLPS : public FGlobalShader
{
	DECLARE_GLOBAL_SHADER(FCompositeVPLPS)
	SHADER_USE_PARAMETER_STRUCT(FCompositeVPLPS, FGlobalShader)

		static bool ShouldCompilePermutation(const FGlobalShaderPermutationParameters& Parameters)
	{
		return ShouldCompileRayTracingShadersForProject(Parameters.Platform);
	}

	BEGIN_SHADER_PARAMETER_STRUCT(FParameters, )
		RENDER_TARGET_BINDING_SLOTS()
		SHADER_PARAMETER_RDG_TEXTURE(Texture2D, VPLGITexture)
		SHADER_PARAMETER_SAMPLER(SamplerState, VPLGITextureSampler)
		SHADER_PARAMETER_STRUCT_REF(FViewUniformShaderParameters, ViewUniformBuffer)

		SHADER_PARAMETER_STRUCT_INCLUDE(FSceneTextureParameters, SceneTextures)
		END_SHADER_PARAMETER_STRUCT()
};

IMPLEMENT_GLOBAL_SHADER(FCompositeVPLPS, "/Engine/Private/CustomRenderer/RayTracingVPLComposite.usf", "CompositeVPL", SF_Pixel);

class FVPLGIGenerateSampleLocationsCS : public FGlobalShader
{
	DECLARE_GLOBAL_SHADER(FVPLGIGenerateSampleLocationsCS)
	SHADER_USE_PARAMETER_STRUCT(FVPLGIGenerateSampleLocationsCS, FGlobalShader)

	BEGIN_SHADER_PARAMETER_STRUCT(FParameters, )
		SHADER_PARAMETER_STRUCT_INCLUDE(FVirtualPointLight::FCommonParameters,			CommonParameters)
		SHADER_PARAMETER_STRUCT_INCLUDE(FDynamicGlobalIllumination::FLightParameters,	LightParameters)
		SHADER_PARAMETER_RDG_BUFFER_UAV(RWBuffer<uint2>,								RWVPLSampleLocationsBuffer)
	END_SHADER_PARAMETER_STRUCT()

	using FPermutationDomain = TShaderPermutationDomain<FVPLGIUniformSamplePatternDim>;

	static bool ShouldCompilePermutation(const FGlobalShaderPermutationParameters& Parameters)
	{
		return FVirtualPointLight::IsSupported();	
	}

	static FIntPoint GetThreadGroupSize()
	{
		return FIntPoint(32, 32);
	}

	static void ModifyCompilationEnvironment(const FGlobalShaderPermutationParameters& Parameters, FShaderCompilerEnvironment& OutEnvironment)
	{
		FGlobalShader::ModifyCompilationEnvironment(Parameters, OutEnvironment);
		OutEnvironment.SetDefine(TEXT("THREADGROUP_SIZE_X"), GetThreadGroupSize().X);
		OutEnvironment.SetDefine(TEXT("THREADGROUP_SIZE_Y"), GetThreadGroupSize().Y);
	}
};

IMPLEMENT_GLOBAL_SHADER(FVPLGIGenerateSampleLocationsCS, "/Engine/Private/CustomRenderer/VPLGI.usf", "GenerateSampleLocationsCS", SF_Compute);

class FVPLGICullLightsCS : public FGlobalShader
{
	DECLARE_GLOBAL_SHADER(FVPLGICullLightsCS)
	SHADER_USE_PARAMETER_STRUCT(FVPLGICullLightsCS, FGlobalShader)

	BEGIN_SHADER_PARAMETER_STRUCT(FParameters, )
		SHADER_PARAMETER_RDG_BUFFER_SRV(StructuredBuffer<FVirtualPointLight>,	VPLList)
		SHADER_PARAMETER_RDG_BUFFER_UAV(RWBuffer<uint>,							CulledVPLs)
		SHADER_PARAMETER_RDG_TEXTURE(Texture2D<float>,							FurthestHZBTexture)
		SHADER_PARAMETER_RDG_TEXTURE(Texture2D<float>,							ClosestHZBTexture)
		SHADER_PARAMETER_STRUCT_REF(FViewUniformShaderParameters,				ViewUniformBuffer)
		SHADER_PARAMETER(uint32,												SampleCount)
		SHADER_PARAMETER(FIntPoint,												CulledVPLSize)
		SHADER_PARAMETER(FVector2D,												DispatchThreadIDToHZBUV)
	END_SHADER_PARAMETER_STRUCT()

	static bool ShouldCompilePermutation(const FGlobalShaderPermutationParameters& Parameters)
	{
		return FVirtualPointLight::IsSupported();	
	}

	static FIntPoint GetThreadGroupSize()
	{
		return FIntPoint(FComputeShaderUtils::kGolden2DGroupSize, FComputeShaderUtils::kGolden2DGroupSize);
	}

	static void ModifyCompilationEnvironment(const FGlobalShaderPermutationParameters& Parameters, FShaderCompilerEnvironment& OutEnvironment)
	{
		FGlobalShader::ModifyCompilationEnvironment(Parameters, OutEnvironment);
		OutEnvironment.SetDefine(TEXT("THREADGROUP_SIZE_X"), GetThreadGroupSize().X);
		OutEnvironment.SetDefine(TEXT("THREADGROUP_SIZE_Y"), GetThreadGroupSize().Y);
		OutEnvironment.SetDefine(TEXT("USE_CACHED_VPLS"), true);
		OutEnvironment.SetDefine(TEXT("TILE_SIZE"), GVPLGITileSize);
	}
};

IMPLEMENT_GLOBAL_SHADER(FVPLGICullLightsCS, "/Engine/Private/CustomRenderer/VPLGICullLights.usf", "CullLightsCS", SF_Compute);

FRDGBufferRef UpdateVPLList(FRDGBuilder& GraphBuilder, const FProjectedShadowInfo* ProjectedShadowInfo, const FViewInfo& View, FRDGBufferRef VPLSampleLocationsBuffer = nullptr)
{
	FSceneRenderTargets& SceneContext = FSceneRenderTargets::Get(GraphBuilder.RHICmdList);

	FVPLGIUpdateCS::FPermutationDomain PermutationVector;
	PermutationVector.Set<FVPLGIUniformSamplePatternDim>(GVPLGIUniformSamplePattern);

	TShaderMapRef<FVPLGIUpdateCS> ComputeShader(View.ShaderMap, PermutationVector);

	FVPLGIUpdateCS::FParameters* Parameters = GraphBuilder.AllocParameters<FVPLGIUpdateCS::FParameters>();
	SetupCommonParameters(GraphBuilder, View, *ProjectedShadowInfo, Parameters->CommonParameters);
	FDynamicGlobalIllumination::SetupLightParameters(ProjectedShadowInfo->GetLightSceneInfo().Proxy, Parameters->LightParameters);
	
	uint32 VPLListSize = GVPLGISampleCount;
	
	auto *SceneViewState = static_cast<FSceneViewState*>(View.State);
	FRDGBufferRef VPLList;
	bool bIsVPLListValid = SceneViewState->VPLListBuffer.IsValid() && GVPLGISampleCount == SceneViewState->VPLListBuffer->Desc.NumElements;

	if (bIsVPLListValid)
	{
		VPLList = GraphBuilder.RegisterExternalBuffer(SceneViewState->VPLListBuffer, TEXT("VPLListBuffer"));
	}
	else
	{
		FRDGBufferDesc BufferDesc = FRDGBufferDesc::CreateStructuredDesc(sizeof(FVector4) * 3, VPLListSize);
		VPLList = GraphBuilder.CreateBuffer(BufferDesc, TEXT("VPLList"), ERDGResourceFlags::MultiFrame);
	}

	FRDGBufferUAVRef VPLListUAV = GraphBuilder.CreateUAV(VPLList);

	if (!bIsVPLListValid)
	{
		AddClearUAVPass(GraphBuilder, VPLListUAV, 0);
	}
	
	if (GVPLGIUniformSamplePattern)
	{
		Parameters->VPLSampleLocationsBuffer = GraphBuilder.CreateSRV(VPLSampleLocationsBuffer, PF_R16G16_UINT);
	}

	Parameters->RWVPLList = GraphBuilder.CreateUAV(VPLList);

	Parameters->ShadowDepthParameters = ProjectedShadowInfo->ShadowDepthPassUniformBuffer;

	check(GVPLGISampleCount < 1024);

	FComputeShaderUtils::AddPass(
		GraphBuilder,
		RDG_EVENT_NAME("VPL Update"),
		ComputeShader,
		Parameters,
		FIntVector(1, 1, 1));
	
	GraphBuilder.QueueBufferExtraction(VPLList, &SceneViewState->VPLListBuffer,  FRDGResourceState::EAccess::Read, FRDGResourceState::EPipeline::Compute);

	return VPLList;
}

FRDGBufferRef CullVPLList(FRDGBuilder& GraphBuilder, const FViewInfo& View, FRDGBufferRef VPLList)
{
	TShaderMapRef<FVPLGICullLightsCS> ComputeShader(View.ShaderMap);

	FVPLGICullLightsCS::FParameters* Parameters = GraphBuilder.AllocParameters<FVPLGICullLightsCS::FParameters>();

	Parameters->VPLList = GraphBuilder.CreateSRV(VPLList);

	FIntPoint CulledVPLSize = FIntPoint(
		FMath::DivideAndRoundUp(View.ViewRect.Width(), GVPLGITileSize),
		FMath::DivideAndRoundUp(View.ViewRect.Height(), GVPLGITileSize));

	uint32 TileCount = CulledVPLSize.X * CulledVPLSize.Y;

	FRDGBufferDesc BufferDesc = FRDGBufferDesc::CreateBufferDesc(sizeof(uint16_t), (GVPLGISampleCount + 1) * TileCount);
	FRDGBufferRef CulledVPLs = GraphBuilder.CreateBuffer(BufferDesc, TEXT("CulledVPLs"));
	Parameters->CulledVPLs = GraphBuilder.CreateUAV(CulledVPLs, PF_R16_UINT);

	Parameters->FurthestHZBTexture = GraphBuilder.RegisterExternalTexture(View.HZB);
	Parameters->ClosestHZBTexture = GraphBuilder.RegisterExternalTexture(View.ClosestHZB);

	const FVector2D HZBUvFactor(
		float(View.ViewRect.Width()) / float(2 * View.HZBMipmap0Size.X),
		float(View.ViewRect.Height()) / float(2 * View.HZBMipmap0Size.Y));

	Parameters->DispatchThreadIDToHZBUV = FVector2D::UnitVector / FVector2D(View.ViewRect.Size()) * HZBUvFactor;

	Parameters->ViewUniformBuffer = View.ViewUniformBuffer;
	Parameters->SampleCount = GVPLGISampleCount;
	Parameters->CulledVPLSize = CulledVPLSize;

	FComputeShaderUtils::AddPass(
		GraphBuilder,
		RDG_EVENT_NAME("VPL Cull"),
		ComputeShader,
		Parameters,
		FComputeShaderUtils::GetGroupCount(CulledVPLSize, FVPLGICullLightsCS::GetThreadGroupSize()));

	return CulledVPLs;
}

void ApplyVPLToScene(FRDGBuilder& GraphBuilder, const FViewInfo& View, const FSortedShadowMapAtlas& RSMAtlas, FVPLGIApplyCS::EAlgorithmType AlgorithmType)
{
	check(RSMAtlas.Shadows.Num() == 1);
	const FProjectedShadowInfo* ProjectedShadowInfo = RSMAtlas.Shadows[0];
	check(ProjectedShadowInfo->bReflectiveShadowmap);

	FRDGBufferRef VPLList = nullptr;
	FRDGBufferRef VPLSampleLocations = nullptr;
	FRDGBufferRef CulledVPLs = nullptr;

	bool bNeedSampleLocationsBuffer = (GVPLGICacheVPLs && GVPLGIUniformSamplePattern) || !GVPLGICacheVPLs;
	FIntPoint RSMSize(ProjectedShadowInfo->ResolutionX, ProjectedShadowInfo->ResolutionY);

	if (bNeedSampleLocationsBuffer)
	{
		auto* SceneViewState = static_cast<FSceneViewState*>(View.State);
		bool bSampleLocationsValid;

		if (SceneViewState->VPLSampleLocationsBuffer.IsValid() &&
			GVPLGISampleCount == SceneViewState->VPLSampleLocationsBuffer->Desc.NumElements)
		{
			VPLSampleLocations = GraphBuilder.RegisterExternalBuffer(SceneViewState->VPLSampleLocationsBuffer, TEXT("VPLSampleLocationsBuffer"));
			bSampleLocationsValid = !GVPLGIGenerateSampleLocationsEveryFrame;
		}
		else
		{
			FRDGBufferDesc BufferDesc = FRDGBufferDesc::CreateBufferDesc(sizeof(uint16) * 2, GVPLGISampleCount);
			VPLSampleLocations = GraphBuilder.CreateBuffer(BufferDesc, TEXT("VPLSampleLocations"), ERDGResourceFlags::MultiFrame);
			bSampleLocationsValid = false;
		}
		
		// Generate sample locations for sampling the RSM
		if (!bSampleLocationsValid)
		{
			auto* Parameters = GraphBuilder.AllocParameters<FVPLGIGenerateSampleLocationsCS::FParameters>();
			SetupCommonParameters(GraphBuilder, View, *ProjectedShadowInfo, Parameters->CommonParameters);
			FDynamicGlobalIllumination::SetupLightParameters(ProjectedShadowInfo->GetLightSceneInfo().Proxy, Parameters->LightParameters);
			Parameters->RWVPLSampleLocationsBuffer = GraphBuilder.CreateUAV(VPLSampleLocations, PF_R16G16_UINT);

			FVPLGIUpdateCS::FPermutationDomain PermutationVector;
			PermutationVector.Set<FVPLGIUniformSamplePatternDim>(GVPLGIUniformSamplePattern);

			TShaderMapRef<FVPLGIGenerateSampleLocationsCS> ComputeShader(View.ShaderMap, PermutationVector);
			
			FComputeShaderUtils::AddPass(
				GraphBuilder,
				RDG_EVENT_NAME("VPL Generate Sample Locations"),
				ComputeShader,
				Parameters,
				FComputeShaderUtils::GetGroupCount(RSMSize, FVPLGIGenerateSampleLocationsCS::GetThreadGroupSize()));

			GraphBuilder.QueueBufferExtraction(VPLSampleLocations, &SceneViewState->VPLSampleLocationsBuffer, FRDGResourceState::EAccess::Read, FRDGResourceState::EPipeline::Compute);
		}
	}

	if (GVPLGICacheVPLs)
	{
		// Update cached VPLs
		VPLList = UpdateVPLList(GraphBuilder, ProjectedShadowInfo, View, VPLSampleLocations);

		if (GVPLGICullLights)
		{
			CulledVPLs = CullVPLList(GraphBuilder, View, VPLList);
		}
	}
	else
	{
		if (GVPLGIModulateBySSAO)
		{
			// Shader needs R8G8B8A8_UNORM typed UAV load support
			check(GDynamicRHI->RHIIsTypedUAVLoadSupported(ProjectedShadowInfo->RenderTargets.ColorTargets[0]->GetDesc().Format));

			// Modulate VPLs in the RSM buffers
			FVPLGIModulateRSMCS::FParameters* Parameters = GraphBuilder.AllocParameters<FVPLGIModulateRSMCS::FParameters>();
			SetupCommonParameters(GraphBuilder, View, *ProjectedShadowInfo, Parameters->CommonParameters);

			FRDGTextureRef RSMBaseColorTexture		= GraphBuilder.RegisterExternalTexture(ProjectedShadowInfo->RenderTargets.ColorTargets[1], TEXT("RSMBaseColorTexture"));
			
			Parameters->RWRSMBaseColorTexture		= GraphBuilder.CreateUAV(RSMBaseColorTexture);

			FDynamicGlobalIllumination::SetupLightParameters(ProjectedShadowInfo->GetLightSceneInfo().Proxy, Parameters->LightParameters);

			TShaderMapRef<FVPLGIModulateRSMCS> ComputeShader(View.ShaderMap);

			FComputeShaderUtils::AddPass(
				GraphBuilder,
				RDG_EVENT_NAME("VPL Modulate"),
				ComputeShader,
				Parameters,
				FComputeShaderUtils::GetGroupCount(RSMSize, FVPLGIModulateRSMCS::GetThreadGroupSize()));
		}
	}
	
	// Apply VPLs
	FVPLGIApplyCS::FPermutationDomain PermutationVector;
	PermutationVector.Set<FVPLGIApplyCS::FVPLGIAlgorithmTypeDim>(AlgorithmType);
	PermutationVector.Set<FVirtualPointLight::FUseCachedDim>(GVPLGICacheVPLs);
	PermutationVector.Set<FVPLGIApplyCS::FVPLGICullDim>(GVPLGICullLights);

	TShaderMapRef<FVPLGIApplyCS> ComputeShader(View.ShaderMap, PermutationVector);

	FVPLGIApplyCS::FParameters* Parameters = GraphBuilder.AllocParameters<FVPLGIApplyCS::FParameters>();
	SetupCommonParameters(GraphBuilder, View, *ProjectedShadowInfo, Parameters->CommonParameters);

	FDynamicGlobalIllumination::SetupLightParameters(ProjectedShadowInfo->GetLightSceneInfo().Proxy, Parameters->LightParameters);

	FSceneRenderTargets& SceneContext				= FSceneRenderTargets::Get(GraphBuilder.RHICmdList);
	TRefCountPtr<IPooledRenderTarget> SceneColorRT	= SceneContext.GetSceneColor();
	FRDGTextureRef SceneColorTexture				= GraphBuilder.RegisterExternalTexture(SceneColorRT, TEXT("SceneColorTexture"));
	Parameters->RWSceneColorTexture					= GraphBuilder.CreateUAV(SceneColorTexture);

	if (GVPLGICacheVPLs)
	{
		Parameters->VPLList							= GraphBuilder.CreateSRV(VPLList);
	}
	else
	{
		Parameters->VPLSampleLocationsBuffer		= GraphBuilder.CreateSRV(VPLSampleLocations, PF_R16G16_UINT);
	}

	if (GVPLGICullLights)
	{
		Parameters->CulledVPLs						= GraphBuilder.CreateSRV(CulledVPLs, PF_R16_UINT);
	}

	Parameters->VPLScale							= View.FinalPostProcessSettings.VPLScale;

	FIntPoint SceneBufferSize = SceneContext.GetBufferSizeXY();
	FIntPoint CulledVPLSize = FIntPoint(
		FMath::DivideAndRoundUp(SceneBufferSize.X, GVPLGITileSize),
		FMath::DivideAndRoundUp(SceneBufferSize.Y, GVPLGITileSize));

	Parameters->CulledVPLSize						= CulledVPLSize;
	
	
	FComputeShaderUtils::AddPass(
		GraphBuilder,
		RDG_EVENT_NAME("VPL Apply"),
		ComputeShader,
		Parameters,
		FComputeShaderUtils::GetGroupCount(View.ViewRect.Size(), FVPLGIApplyCS::GetThreadGroupSize()));
}

static bool potatoepixel = false;

void ApplyVPLToScenePingPong(FRDGBuilder& GraphBuilder, const FViewInfo& View, const FSortedShadowMapAtlas& RSMAtlas)
{
	potatoepixel = !potatoepixel;
	if (potatoepixel)
	{
		ApplyVPLToScene(GraphBuilder, View, RSMAtlas, FVPLGIApplyCS::EAlgorithmType::CheckerboardEven);
	}
	else
	{
		ApplyVPLToScene(GraphBuilder, View, RSMAtlas, FVPLGIApplyCS::EAlgorithmType::CheckerboardOdd);
	}
}

void ApplyVPLToSceneNoCheckerboard(FRDGBuilder& GraphBuilder, const FViewInfo& View, const FSortedShadowMapAtlas& RSMAtlas)
{
	ApplyVPLToScene(GraphBuilder, View, RSMAtlas, FVPLGIApplyCS::EAlgorithmType::NoCheckerboard);
}

FRDGTextureUAVRef FVirtualPointLight::CopySceneColor(FRDGBuilder& GraphBuilder, const FViewInfo& View)
{
	FSceneRenderTargets& SceneContext = FSceneRenderTargets::Get(GraphBuilder.RHICmdList);

	FRDGTextureDesc Desc = SceneContext.GetSceneColor()->GetDesc();
	Desc.Format = PF_FloatRGB;
	Desc.Flags &= ~(TexCreate_FastVRAM | TexCreate_Transient);
	FRDGTextureRef SceneColorCopyTexture = GraphBuilder.CreateTexture(Desc, TEXT("SceneColorCopyTexture"));
	
	FRDGTextureUAVRef SceneColorCopyTextureUAV = GraphBuilder.CreateUAV(SceneColorCopyTexture);

	FVPLGICopyColorCS::FParameters* Parameters = GraphBuilder.AllocParameters<FVPLGICopyColorCS::FParameters>();
	Parameters->SceneColorTexture = GraphBuilder.RegisterExternalTexture(SceneContext.GetSceneColor(), TEXT("SceneColorTexture"));;
	Parameters->RWSceneColorCopyTexture = SceneColorCopyTextureUAV;

	TShaderMapRef<FVPLGICopyColorCS> ComputeShader(View.ShaderMap);

	FComputeShaderUtils::AddPass(
		GraphBuilder,
		RDG_EVENT_NAME(" Copy"),
		ComputeShader,
		Parameters,
		FComputeShaderUtils::GetGroupCount(Desc.Extent, FVPLGICopyColorCS::GetThreadGroupSize()));

	return SceneColorCopyTextureUAV;
}

void FVirtualPointLight::RenderLightsRasterized(FRDGBuilder& GraphBuilder, const FViewInfo& View, const FSortedShadowMaps& ShadowMaps)
{
	checkf(ShadowMaps.RSMAtlases.Num() <= 1, TEXT("Multiple lights are not implemented"));

	for (const FSortedShadowMapAtlas& RSMAtlas : ShadowMaps.RSMAtlases)
	{
		switch (GVPLGIType)
		{
		case 0:
			ApplyVPLToScenePingPong(GraphBuilder, View, RSMAtlas);
			break;
		case 1:
			ApplyVPLToSceneNoCheckerboard(GraphBuilder, View, RSMAtlas);
			break;
		}
	}
}


#if RHI_RAYTRACING
void FDeferredShadingSceneRenderer::CompositeRayTracingVPL(FRDGBuilder& GraphBuilder, TRefCountPtr<IPooledRenderTarget>& VPLRT, bool bFinalPass)
{
	FSceneRenderTargets& SceneContext = FSceneRenderTargets::Get(GraphBuilder.RHICmdList);

	//ResolveSceneColor(RHICmdList);
	for (FViewInfo& View : Views)
	{
		FSceneTextureParameters SceneTextures;
		SetupSceneTextureParameters(GraphBuilder, &SceneTextures);

		FCompositeVPLPS::FParameters *PassParameters = GraphBuilder.AllocParameters<FCompositeVPLPS::FParameters>();
		PassParameters->VPLGITexture = GraphBuilder.RegisterExternalTexture(VPLRT);
		PassParameters->VPLGITextureSampler = TStaticSamplerState<SF_Point, AM_Clamp, AM_Clamp, AM_Clamp>::GetRHI();
		PassParameters->ViewUniformBuffer = View.ViewUniformBuffer;
		
		if (bFinalPass)
		{
			PassParameters->RenderTargets[0] = FRenderTargetBinding(GraphBuilder.RegisterExternalTexture(SceneContext.GetSceneColor()), ERenderTargetLoadAction::ELoad);
		}
		else
		{
			PassParameters->RenderTargets[0] = FRenderTargetBinding(GraphBuilder.RegisterExternalTexture(SceneContext.GetSceneColor()), ERenderTargetLoadAction::ELoad);

//			PassParameters->RenderTargets[0] = FRenderTargetBinding(GraphBuilder.RegisterExternalTexture(OutputData), ERenderTargetLoadAction::ELoad);
		}

		PassParameters->SceneTextures = SceneTextures;

		// dxr_todo: Unify with RTGI compositing workflow
		GraphBuilder.AddPass(
			RDG_EVENT_NAME("GlobalIlluminationComposite"),
			PassParameters,
			ERDGPassFlags::Raster,
			[this, &SceneContext, &View, PassParameters](FRHICommandListImmediate& RHICmdList)
		{
			TShaderMapRef<FPostProcessVS> VertexShader(View.ShaderMap);
			TShaderMapRef<FCompositeVPLPS> PixelShader(View.ShaderMap);
			FGraphicsPipelineStateInitializer GraphicsPSOInit;
			RHICmdList.ApplyCachedRenderTargets(GraphicsPSOInit);

			// Additive blending
			GraphicsPSOInit.BlendState = TStaticBlendState<CW_RGB, BO_Add, BF_One, BF_One>::GetRHI();
			GraphicsPSOInit.RasterizerState = TStaticRasterizerState<FM_Solid, CM_None>::GetRHI();
			GraphicsPSOInit.DepthStencilState = TStaticDepthStencilState<false, CF_Always>::GetRHI();

			GraphicsPSOInit.BoundShaderState.VertexDeclarationRHI = GFilterVertexDeclaration.VertexDeclarationRHI;
			GraphicsPSOInit.BoundShaderState.VertexShaderRHI = VertexShader.GetVertexShader();
			GraphicsPSOInit.BoundShaderState.PixelShaderRHI = PixelShader.GetPixelShader();
			GraphicsPSOInit.PrimitiveType = PT_TriangleList;
			SetGraphicsPipelineState(RHICmdList, GraphicsPSOInit);

			SetShaderParameters(RHICmdList, PixelShader, PixelShader.GetPixelShader(), *PassParameters);

			RHICmdList.SetViewport(View.ViewRect.Min.X, View.ViewRect.Min.Y, 0.0f, View.ViewRect.Max.X, View.ViewRect.Max.Y, 1.0f);

			DrawRectangle(
				RHICmdList,
				0, 0,
				View.ViewRect.Width(), View.ViewRect.Height(),
				View.ViewRect.Min.X, View.ViewRect.Min.Y,
				View.ViewRect.Width(), View.ViewRect.Height(),
				FIntPoint(View.ViewRect.Width(), View.ViewRect.Height()),
				SceneContext.GetBufferSizeXY(),
				VertexShader
			);
		}
		);
	}
}
#endif