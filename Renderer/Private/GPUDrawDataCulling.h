#pragma once

#include "MeshMaterialShader.h"
#include "MeshPassProcessor.h"

// TEMP
const uint32 WaveThreadsCount = 64;
const uint32 MaxGroupThreadsCount = 256;
const uint32 MaxGroupPerDispatch = GMaxComputeDispatchDimension;


struct FCullingMaterialData
{
	const FMaterial* Material;
	const FMaterialRenderProxy* MaterialRenderProxy;
	const bool MaterialFullyOpaque;
};


class FGPUDrawDataCuller 
{
public:

	static FCullingMaterialData CreateCullingMaterialData(const FMaterial& MeshPassMaterial, const FMaterialRenderProxy& MaterialRenderProxy);

	static FMeshCullingContext CreateMeshCullingContext(
		const FMaterial* CullingMaterial,
		const EBlendMode& MeshDrawBlendMode,
		const FVertexFactory* VertexFactory,
		const FGraphicsMinimalPipelineStateInitializer& PipelineState, 
		const FMeshDrawCommand& MeshDrawCommand);

	void SetupCullingOverrides(FMeshCullingContext&& CullingContext, FMeshDrawCommand& MeshDrawCommand) const;
};
//
/*
template<typename T>
const static unsigned TBitsNum = sizeof(T)*8;
template<>
const static unsigned TBitsNum<bool> = 1;

template <typename T>
struct TMemberPointer
{};

template <typename T, typename U>
struct TMemberPointer<T U::*>
{
	using MemberType = T;
	using ClassType = U;
};

template<auto Field, unsigned BitsNum = sizeof(Field)>
class BitPacker
{
	const static unsigned TotalBits = 0;

protected:
	template<auto Mem>
	void SetValue(TMemberPointer<decltype(Mem)>::MemberType V)
	{
		
	};
};

template<auto Field, unsigned BitsNum, auto ... Rest>
class BitPacker : public BitPacker<Rest...>
{
	const static unsigned BitsOffset = BitPacker<Rest..., 0>::TotalBits;
	const static unsigned TotalBits = BitsNum + BitsOffset;

	
	void SetValue<Field>(TMemberPointer<decltype(Field)>::MemberType V )
	{
		
	}

};

*/
