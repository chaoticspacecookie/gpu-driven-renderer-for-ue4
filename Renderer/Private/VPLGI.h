/*
	Virtual point lights, used as a cheap substitution to dynamic GI
	This is a smaller footprint implementation compared to Epic one with LPVs that already has some
	VPLs but tied to the LPV system
*/
#pragma once

#include "CustomRenderer/DynamicGIGlobals.h"
#include "DynamicGI.h"
#include "ShaderParameterMacros.h"
#include "ShaderCore.h"
#include "RenderGraphResources.h"
#include "ShaderPermutation.h"

class FProjectedShadowInfo;
class FViewInfo;
class FSceneTexturesUniformParameters;
struct FSortedShadowMaps;

namespace FVirtualPointLight
{
	BEGIN_SHADER_PARAMETER_STRUCT(FCommonParameters, )
		SHADER_PARAMETER_STRUCT_INCLUDE(FDynamicGlobalIllumination::FRSMParameters,	RSMParameters)
		SHADER_PARAMETER(uint32, SampleCount)
	END_SHADER_PARAMETER_STRUCT()

	class FUseCachedDim : SHADER_PERMUTATION_BOOL("USE_CACHED_VPLS");

	/** Get whether the shaders are supported and compiled on the current platform and project */
	bool IsSupported();

	void SetupCommonParameters(FRDGBuilder& GraphBuilder, const FViewInfo& View, const FProjectedShadowInfo& ProjectedShadowInfo, FCommonParameters &OutParameters);
	bool ShouldCacheVPLs();

	void RenderLightsRasterized(FRDGBuilder& GraphBuilder, const FViewInfo& View, const FSortedShadowMaps& ShadowMaps);

	FRDGTextureUAVRef CopySceneColor(FRDGBuilder& GraphBuilder, const FViewInfo& View);
}
