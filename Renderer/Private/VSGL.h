#pragma once

#include "GlobalShader.h"
#include "ShaderParameterStruct.h"
#include "SceneRendering.h"
#include "SceneRenderTargets.h"
#include "ShadowRendering.h"
#include "ScenePrivate.h"

class FRDGBuilder;
class FViewInfo;
struct FSortedShadowMaps;

enum class ELightType
{
	Diffuse = 0,
	Specular,
	Num
};

namespace FVirtualSphericalGaussianLight
{
	bool IsSupported();

	void ApplyVSGL(FRDGBuilder& GraphBuilder, const FViewInfo& View, FRDGBufferRef SGLights);
	void RenderLights(FRDGBuilder& GraphBuilder, const FViewInfo& View, const FSortedShadowMaps& ShadowMaps);
	void GenerateVSGL(FRDGBuilder& GraphBuilder, const FViewInfo& View, FRDGBufferRef SGLights, ELightType LightType, const FProjectedShadowInfo* ProjectedShadowInfo);

	int32 GetMaxRSMResolution();
}
