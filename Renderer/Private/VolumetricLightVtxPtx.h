#pragma once

#include "CoreMinimal.h"
#include "GlobalShader.h"
#include "../SceneRendering.h"
#include "../LightSceneInfo.h"
#include "SceneView.h"
#include "PostProcess/CustomRenderer/VLSInfo.h"
#include "LightRendering.h"
#include "SceneRenderTargetParameters.h"
#include "PostProcess/SceneRenderTargets.h"
#include "ShadowRendering.h"
#include "ShaderParameterMacros.h"

//vertex shader to render a volumetric light
class FVolumetricLightVertexShader : public FGlobalShader
{
	DECLARE_SHADER_TYPE(FVolumetricLightVertexShader, Global);
public:

	static bool ShouldCompilePermutation(const FGlobalShaderPermutationParameters& Parameters)
	{
		return IsFeatureLevelSupported(Parameters.Platform, ERHIFeatureLevel::SM5);
	}

	static void ModifyCompilationEnvironment(const FGlobalShaderPermutationParameters& Parameters, FShaderCompilerEnvironment& OutEnvironment)
	{
		FGlobalShader::ModifyCompilationEnvironment(Parameters, OutEnvironment);
	}

	FVolumetricLightVertexShader() {}
	FVolumetricLightVertexShader(const ShaderMetaType::CompiledShaderInitializerType& Initializer) :
		FGlobalShader(Initializer)
	{
		StencilingGeometryParameters.Bind(Initializer.ParameterMap);
	}

	void SetParameters(FRHICommandList& RHICmdList, const FViewInfo& View, const FLightSceneInfo* LightSceneInfo)
	{
		FGlobalShader::SetParameters<FViewUniformShaderParameters>(RHICmdList, RHICmdList.GetBoundVertexShader(), View.ViewUniformBuffer);
		StencilingGeometryParameters.Set(RHICmdList, this, View, LightSceneInfo);
	}

	void SetSimpleLightParameters(FRHICommandList& RHICmdList, const FViewInfo& View, const FSphere& LightBounds)
	{
		FGlobalShader::SetParameters<FViewUniformShaderParameters>(RHICmdList, RHICmdList.GetBoundVertexShader(), View.ViewUniformBuffer);

		FVector4 StencilingSpherePosAndScale;
		StencilingGeometry::GStencilSphereVertexBuffer.CalcTransform(StencilingSpherePosAndScale, LightBounds, View.ViewMatrices.GetPreViewTranslation());
		StencilingGeometryParameters.Set(RHICmdList, this, StencilingSpherePosAndScale);
	}

private:

	LAYOUT_FIELD(FStencilingGeometryShaderParameters, StencilingGeometryParameters);
};

class FVolumetricLightFullScreenVertexShader : public FGlobalShader
{
	DECLARE_SHADER_TYPE(FVolumetricLightFullScreenVertexShader, Global);

	static bool ShouldCompilePermutation(const FGlobalShaderPermutationParameters& Parameters)
	{
		return true;
	}

	/** Default constructor. */
	FVolumetricLightFullScreenVertexShader() {}

	void SetParameters(FRHICommandList& RHICmdList, FRHIUniformBuffer* ViewUniformBuffer)
	{
		FGlobalShader::SetParameters<FViewUniformShaderParameters>(RHICmdList, RHICmdList.GetBoundVertexShader(), ViewUniformBuffer);
	}

public:

	/** Initialization constructor. */
	FVolumetricLightFullScreenVertexShader(const ShaderMetaType::CompiledShaderInitializerType& Initializer)
		: FGlobalShader(Initializer)
	{
	}
};

class FVolumetricLightPixelShader : public FGlobalShader
{
	DECLARE_GLOBAL_SHADER(FVolumetricLightPixelShader);
	SHADER_USE_PARAMETER_STRUCT(FVolumetricLightPixelShader, FGlobalShader)

	//permutations
	class VLSHasIES : SHADER_PERMUTATION_BOOL("USE_IES_PROFILE");
	class VLSUseShadows : SHADER_PERMUTATION_BOOL("USE_SHADOW");

	using FPermutationDomain = TShaderPermutationDomain<VLSHasIES, VLSUseShadows>;

public:

	BEGIN_SHADER_PARAMETER_STRUCT(FParameters, )
		SHADER_PARAMETER(FIntVector4, ViewDimensions)
		SHADER_PARAMETER(FIntPoint, RTDimensions)
		SHADER_PARAMETER(FMatrix, WorldToLight)
		SHADER_PARAMETER(FMatrix, LightToWorld)
		SHADER_PARAMETER(FMatrix, WorldToShadowMatrix)
		SHADER_PARAMETER(FLinearColor, LightColor)
		SHADER_PARAMETER(FVector4, LightPosition)
		SHADER_PARAMETER(FVector4, LightDirectionAndCosAngle)
		SHADER_PARAMETER(FVector4, LightSphereInfo)
		SHADER_PARAMETER(float, ShadowDepthBias)
		SHADER_PARAMETER(int, TextureRatio)
		SHADER_PARAMETER(FVector4, ShadowMinMax)
		SHADER_PARAMETER(FVector2D, VolMinScaleMaxDistance)
		SHADER_PARAMETER(FVector2D, LightAngles)
		SHADER_PARAMETER(FVector4, VLSInfo)
		SHADER_PARAMETER_TEXTURE(Texture2D, DepthDownscaled)
		SHADER_PARAMETER_SAMPLER(SamplerState, DepthDownscaledSampler)
		SHADER_PARAMETER_STRUCT_REF(FSceneTexturesUniformParameters, SceneTextures)
		SHADER_PARAMETER_STRUCT_REF(FViewUniformShaderParameters, View)
		SHADER_PARAMETER_TEXTURE(Texture2D, LightShadowMap)
		SHADER_PARAMETER_SAMPLER(SamplerState, LightShadowMapSampler)
		SHADER_PARAMETER_TEXTURE(Texture2D, IESTexture)
		SHADER_PARAMETER_SAMPLER(SamplerState, IESTextureSampler)
	END_SHADER_PARAMETER_STRUCT()

	static bool ShouldCompilePermutation(const FGlobalShaderPermutationParameters& Parameters)
	{
		return IsFeatureLevelSupported(Parameters.Platform, ERHIFeatureLevel::SM5);
	}

	static void ModifyCompilationEnvironment(const FGlobalShaderPermutationParameters& Parameters, FShaderCompilerEnvironment& OutEnvironment)
	{
		FGlobalShader::ModifyCompilationEnvironment(Parameters, OutEnvironment);
		OutEnvironment.SetRenderTargetOutputFormat(1, PF_R16F);
		OutEnvironment.CompilerFlags.Add(CFLAG_StandardOptimization);
		
	}

	void SetParameters(FRHICommandList& RHICmdList, TShaderMapRef<FVolumetricLightPixelShader> PixelShader, const FViewInfo& View, struct FVLSInfo& LightToProcess, bool bUseShadow, bool bHasIESProfile, float VolumetricLightScale);
};

class FVolumetricLightVtxPtxLauncher
{
public:

	static const bool s_HasIESProfile = true;
	static const bool s_HasShadows = true;

	FVolumetricLightVtxPtxLauncher();
	~FVolumetricLightVtxPtxLauncher();

	void Run(FRHICommandListImmediate& RHICmdList, const FViewInfo& View, struct FVLSInfo& CurrentLightInfo, const FLightSceneInfo* LightSceneInfo, float VolumetricLightScale);

protected:

	bool bRequiresInit;

};

//Depth downscale
class FDepthDownscaleCS : public FGlobalShader
{
	DECLARE_GLOBAL_SHADER(FDepthDownscaleCS);
	SHADER_USE_PARAMETER_STRUCT(FDepthDownscaleCS, FGlobalShader)

public:

	BEGIN_SHADER_PARAMETER_STRUCT(FParameters, )
		SHADER_PARAMETER_STRUCT_REF(FSceneTexturesUniformParameters, SceneTextures)
		SHADER_PARAMETER_STRUCT_REF(FViewUniformShaderParameters, View)
		SHADER_PARAMETER_UAV(RWTexture2D<float>, DownsampledDepth)
	END_SHADER_PARAMETER_STRUCT()


	static bool ShouldCompilePermutation(const FGlobalShaderPermutationParameters& Parameters)
	{
		return IsFeatureLevelSupported(Parameters.Platform, ERHIFeatureLevel::SM5);
	}

	static void ModifyCompilationEnvironment(const FGlobalShaderPermutationParameters& Parameters, FShaderCompilerEnvironment& OutEnvironment)
	{

	}

};