#include "VolumetricLightVtxPtx.h"
#include "Shader.h"
#include "PostProcess/SceneFilterRendering.h"
#include "PostProcess/PostProcessing.h"

static TAutoConsoleVariable<int32> CVarVolumetricLightFraction(TEXT("r.VolumetricLights.Fraction"), 2, TEXT("Define how much smaller is the volumetric light buffer in comparison with the main buffer."), ECVF_Scalability);
static TAutoConsoleVariable<int32> CVarVolumetricLightMaxSamples(TEXT("r.VolumetricLights.MaxSamples"), 15, TEXT("Caps to a maximum number the sample count for volumetric lights."), ECVF_Scalability);
static TAutoConsoleVariable<int32> CVarVolumetricLightEnableBlur(TEXT("r.VolumetricLights.EnableBlur"), 1, TEXT("Should we blur the volumetric lights?"), ECVF_Scalability);

void FVolumetricLightPixelShader::SetParameters(FRHICommandList& RHICmdList, TShaderMapRef<FVolumetricLightPixelShader> PixelShader, const FViewInfo& View, struct FVLSInfo& LightToProcess, bool bUseShadow, bool bHasIESProfile, float VolumetricLightScale)
{
	FParameters Parameters;

	FRHIPixelShader* ShaderRHI = RHICmdList.GetBoundPixelShader();

	Parameters.View = View.ViewUniformBuffer;
	Parameters.SceneTextures = CreateSceneTextureUniformBufferSingleDraw(RHICmdList, ESceneTextureSetupMode::SceneDepth, View.FeatureLevel);

	FSceneRenderTargets& RenterTargets = FSceneRenderTargets::Get(RHICmdList);

	Parameters.DepthDownscaled = RenterTargets.SmallDepthZ->GetRenderTargetItem().ShaderResourceTexture;
	Parameters.DepthDownscaledSampler = TStaticSamplerState<SF_Point, AM_Clamp, AM_Clamp, AM_Clamp>::GetRHI();

	int32 DividerInt = CVarVolumetricLightFraction.GetValueOnRenderThread();
	float DividerFloat = (float)DividerInt;

	FIntRect AdjustedViewSize = View.ViewRect / DividerFloat;

	Parameters.ViewDimensions = FIntVector4(AdjustedViewSize.Min.X, AdjustedViewSize.Min.Y, AdjustedViewSize.Size().X, AdjustedViewSize.Size().Y);
	Parameters.RTDimensions = RenterTargets.GetBufferSizeXY() / DividerInt;

	FRHISamplerState* SamplerLightMap = TStaticSamplerState<SF_Point, AM_Clamp, AM_Clamp, AM_Clamp>::GetRHI();

	if (bUseShadow)
	{
		FRHITexture* ShadowDepthTextureValue;
		ShadowDepthTextureValue = LightToProcess.VLProjectInfo->RenderTargets.DepthTarget->GetRenderTargetItem().ShaderResourceTexture.GetReference();
		Parameters.LightShadowMap = ShadowDepthTextureValue;
		Parameters.LightShadowMapSampler = SamplerLightMap;
	}

	FLightShaderParameters LightParams;
	LightToProcess.VLLightProxy->GetLightShaderParameters(LightParams);

	int32 MaxSamples = CVarVolumetricLightMaxSamples.GetValueOnRenderThread();

	FVector4 vlsInfoData;
	vlsInfoData.X = FMath::Min(LightToProcess.VLLightProxy->GetNumVLSRaySteps(), MaxSamples);
	vlsInfoData.Y = LightToProcess.VLLightProxy->GetVLIntensity() * VolumetricLightScale;
	vlsInfoData.Z = 0.0f;
	vlsInfoData.W = FMath::Tan(LightToProcess.VLLightProxy->GetOuterConeAngle());

	if (bHasIESProfile && LightToProcess.VLLightProxy->GetUseIESProfileMultiplier())
	{
		vlsInfoData.Y *= LightToProcess.VLLightProxy->GetIESTexture()->TextureMultiplier;
	}

	Parameters.VLSInfo = vlsInfoData;

	if (bUseShadow)
	{
		FVector4 ShadowmapMinMaxValue;
		FMatrix vlsLightProxyShadowMatrix = LightToProcess.VLProjectInfo->GetWorldToShadowMatrix(ShadowmapMinMaxValue);

		Parameters.ShadowDepthBias = LightToProcess.VLProjectInfo->GetShaderDepthBias();
		Parameters.WorldToShadowMatrix = vlsLightProxyShadowMatrix;
		Parameters.ShadowMinMax = ShadowmapMinMaxValue;
	}

	FMatrix WtL = LightToProcess.VLLightProxy->GetWorldToLight();
	FMatrix LtW = LightToProcess.VLLightProxy->GetLightToWorld();
	Parameters.WorldToLight = WtL;
	Parameters.LightToWorld = LtW;

	FLinearColor ProxyLightColor = LightToProcess.VLLightProxy->GetColor();
	if (LightToProcess.VLLightProxy->IsInverseSquared())
	{
		ProxyLightColor /= 16.0f;
	}

	Parameters.LightColor = ProxyLightColor * LightToProcess.VLLightProxy->GetIndirectLightingScale();
	Parameters.LightPosition = LightToProcess.VLLightProxy->GetPosition();


	FVector2D MinMaxVector(0.0f, LightToProcess.VLLightProxy->GetVLMaxDistance());
	Parameters.VolMinScaleMaxDistance = MinMaxVector;

	FVector4 LDCA(
		LightToProcess.VLLightProxy->GetDirection().X,
		LightToProcess.VLLightProxy->GetDirection().Y,
		LightToProcess.VLLightProxy->GetDirection().Z,
		FMath::Cos(LightToProcess.VLLightProxy->GetOuterConeAngle()));

	Parameters.LightDirectionAndCosAngle = LDCA;

	FSphere sphere = LightToProcess.VLLightProxy->GetBoundingSphere();
	FVector4 lightSphereInfoVec;

	LightToProcess.VLLightProxy->GetPosition();

	lightSphereInfoVec.X = LightToProcess.VLLightProxy->GetPosition().X;
	lightSphereInfoVec.Y = LightToProcess.VLLightProxy->GetPosition().Y;
	lightSphereInfoVec.Z = LightToProcess.VLLightProxy->GetPosition().Z;
	lightSphereInfoVec.W = LightToProcess.VLLightProxy->GetRadius();
	Parameters.LightSphereInfo = lightSphereInfoVec;

	Parameters.LightAngles = LightParams.SpotAngles;

	int32 Divider = CVarVolumetricLightFraction.GetValueOnRenderThread();
	Parameters.TextureRatio = Divider;

	FTexture* IESTextureResource = LightToProcess.VLLightProxy->GetIESTextureResource();

	FRHITexture* TextureRHI = IESTextureResource ? IESTextureResource->TextureRHI : GSystemTextures.WhiteDummy->GetRenderTargetItem().TargetableTexture;

	Parameters.IESTexture = TextureRHI;
	Parameters.IESTextureSampler = TStaticSamplerState<SF_Bilinear, AM_Clamp, AM_Clamp, AM_Clamp>::GetRHI();

	SetShaderParameters(RHICmdList, PixelShader, ShaderRHI, Parameters);
}


IMPLEMENT_SHADER_TYPE(, FVolumetricLightVertexShader, TEXT("/Engine/Private/CustomRenderer/VolumetricLightVertexShaderCUSTOM_RENDERER.usf"), TEXT("RadialVertexMain"), SF_Vertex);
IMPLEMENT_SHADER_TYPE(, FVolumetricLightFullScreenVertexShader, TEXT("/Engine/Private/CustomRenderer/VolumetricLightVertexShaderCUSTOM_RENDERER.usf"), TEXT("FullScreenMain"), SF_Vertex);

IMPLEMENT_GLOBAL_SHADER(FVolumetricLightPixelShader, "/Engine/Private/CustomRenderer/VolumetricLightPixelShaderCUSTOM_RENDERER.usf", "MainPS", SF_Pixel);
IMPLEMENT_GLOBAL_SHADER(FDepthDownscaleCS, "/Engine/Private/CustomRenderer/DepthDownscale.usf", "MainCS", SF_Compute);

FVolumetricLightVtxPtxLauncher::FVolumetricLightVtxPtxLauncher()
	: bRequiresInit(true)
{

}

FVolumetricLightVtxPtxLauncher::~FVolumetricLightVtxPtxLauncher()
{

}

static bool SphereConeIntersection(const FVector& ConeDir, const FVector& ConeOrigin, float ConeAngle, const FVector& SphereCenter, float SphereRadius)
{
	float ConeAngleSin;
	float ConeAngleCos;
	FMath::SinCos(&ConeAngleSin, &ConeAngleCos, ConeAngle);
	FVector U = ConeOrigin - (SphereRadius / ConeAngleSin) * ConeDir;
	FVector D = SphereCenter - U;

	float DSquare = FVector::DotProduct(D, D);
	float E = FVector::DotProduct(ConeDir, D);

	if ((E > 0.0f) && ((E*E) > DSquare * ConeAngleCos * ConeAngleCos))
	{
		D = SphereCenter - ConeOrigin;
		DSquare = FVector::DotProduct(D, D);
		E = -FVector::DotProduct(ConeDir, D);
		if ((E > 0.0f) && ((E*E) > DSquare * ConeAngleSin * ConeAngleSin))
		{
			return DSquare <= (SphereRadius * SphereRadius);
		}
		else
		{
			return true;
		}
	}
	return false;
}

/** Sets up rasterizer and depth state for rendering bounding geometry in a deferred pass. */
static bool SetBoundingGeometryRasterizerAndDepthStateForVolumetric(FGraphicsPipelineStateInitializer& GraphicsPSOInit, const FViewInfo& View, const FVLSInfo& LightInfo)
{
	//add proper calculation for the cone
	FSphere LightBounds = LightInfo.VLLightProxy->GetBoundingSphere();

	bool bCameraInsideLightGeometry = ((FVector)View.ViewMatrices.GetViewOrigin() - LightBounds.Center).SizeSquared() < FMath::Square(LightBounds.W * 1.05f + View.NearClippingDistance * 2.0f)
		// Always draw backfaces in ortho
		//@todo - accurate ortho camera / light intersection
		|| !View.IsPerspectiveProjection();

	if (bCameraInsideLightGeometry)
	{
		float NearHalfHeight = View.CachedViewUniformShaderParameters->ClipToView.M[1][1] * View.CachedViewUniformShaderParameters->NearPlane;
		float NearHalfWidth = View.CachedViewUniformShaderParameters->ClipToView.M[0][0] * View.CachedViewUniformShaderParameters->NearPlane;
		float SphereRadius = FMath::Max(NearHalfHeight, NearHalfWidth);
		SphereRadius = FMath::Sqrt(View.CachedViewUniformShaderParameters->NearPlane * View.CachedViewUniformShaderParameters->NearPlane + SphereRadius * SphereRadius);
		bCameraInsideLightGeometry = bCameraInsideLightGeometry && SphereConeIntersection(LightInfo.VLLightProxy->GetDirection(), LightInfo.VLLightProxy->GetPosition(), LightInfo.VLLightProxy->GetOuterConeAngle() + FMath::DegreesToRadians(3.0f), View.ViewMatrices.GetViewOrigin(), SphereRadius);
	}

	if (bCameraInsideLightGeometry)
	{
		// Render backfaces with depth tests disabled since the camera is inside (or close to inside) the light geometry
		GraphicsPSOInit.RasterizerState = TStaticRasterizerState<FM_Solid, CM_None>::GetRHI();
	}
	else
	{
		// Render frontfaces with depth tests on to get the speedup from HiZ since the camera is outside the light geometry
		GraphicsPSOInit.RasterizerState = View.bReverseCulling ? TStaticRasterizerState<FM_Solid, CM_CCW>::GetRHI() : TStaticRasterizerState<FM_Solid, CM_CW>::GetRHI();
	}

	GraphicsPSOInit.DepthStencilState =
		bCameraInsideLightGeometry
		? TStaticDepthStencilState<false, CF_Always>::GetRHI()
		: TStaticDepthStencilState<false, CF_DepthNearOrEqual>::GetRHI();

	return bCameraInsideLightGeometry;
}

template <bool bConeMeshPass>
static FRHIVertexDeclaration* GetDeferredLightingVertexDeclarationVolumetricLights()
{
	return bConeMeshPass ? GetVertexDeclarationFVector4() : GFilterVertexDeclaration.VertexDeclarationRHI;
}

void FVolumetricLightVtxPtxLauncher::Run(FRHICommandListImmediate& RHICmdList, const FViewInfo& View, FVLSInfo& CurrentLightInfo, const FLightSceneInfo* LightSceneInfo, float VolumetricLightScale)
{
	check(IsInRenderingThread());

	FSceneRenderTargets& RenterTargets = FSceneRenderTargets::Get(RHICmdList);

	float Divider = (float)CVarVolumetricLightFraction.GetValueOnRenderThread();

	FVolumetricLightPixelShader::FPermutationDomain PermutationVector;
	PermutationVector.Set<FVolumetricLightPixelShader::VLSHasIES>(CurrentLightInfo.VLLightProxy->GetIESTextureResource() != nullptr);
	PermutationVector.Set<FVolumetricLightPixelShader::VLSUseShadows>(CurrentLightInfo.VLLightProxy->RendersVolumetricShadow());

	TShaderMapRef<FVolumetricLightPixelShader> PixelShader(View.ShaderMap, PermutationVector);

	FGraphicsPipelineStateInitializer GraphicsPSOInit;
	RHICmdList.ApplyCachedRenderTargets(GraphicsPSOInit);
	GraphicsPSOInit.BlendState = TStaticBlendState<CW_RGBA, BO_Add, BF_One, BF_One, BO_Add, BF_One, BF_Zero>::GetRHI();
	GraphicsPSOInit.PrimitiveType = PT_TriangleList;
	GraphicsPSOInit.BoundShaderState.PixelShaderRHI = PixelShader.GetPixelShader();
	bool IsInCone = SetBoundingGeometryRasterizerAndDepthStateForVolumetric(GraphicsPSOInit, View, CurrentLightInfo);
	if (IsInCone)
	{
		TShaderMapRef<FVolumetricLightFullScreenVertexShader> VertexShader(View.ShaderMap);
		GraphicsPSOInit.BoundShaderState.VertexShaderRHI = VertexShader.GetVertexShader();
		GraphicsPSOInit.BoundShaderState.VertexDeclarationRHI = GetDeferredLightingVertexDeclarationVolumetricLights<false>();
		SetGraphicsPipelineState(RHICmdList, GraphicsPSOInit);

		VertexShader->SetParameters(RHICmdList, View.ViewUniformBuffer);
		PixelShader->SetParameters(RHICmdList, PixelShader, View, CurrentLightInfo, CurrentLightInfo.VLLightProxy->RendersVolumetricShadow(), CurrentLightInfo.VLLightProxy->GetIESTextureResource() != nullptr, VolumetricLightScale);
		// Set the device viewport for the view.
		RHICmdList.SetViewport(View.ViewRect.Min.X / Divider, View.ViewRect.Min.Y / Divider, 0.0f, View.ViewRect.Max.X / Divider, View.ViewRect.Max.Y / Divider, 1.0f);

		DrawRectangle(
			RHICmdList,
			0, 0,
			View.ViewRect.Width(), View.ViewRect.Height(),
			View.ViewRect.Min.X, View.ViewRect.Min.Y,
			View.ViewRect.Width(), View.ViewRect.Height(),
			View.ViewRect.Size(),
			FSceneRenderTargets::Get(RHICmdList).GetBufferSizeXY(),
			VertexShader,
			EDRF_UseTriangleOptimization);
	}
	else
	{
		TShaderMapRef<FVolumetricLightVertexShader> VertexShader(View.ShaderMap);
		GraphicsPSOInit.BoundShaderState.VertexShaderRHI = VertexShader.GetVertexShader();
		GraphicsPSOInit.BoundShaderState.VertexDeclarationRHI = GetDeferredLightingVertexDeclarationVolumetricLights<true>();
		SetGraphicsPipelineState(RHICmdList, GraphicsPSOInit);
		VertexShader->SetParameters(RHICmdList, View, LightSceneInfo);

		PixelShader->SetParameters(RHICmdList, PixelShader, View, CurrentLightInfo, CurrentLightInfo.VLLightProxy->RendersVolumetricShadow(), CurrentLightInfo.VLLightProxy->GetIESTextureResource() != nullptr, VolumetricLightScale);
		// Set the device viewport for the view.
		RHICmdList.SetViewport(View.ViewRect.Min.X / Divider, View.ViewRect.Min.Y / Divider, 0.0f, View.ViewRect.Max.X / Divider, View.ViewRect.Max.Y / Divider, 1.0f);

		StencilingGeometry::DrawCone(RHICmdList);
	}

}
