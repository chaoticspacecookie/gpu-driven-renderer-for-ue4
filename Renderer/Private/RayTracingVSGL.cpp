
#include "DeferredShadingRenderer.h"

#if RHI_RAYTRACING

#include "ClearQuad.h"
#include "SceneRendering.h"
#include "SceneRenderTargets.h"
#include "SceneUtils.h"
#include "RenderTargetPool.h"
#include "RHIResources.h"
#include "UniformBuffer.h"
#include "RHI/Public/PipelineStateCache.h"
#include "Raytracing/RaytracingOptions.h"
#include "Raytracing/RayTracingLighting.h"
#include "SceneTextureParameters.h"
#include "Raytracing/RayTracingSkyLight.h"
#include "PostProcess/PostProcessing.h"
#include "PostProcess/SceneFilterRendering.h"
#include "Raytracing/RaytracingOptions.h"
#include "BlueNoise.h"
#include "PathTracingUniformBuffers.h"
#include "ShaderParameterStruct.h"
#include "VSGL.h"

DECLARE_GPU_STAT_NAMED(RayTracingVSGL, TEXT("Ray Tracing VSGL"));

static TAutoConsoleVariable<float> CVarRayTracingRenderVSGLScaleFactor(
	TEXT("r.RayTracing.VSGL.ScaleFactor"),
	1.0f,
	TEXT("Spotlight dynamic GI Scale Factor"));

static TAutoConsoleVariable<int32> CVarRayTracingRenderVSGLSamplePerPixel(
	TEXT("r.RayTracing.VSGL.SamplePerPixel"),
	1,
	TEXT("Sample per pixel on denoiser"));

static TAutoConsoleVariable<int32> CVarUseVSGLDenoiser(
	TEXT("r.RayTracing.VSGL.Denoiser"),
	2,
	TEXT("Choose the denoising algorithm.\n")
	TEXT(" 0: Disabled;\n")
	TEXT(" 1: Forces the default denoiser of the renderer;\n")
	TEXT(" 2: GScreenSpaceDenoiser witch may be overriden by a third party plugin (default)."),
	ECVF_RenderThreadSafe);

struct FSphericalGaussianLight
{
	FVector Position;
	float  VarianceInv;
	FVector Coefficient;
	float  Sharpness;
	FVector Axis;
	float	Padding;
};

class FRayTracingVSGLRGS : public FGlobalShader
{
	DECLARE_GLOBAL_SHADER(FRayTracingVSGLRGS)
	SHADER_USE_ROOT_PARAMETER_STRUCT(FRayTracingVSGLRGS, FGlobalShader)

	class FUseAttenuationTermDim : SHADER_PERMUTATION_BOOL("USE_ATTENUATION_TERM");
	class FEnableTwoSidedGeometryDim : SHADER_PERMUTATION_BOOL("ENABLE_TWO_SIDED_GEOMETRY");

	using FPermutationDomain = TShaderPermutationDomain<FUseAttenuationTermDim, FEnableTwoSidedGeometryDim>;

	static bool ShouldCompilePermutation(const FGlobalShaderPermutationParameters& Parameters)
	{
		return ShouldCompileRayTracingShadersForProject(Parameters.Platform);
	}

	BEGIN_SHADER_PARAMETER_STRUCT(FParameters, )
		SHADER_PARAMETER(uint32, SamplesPerPixel)
		SHADER_PARAMETER(uint32, MaxBounces)
		SHADER_PARAMETER(uint32, UpscaleFactor)
		SHADER_PARAMETER(float, MaxRayDistanceForGI)
		SHADER_PARAMETER(float, MaxRayDistanceForAO)
		SHADER_PARAMETER(float, NextEventEstimationSamples)
		SHADER_PARAMETER(float, DiffuseThreshold)
		SHADER_PARAMETER(float, MaxNormalBias)
		SHADER_PARAMETER(uint32, TileOffsetX)
		SHADER_PARAMETER(uint32, TileOffsetY)

		SHADER_PARAMETER_SRV(RaytracingAccelerationStructure, TLAS)
		SHADER_PARAMETER_STRUCT_INCLUDE(FSceneTextureParameters, SceneTextures)
		SHADER_PARAMETER_STRUCT_REF(FSceneTexturesUniformParameters, SceneTexturesStruct)
		//SHADER_PARAMETER_RDG_TEXTURE_UAV(RWTexture2D<float4>, RWSceneColorTexture)
		SHADER_PARAMETER_RDG_TEXTURE_UAV(RWTexture2D<float4>, RWOcclusionMaskUAV)
		SHADER_PARAMETER_RDG_TEXTURE_UAV(RWTexture2D<float4>, RWRayDistanceUAV)
		SHADER_PARAMETER_RDG_BUFFER_SRV(StructuredBuffer<FSphericalGaussianLight>, SGLights)

		SHADER_PARAMETER_STRUCT_REF(FViewUniformShaderParameters, ViewUniformBuffer)
	END_SHADER_PARAMETER_STRUCT()
};

IMPLEMENT_GLOBAL_SHADER(FRayTracingVSGLRGS, "/Engine/Private/CustomRenderer/RayTracingVSGL.usf", "VSGL", SF_RayGen);

void FDeferredShadingSceneRenderer::PrepareRayTracingVSGL(const FViewInfo& View, TArray<FRHIRayTracingShader*>& OutRayGenShaders)
{
	FRayTracingVSGLRGS::FPermutationDomain PermutationVector2;
	for (uint32 TwoSidedGeometryIndex = 0; TwoSidedGeometryIndex < 2; ++TwoSidedGeometryIndex)
	{
		for (uint32 EnableMaterialsIndex = 0; EnableMaterialsIndex < 2; ++EnableMaterialsIndex)
		{
			PermutationVector2.Set<FRayTracingVSGLRGS::FEnableTwoSidedGeometryDim>(TwoSidedGeometryIndex != 0);
			TShaderMapRef<FRayTracingVSGLRGS> RayGenerationShader(View.ShaderMap, PermutationVector2);
			OutRayGenShaders.Add(RayGenerationShader.GetRayTracingShader());
		}
	}
}

void FDeferredShadingSceneRenderer::DenoiseRayTracingVSGL(FRDGBuilder& GraphBuilder, FViewInfo& View, FRDGTextureRef& OutputColor, FRDGTextureRef& OutputHitDistance, TRefCountPtr<IPooledRenderTarget>& OutTexture)
{
	FSceneTextureParameters SceneTextureParams;
	SetupSceneTextureParameters(GraphBuilder, &SceneTextureParams);

	FSceneRenderTargets& SceneContext = FSceneRenderTargets::Get(GraphBuilder.RHICmdList);

	IScreenSpaceDenoiser::FDiffuseIndirectInputs DenoiserInputs;
	DenoiserInputs.Color = OutputColor;
	DenoiserInputs.RayHitDistance = OutputHitDistance;

	const IScreenSpaceDenoiser* DefaultDenoiser = IScreenSpaceDenoiser::GetDefaultDenoiser();
	const IScreenSpaceDenoiser* DenoiserToUse = DefaultDenoiser;

	RDG_EVENT_SCOPE(GraphBuilder, "%s%s(VSGL) %dx%d",
		DenoiserToUse != DefaultDenoiser ? TEXT("ThirdParty ") : TEXT(""),
		DenoiserToUse->GetDebugName(),
		View.ViewRect.Width(), View.ViewRect.Height());

	IScreenSpaceDenoiser::FAmbientOcclusionRayTracingConfig RayTracingConfig;
	static IConsoleVariable* CVar = IConsoleManager::Get().FindConsoleVariable(TEXT("r.VPL.Denoiser.ReconstructionSamples"));

	float ResolutionFraction = FMath::Clamp(CVarRayTracingRenderVSGLScaleFactor.GetValueOnAnyThread(), 0.25f, 1.0f);

	int32 RayTracingGISamplesPerPixel = CVar->GetInt();
	RayTracingConfig.ResolutionFraction = ResolutionFraction;
	RayTracingConfig.RayCountPerPixel = RayTracingGISamplesPerPixel;

	int32 UpscaleFactor = int32(1.0 / RayTracingConfig.ResolutionFraction);

	IScreenSpaceDenoiser::FVPLGIOutput DenoiserOutputs;
	//DenoiserOutputs.VPLGI = SkyLightTexture;
	DenoiserOutputs = DenoiserToUse->DenoiseVPLGI(
		GraphBuilder,
		View,
		&View.PrevViewInfo,
		SceneTextureParams,
		DenoiserInputs,
		RayTracingConfig);

	GraphBuilder.QueueTextureExtraction(DenoiserOutputs.VPLGI, &OutTexture);
}

void FDeferredShadingSceneRenderer::RenderRayTracingVSGL(FRDGBuilder& GraphBuilder, FSceneTextureParameters& SceneTextures, FViewInfo& View, FRDGBufferRef SGLights, FRDGTextureRef* OutAmbientOcclusionTexture)
{
	RDG_GPU_STAT_SCOPE(GraphBuilder, RayTracingVSGL);
	RDG_EVENT_SCOPE(GraphBuilder, "Ray Tracing VSGL");

	FSceneRenderTargets& SceneContext = FSceneRenderTargets::Get(GraphBuilder.RHICmdList);

	int32 GatherSamples = 8;
	int32 SamplesPerPixel = 1;

	uint32 IterationCount = SamplesPerPixel;
	uint32 SequenceCount = 1;
	uint32 DimensionCountX = SceneTextures.SceneDepthBuffer->Desc.Extent.X;
	uint32 DimensionCountY = SceneTextures.SceneDepthBuffer->Desc.Extent.Y;
	
	FRayTracingVSGLRGS::FPermutationDomain PermutationVector;
	PermutationVector.Set<FRayTracingVSGLRGS::FUseAttenuationTermDim>(true);
	PermutationVector.Set<FRayTracingVSGLRGS::FEnableTwoSidedGeometryDim>(true);
	TShaderMapRef<FRayTracingVSGLRGS> RayGenerationShader(GetGlobalShaderMap(FeatureLevel), PermutationVector);


	float ResolutionFraction = FMath::Clamp(CVarRayTracingRenderVSGLScaleFactor.GetValueOnAnyThread(), 0.25f, 1.0f);
	
	// Allocates denoiser inputs.
	IScreenSpaceDenoiser::FAmbientOcclusionInputs DenoiserInputs;
	{
		FRDGTextureDesc Desc = FRDGTextureDesc::Create2DDesc(
			SceneTextures.SceneDepthBuffer->Desc.Extent,
			PF_R16F,
			FClearValueBinding::None,
			/* InFlags = */ TexCreate_None,
			/* InTargetableFlags = */ TexCreate_ShaderResource | TexCreate_RenderTargetable | TexCreate_UAV,
			/* bInForceSeparateTargetAndShaderResource = */ false);
		DenoiserInputs.Mask = GraphBuilder.CreateTexture(Desc, TEXT("RayTracingAmbientOcclusion"));
		DenoiserInputs.RayHitDistance = GraphBuilder.CreateTexture(Desc, TEXT("RayTracingAmbientOcclusionHitDistance"));
	}


	int32 UpscaleFactor = int32(1.0 / ResolutionFraction);

	FRayTracingVSGLRGS::FParameters *PassParameters = GraphBuilder.AllocParameters<FRayTracingVSGLRGS::FParameters>();
	PassParameters->SamplesPerPixel = 8;
	PassParameters->MaxBounces = 1;
	PassParameters->UpscaleFactor = UpscaleFactor;
	PassParameters->MaxRayDistanceForGI = 1.0e27;
	PassParameters->MaxRayDistanceForAO = 1.0e27;
	PassParameters->NextEventEstimationSamples = 1.0f;
	PassParameters->DiffuseThreshold = 0.0f;
	PassParameters->MaxNormalBias = 1.0f;
	PassParameters->SceneTextures = SceneTextures;
	PassParameters->SceneTexturesStruct =  CreateSceneTextureUniformBuffer(SceneContext, View.FeatureLevel, ESceneTextureSetupMode::All, EUniformBufferUsage::UniformBuffer_SingleFrame);
	//PassParameters->RWSceneColorTexture = GraphBuilder.CreateUAV(OutputColor);
	PassParameters->RWOcclusionMaskUAV = GraphBuilder.CreateUAV(DenoiserInputs.Mask);
	PassParameters->RWRayDistanceUAV = GraphBuilder.CreateUAV(DenoiserInputs.RayHitDistance);
	// Global
	PassParameters->TLAS = View.RayTracingScene.RayTracingSceneRHI->GetShaderResourceView();
	PassParameters->ViewUniformBuffer = View.ViewUniformBuffer;
	PassParameters->SGLights = GraphBuilder.CreateSRV(SGLights);
	
	//ResolveSceneColor(RHICmdList);

	FIntPoint LocalGatherPointsResolution = View.ViewRect.Size();
	FIntPoint RayTracingResolution = FIntPoint::DivideAndRoundUp(View.ViewRect.Size(), UpscaleFactor);

	GraphBuilder.AddPass(
		RDG_EVENT_NAME("VSGL %d%d", LocalGatherPointsResolution.X, LocalGatherPointsResolution.Y),
		PassParameters,
		ERDGPassFlags::Compute,
		[PassParameters, this, &View, RayGenerationShader, RayTracingResolution, LocalGatherPointsResolution, DimensionCountX, DimensionCountY](FRHICommandList& RHICmdList)
	{
		FRHIRayTracingScene* RayTracingSceneRHI = View.RayTracingScene.RayTracingSceneRHI;
		FRayTracingShaderBindingsWriter GlobalResources;
		SetShaderParameters(GlobalResources, RayGenerationShader, *PassParameters);
		RHICmdList.RayTraceDispatch(View.RayTracingMaterialPipeline, RayGenerationShader.GetRayTracingShader(), RayTracingSceneRHI, GlobalResources, RayTracingResolution.X, RayTracingResolution.Y);
	});

	int32 DenoiserMode = CVarUseVSGLDenoiser.GetValueOnRenderThread();
	int32 SamplePerPixel = CVarRayTracingRenderVSGLSamplePerPixel.GetValueOnRenderThread();
	const IScreenSpaceDenoiser* DefaultDenoiser = IScreenSpaceDenoiser::GetDefaultDenoiser();
	const IScreenSpaceDenoiser* DenoiserToUse = DenoiserMode == 1 ? DefaultDenoiser : GScreenSpaceDenoiser;

	IScreenSpaceDenoiser::FAmbientOcclusionRayTracingConfig RayTracingConfig;
	RayTracingConfig.RayCountPerPixel = SamplePerPixel;// GRayTracingAmbientOcclusionSamplesPerPixel >= 0 ? GRayTracingAmbientOcclusionSamplesPerPixel : View.FinalPostProcessSettings.RayTracingAOSamplesPerPixel;
	RayTracingConfig.ResolutionFraction = ResolutionFraction;
	FSceneTextureParameters SceneTextureParams;
	SetupSceneTextureParameters(GraphBuilder, &SceneTextureParams);

	IScreenSpaceDenoiser::FAmbientOcclusionOutputs DenoiserOutputs = DenoiserToUse->DenoiseAmbientOcclusion(
		GraphBuilder,
		View,
		&View.PrevViewInfo,
		SceneTextureParams,
		DenoiserInputs,
		RayTracingConfig);

	*OutAmbientOcclusionTexture = DenoiserOutputs.AmbientOcclusionMask;

}

class FCompositeVSGLPS : public FGlobalShader
{
	DECLARE_GLOBAL_SHADER(FCompositeVSGLPS)
	SHADER_USE_PARAMETER_STRUCT(FCompositeVSGLPS, FGlobalShader)

		static bool ShouldCompilePermutation(const FGlobalShaderPermutationParameters& Parameters)
	{
		return ShouldCompileRayTracingShadersForProject(Parameters.Platform);
	}

	BEGIN_SHADER_PARAMETER_STRUCT(FParameters, )
		RENDER_TARGET_BINDING_SLOTS()
		SHADER_PARAMETER_RDG_TEXTURE(Texture2D, VPLGITexture)
		SHADER_PARAMETER_SAMPLER(SamplerState, VPLGITextureSampler)
		SHADER_PARAMETER_STRUCT_REF(FViewUniformShaderParameters, ViewUniformBuffer)

		SHADER_PARAMETER_STRUCT_INCLUDE(FSceneTextureParameters, SceneTextures)
		END_SHADER_PARAMETER_STRUCT()
};

IMPLEMENT_GLOBAL_SHADER(FCompositeVSGLPS, "/Engine/Private/CustomRenderer/RayTracingVSGLComposite.usf", "CompositeVSGL", SF_Pixel);

void FDeferredShadingSceneRenderer::CompositeRayTracingVSGL(FRDGBuilder& GraphBuilder, FRDGTextureRef& VSGLRT, bool bFinalPass)
{
	FSceneRenderTargets& SceneContext = FSceneRenderTargets::Get(GraphBuilder.RHICmdList);

	//ResolveSceneColor(RHICmdList);
	for (FViewInfo& View : Views)
	{
		FSceneTextureParameters SceneTextures;
		SetupSceneTextureParameters(GraphBuilder, &SceneTextures);

		FCompositeVSGLPS::FParameters *PassParameters = GraphBuilder.AllocParameters<FCompositeVSGLPS::FParameters>();
		PassParameters->VPLGITexture = VSGLRT;// GraphBuilder.RegisterExternalTexture(VSGLRT);
		PassParameters->VPLGITextureSampler = TStaticSamplerState<SF_Point, AM_Clamp, AM_Clamp, AM_Clamp>::GetRHI();
		PassParameters->ViewUniformBuffer = View.ViewUniformBuffer;

		if (bFinalPass)
		{
			PassParameters->RenderTargets[0] = FRenderTargetBinding(GraphBuilder.RegisterExternalTexture(SceneContext.GetSceneColor()), ERenderTargetLoadAction::ELoad);
		}
		else
		{
			PassParameters->RenderTargets[0] = FRenderTargetBinding(GraphBuilder.RegisterExternalTexture(SceneContext.GetSceneColor()), ERenderTargetLoadAction::ELoad);

			//			PassParameters->RenderTargets[0] = FRenderTargetBinding(GraphBuilder.RegisterExternalTexture(OutputData), ERenderTargetLoadAction::ELoad);
		}

		PassParameters->SceneTextures = SceneTextures;

		// dxr_todo: Unify with RTGI compositing workflow
		GraphBuilder.AddPass(
			RDG_EVENT_NAME("GlobalIlluminationComposite"),
			PassParameters,
			ERDGPassFlags::Raster,
			[this, &SceneContext, &View, PassParameters](FRHICommandListImmediate& RHICmdList)
		{
			TShaderMapRef<FPostProcessVS> VertexShader(View.ShaderMap);
			TShaderMapRef<FCompositeVSGLPS> PixelShader(View.ShaderMap);
			FGraphicsPipelineStateInitializer GraphicsPSOInit;
			RHICmdList.ApplyCachedRenderTargets(GraphicsPSOInit);

			// Additive blending
			GraphicsPSOInit.BlendState = TStaticBlendState<CW_RGBA, BO_Add, BF_One, BF_SourceAlpha, BO_Add, BF_Zero, BF_SourceAlpha>::GetRHI();
			GraphicsPSOInit.RasterizerState = TStaticRasterizerState<FM_Solid, CM_None>::GetRHI();
			GraphicsPSOInit.DepthStencilState = TStaticDepthStencilState<false, CF_Always>::GetRHI();

			GraphicsPSOInit.BoundShaderState.VertexDeclarationRHI = GFilterVertexDeclaration.VertexDeclarationRHI;
			GraphicsPSOInit.BoundShaderState.VertexShaderRHI = VertexShader.GetVertexShader();
			GraphicsPSOInit.BoundShaderState.PixelShaderRHI = PixelShader.GetPixelShader();
			GraphicsPSOInit.PrimitiveType = PT_TriangleList;
			SetGraphicsPipelineState(RHICmdList, GraphicsPSOInit);

			SetShaderParameters(RHICmdList, PixelShader, PixelShader.GetPixelShader(), *PassParameters);

			RHICmdList.SetViewport(View.ViewRect.Min.X, View.ViewRect.Min.Y, 0.0f, View.ViewRect.Max.X, View.ViewRect.Max.Y, 1.0f);

			DrawRectangle(
				RHICmdList,
				0, 0,
				View.ViewRect.Width(), View.ViewRect.Height(),
				View.ViewRect.Min.X, View.ViewRect.Min.Y,
				View.ViewRect.Width(), View.ViewRect.Height(),
				FIntPoint(View.ViewRect.Width(), View.ViewRect.Height()),
				SceneContext.GetBufferSizeXY(),
				VertexShader
			);
		}
		);
	}
}
#endif // RHI_RAYTRACING
