
#include "DeferredShadingRenderer.h"

#if RHI_RAYTRACING

#include "ClearQuad.h"
#include "SceneRendering.h"
#include "SceneRenderTargets.h"
#include "SceneUtils.h"
#include "RenderTargetPool.h"
#include "RHIResources.h"
#include "UniformBuffer.h"
#include "RHI/Public/PipelineStateCache.h"
#include "Raytracing/RaytracingOptions.h"
#include "Raytracing/RayTracingLighting.h"
#include "SceneTextureParameters.h"
#include "Raytracing/RayTracingSkyLight.h"
#include "PostProcess/PostProcessing.h"
#include "PostProcess/SceneFilterRendering.h"
#include "Raytracing/RaytracingOptions.h"
#include "BlueNoise.h"
#include "PathTracingUniformBuffers.h"
#include "ShaderParameterStruct.h"
#include "VPLGI.h"

DECLARE_GPU_STAT_NAMED(RayTracingVPL, TEXT("Ray Tracing VPL"));

static TAutoConsoleVariable<float> CVarRayTracingRenderVPLScaleFactor(
	TEXT("r.RayTracing.VPL.ScaleFactor"),
	1.0f,
	TEXT("Spotlight dynamic GI Scale Factor"));

struct CustomRendererVPL
{
	FVector WorldPosition;
	FVector Normal;
	FVector Flux;
	float Depth;
};

class FClearGIBuffer : public FGlobalShader
{
	DECLARE_GLOBAL_SHADER(FClearGIBuffer)
	SHADER_USE_PARAMETER_STRUCT(FClearGIBuffer, FGlobalShader)

	BEGIN_SHADER_PARAMETER_STRUCT(FParameters, )
		SHADER_PARAMETER_RDG_TEXTURE_UAV(RWTexture2D<float4>, RWColor)
		SHADER_PARAMETER_RDG_TEXTURE_UAV(RWTexture2D<float2>, RWHitDistanceUAV)
	END_SHADER_PARAMETER_STRUCT()

	static bool ShouldCompilePermutation(const FGlobalShaderPermutationParameters& Parameters)
	{
		return IsFeatureLevelSupported(Parameters.Platform, ERHIFeatureLevel::SM5) && FVirtualPointLight::IsSupported();
	}

	static FIntPoint GetThreadGroupSize()
	{
		return FIntPoint(16, 16);
	}

	static void ModifyCompilationEnvironment(const FGlobalShaderPermutationParameters& Parameters, FShaderCompilerEnvironment& OutEnvironment)
	{
		FGlobalShader::ModifyCompilationEnvironment(Parameters, OutEnvironment);
		OutEnvironment.SetDefine(TEXT("THREADGROUP_SIZE_X"), GetThreadGroupSize().X);
		OutEnvironment.SetDefine(TEXT("THREADGROUP_SIZE_Y"), GetThreadGroupSize().Y);
	}
};

IMPLEMENT_GLOBAL_SHADER(FClearGIBuffer, "/Engine/Private/CustomRenderer/RayTracingVPLComposite.usf", "ClearGIBuffer", SF_Compute);


class FRayTracingVPLHITRGS : public FGlobalShader
{
	DECLARE_GLOBAL_SHADER(FRayTracingVPLHITRGS)
	SHADER_USE_ROOT_PARAMETER_STRUCT(FRayTracingVPLHITRGS, FGlobalShader)

	class FUseAttenuationTermDim : SHADER_PERMUTATION_BOOL("USE_ATTENUATION_TERM");
	class FEnableTwoSidedGeometryDim : SHADER_PERMUTATION_BOOL("ENABLE_TWO_SIDED_GEOMETRY");

	using FPermutationDomain = TShaderPermutationDomain<FUseAttenuationTermDim, FEnableTwoSidedGeometryDim>;

	static bool ShouldCompilePermutation(const FGlobalShaderPermutationParameters& Parameters)
	{
		return ShouldCompileRayTracingShadersForProject(Parameters.Platform);
	}

	BEGIN_SHADER_PARAMETER_STRUCT(FParameters, )
		SHADER_PARAMETER_RDG_BUFFER_UAV(RWStructuredBuffer<FVirtualPointLight::FVirtualPointLight>, HitList)

		// Sampling sequence
		SHADER_PARAMETER_STRUCT_REF(FHaltonIteration, HaltonIteration)
		SHADER_PARAMETER_STRUCT_REF(FHaltonPrimes, HaltonPrimes)
		SHADER_PARAMETER_STRUCT_REF(FBlueNoise, BlueNoise)
		SHADER_PARAMETER(uint32, UpscaleFactor)
		SHADER_PARAMETER_STRUCT_INCLUDE(FDynamicGlobalIllumination::FLightParameters, LightParameters)

		SHADER_PARAMETER_STRUCT_REF(FViewUniformShaderParameters, ViewUniformBuffer)
		
		SHADER_PARAMETER_SRV(RaytracingAccelerationStructure, TLAS)
		SHADER_PARAMETER_STRUCT_INCLUDE(FSceneTextureParameters, SceneTextures)
	END_SHADER_PARAMETER_STRUCT()
};

IMPLEMENT_GLOBAL_SHADER(FRayTracingVPLHITRGS, "/Engine/Private/CustomRenderer/RayTracingVPL.usf", "VPLHIT", SF_RayGen);

class FRayTracingVPLRGS : public FGlobalShader
{
	DECLARE_GLOBAL_SHADER(FRayTracingVPLRGS)
	SHADER_USE_ROOT_PARAMETER_STRUCT(FRayTracingVPLRGS, FGlobalShader)

		class FUseAttenuationTermDim : SHADER_PERMUTATION_BOOL("USE_ATTENUATION_TERM");
	class FEnableTwoSidedGeometryDim : SHADER_PERMUTATION_BOOL("ENABLE_TWO_SIDED_GEOMETRY");

	using FPermutationDomain = TShaderPermutationDomain<FUseAttenuationTermDim, FEnableTwoSidedGeometryDim>;

	static bool ShouldCompilePermutation(const FGlobalShaderPermutationParameters& Parameters)
	{
		return ShouldCompileRayTracingShadersForProject(Parameters.Platform);
	}

	BEGIN_SHADER_PARAMETER_STRUCT(FParameters, )
		SHADER_PARAMETER(uint32, SamplesPerPixel)
		SHADER_PARAMETER(uint32, MaxBounces)
		SHADER_PARAMETER(uint32, UpscaleFactor)
		SHADER_PARAMETER(float, MaxRayDistanceForGI)
		SHADER_PARAMETER(float, MaxRayDistanceForAO)
		SHADER_PARAMETER(float, NextEventEstimationSamples)
		SHADER_PARAMETER(float, DiffuseThreshold)
		SHADER_PARAMETER(float, MaxNormalBias)
		SHADER_PARAMETER(uint32, TileOffsetX)
		SHADER_PARAMETER(uint32, TileOffsetY)
		SHADER_PARAMETER_RDG_BUFFER_SRV(StructuredBuffer<FVirtualPointLight::FVirtualPointLight>, HitList2)

		SHADER_PARAMETER_SRV(RaytracingAccelerationStructure, TLAS)

		SHADER_PARAMETER_RDG_TEXTURE_UAV(RWTexture2D<float4>, RWColor)
		SHADER_PARAMETER_RDG_TEXTURE_UAV(RWTexture2D<float2>, RWHitDistanceUAV)

		SHADER_PARAMETER_STRUCT_INCLUDE(FDynamicGlobalIllumination::FLightParameters, LightParameters)

		// Sampling sequence
		SHADER_PARAMETER_STRUCT_REF(FHaltonIteration, HaltonIteration)
		SHADER_PARAMETER_STRUCT_REF(FHaltonPrimes, HaltonPrimes)
		SHADER_PARAMETER_STRUCT_REF(FBlueNoise, BlueNoise)

		SHADER_PARAMETER_STRUCT_REF(FViewUniformShaderParameters, ViewUniformBuffer)
		SHADER_PARAMETER_STRUCT_INCLUDE(FSceneTextureParameters, SceneTextures)
		END_SHADER_PARAMETER_STRUCT()
};

IMPLEMENT_GLOBAL_SHADER(FRayTracingVPLRGS, "/Engine/Private/CustomRenderer/RayTracingVPL.usf", "VPL", SF_RayGen);

void FDeferredShadingSceneRenderer::PrepareRayTracingVPL(const FViewInfo& View, TArray<FRHIRayTracingShader*>& OutRayGenShaders)
{
	FRayTracingVPLHITRGS::FPermutationDomain PermutationVector;
	for (uint32 TwoSidedGeometryIndex = 0; TwoSidedGeometryIndex < 2; ++TwoSidedGeometryIndex)
	{
		for (uint32 EnableMaterialsIndex = 0; EnableMaterialsIndex < 2; ++EnableMaterialsIndex)
		{
			PermutationVector.Set<FRayTracingVPLHITRGS::FEnableTwoSidedGeometryDim>(TwoSidedGeometryIndex != 0);
			TShaderMapRef<FRayTracingVPLHITRGS> RayGenerationShader(View.ShaderMap, PermutationVector);
			OutRayGenShaders.Add(RayGenerationShader.GetRayTracingShader());
		}
	}
	
	FRayTracingVPLRGS::FPermutationDomain PermutationVector2;
	for (uint32 TwoSidedGeometryIndex = 0; TwoSidedGeometryIndex < 2; ++TwoSidedGeometryIndex)
	{
		for (uint32 EnableMaterialsIndex = 0; EnableMaterialsIndex < 2; ++EnableMaterialsIndex)
		{
			PermutationVector2.Set<FRayTracingVPLRGS::FEnableTwoSidedGeometryDim>(TwoSidedGeometryIndex != 0);
			TShaderMapRef<FRayTracingVPLRGS> RayGenerationShader(View.ShaderMap, PermutationVector2);
			OutRayGenShaders.Add(RayGenerationShader.GetRayTracingShader());
		}
	}
}

void FDeferredShadingSceneRenderer::RenderRayTracingVPL(FRHICommandListImmediate& RHICmdList, FRDGBuilder& GraphBuilder, FViewInfo& View, TRefCountPtr<IPooledRenderTarget>& OutTexture, FRDGTextureRef& OutputColor, FRDGTextureRef& OutputHitDistance)
{
	FSceneRenderTargets& SceneContext = FSceneRenderTargets::Get(RHICmdList);

	FRDGBufferDesc BufferDesc = FRDGBufferDesc::CreateStructuredDesc(sizeof(FVector4) * 3, 256*256);
	FRDGBufferRef HitListParameter = GraphBuilder.CreateBuffer(BufferDesc, TEXT("HitList"), ERDGResourceFlags::None);

	for (auto Light : Scene->Lights)
	{
		if (Light.LightSceneInfo->Proxy->AffectsDynamicIndirectLighting())
		{
			RenderRayTracingVPLHIT(Light, RHICmdList, GraphBuilder, View, HitListParameter);
			RenderRayTracingVPL(Light, RHICmdList, GraphBuilder, View, HitListParameter, OutTexture, OutputColor, OutputHitDistance);
		}
	}

	SceneContext.bScreenSpaceAOIsValid = true;
}

bool FDeferredShadingSceneRenderer::RenderRayTracingVPLHIT(FLightSceneInfoCompact& Light, FRHICommandListImmediate& RHICmdList, FRDGBuilder& GraphBuilder, FViewInfo& View, FRDGBufferRef& HitList)
{
	RDG_GPU_STAT_SCOPE(GraphBuilder, RayTracingVPL);
	RDG_EVENT_SCOPE(GraphBuilder, "Ray Tracing VPL Hit");

	FSceneRenderTargets& SceneContext = FSceneRenderTargets::Get(RHICmdList);

	FSceneViewState* SceneViewState = (FSceneViewState*)View.State;
	if (!SceneViewState)
	{
		return false;
	}

	bool bLightsProcessed = false;


	FRayTracingVPLHITRGS::FPermutationDomain PermutationVector;
	PermutationVector.Set<FRayTracingVPLHITRGS::FUseAttenuationTermDim>(true);
	PermutationVector.Set<FRayTracingVPLHITRGS::FEnableTwoSidedGeometryDim>(true);
	TShaderMapRef<FRayTracingVPLHITRGS> RayGenerationShader(GetGlobalShaderMap(FeatureLevel), PermutationVector);

	FRayTracingVPLHITRGS::FParameters *PassParameters = GraphBuilder.AllocParameters<FRayTracingVPLHITRGS::FParameters>();
	FSceneTextureParameters SceneTextures;

	int32 GatherSamples = 8;// GetRayTracingGlobalIlluminationSamplesPerPixel(View);
	int32 SamplesPerPixel = 1;

	uint32 IterationCount = SamplesPerPixel;
	uint32 SequenceCount = 16;
	uint32 DimensionCount = 256*256;
	int32 FrameIndex = View.ViewState->FrameIndex % 1024;

	FHaltonSequenceIteration HaltonSequenceIteration(Scene->HaltonSequence, IterationCount, SequenceCount, DimensionCount, FrameIndex);

	FHaltonIteration HaltonIteration;
	InitializeHaltonSequenceIteration(HaltonSequenceIteration, HaltonIteration);

	FHaltonPrimes HaltonPrimes;
	InitializeHaltonPrimes(Scene->HaltonPrimesResource, HaltonPrimes);

	FBlueNoise BlueNoise;
	InitializeBlueNoise(BlueNoise);

	SetupSceneTextureParameters(GraphBuilder, &SceneTextures);
	PassParameters->SceneTextures = SceneTextures;

	PassParameters->HaltonIteration = CreateUniformBufferImmediate(HaltonIteration, EUniformBufferUsage::UniformBuffer_SingleDraw);
	PassParameters->HaltonPrimes = CreateUniformBufferImmediate(HaltonPrimes, EUniformBufferUsage::UniformBuffer_SingleDraw);
	PassParameters->BlueNoise = CreateUniformBufferImmediate(BlueNoise, EUniformBufferUsage::UniformBuffer_SingleDraw);

	FDynamicGlobalIllumination::SetupLightParameters(Light.LightSceneInfo->Proxy, PassParameters->LightParameters);

	// Global
	PassParameters->TLAS = View.RayTracingScene.RayTracingSceneRHI->GetShaderResourceView();
	PassParameters->ViewUniformBuffer = View.ViewUniformBuffer;
	PassParameters->UpscaleFactor = CVarRayTracingRenderVPLScaleFactor.GetValueOnAnyThread();
	PassParameters->HitList = GraphBuilder.CreateUAV(HitList);
	//GraphBuilder.CreateUAV(HitList);
	FIntPoint LocalGatherPointsResolution = View.ViewRect.Size();
	GraphBuilder.AddPass(
		RDG_EVENT_NAME("GatherPoints %d%d", LocalGatherPointsResolution.X, LocalGatherPointsResolution.Y),
		PassParameters,
		ERDGPassFlags::Compute,
		[PassParameters, this, &View, RayGenerationShader, LocalGatherPointsResolution](FRHICommandList& RHICmdList)
	{
		FRHIRayTracingScene* RayTracingSceneRHI = View.RayTracingScene.RayTracingSceneRHI;
		FRayTracingShaderBindingsWriter GlobalResources;
		SetShaderParameters(GlobalResources, RayGenerationShader, *PassParameters);
		RHICmdList.RayTraceDispatch(View.RayTracingMaterialPipeline, RayGenerationShader.GetRayTracingShader(), RayTracingSceneRHI, GlobalResources, 256, 256);
	});
		
	GraphBuilder.QueueBufferExtraction(HitList, &SceneViewState->HitList, FRDGResourceState::EAccess::Write, FRDGResourceState::EPipeline::Compute);
	bLightsProcessed = true;

	return bLightsProcessed;
}

void FDeferredShadingSceneRenderer::RenderRayTracingVPL(
	FLightSceneInfoCompact& Light,
	FRHICommandListImmediate& RHICmdList,
	FRDGBuilder& GraphBuilder,
	FViewInfo& View,
	FRDGBufferRef HitList,
	TRefCountPtr<IPooledRenderTarget>& OutTexture,
	FRDGTextureRef& OutputColor, 
	FRDGTextureRef& OutputHitDistance
)
{
	RDG_GPU_STAT_SCOPE(GraphBuilder, RayTracingVPL);
	RDG_EVENT_SCOPE(GraphBuilder, "Ray Tracing VPL");

	FSceneRenderTargets& SceneContext = FSceneRenderTargets::Get(RHICmdList);
	
	int32 GatherSamples = 8;// GetRayTracingGlobalIlluminationSamplesPerPixel(View);
	int32 SamplesPerPixel = 1;

	uint32 IterationCount = SamplesPerPixel;
	uint32 SequenceCount = 1;
	uint32 DimensionCount = 512;
	int32 FrameIndex = View.ViewState->FrameIndex % 1024;
	FHaltonSequenceIteration HaltonSequenceIteration(Scene->HaltonSequence, IterationCount, SequenceCount, DimensionCount, FrameIndex);

	FRayTracingVPLRGS::FPermutationDomain PermutationVector;
	PermutationVector.Set<FRayTracingVPLRGS::FUseAttenuationTermDim>(true);
	PermutationVector.Set<FRayTracingVPLRGS::FEnableTwoSidedGeometryDim>(true);
	TShaderMapRef<FRayTracingVPLRGS> RayGenerationShader(GetGlobalShaderMap(FeatureLevel), PermutationVector);


	FHaltonIteration HaltonIteration;
	InitializeHaltonSequenceIteration(HaltonSequenceIteration, HaltonIteration);

	FHaltonPrimes HaltonPrimes;
	InitializeHaltonPrimes(Scene->HaltonPrimesResource, HaltonPrimes);

	FBlueNoise BlueNoise;
	InitializeBlueNoise(BlueNoise);

	FRayTracingVPLRGS::FParameters *PassParameters = GraphBuilder.AllocParameters<FRayTracingVPLRGS::FParameters>();
	PassParameters->SamplesPerPixel = 8;
	PassParameters->MaxBounces = 1;
	PassParameters->UpscaleFactor = CVarRayTracingRenderVPLScaleFactor.GetValueOnAnyThread();
	PassParameters->MaxRayDistanceForGI = 1.0e27;
	PassParameters->MaxRayDistanceForAO = 1.0e27;
	PassParameters->NextEventEstimationSamples = 1.0f;
	PassParameters->DiffuseThreshold = 0.0f;
	PassParameters->MaxNormalBias = 1.0f;
	FSceneTextureParameters SceneTextures;
	SetupSceneTextureParameters(GraphBuilder, &SceneTextures);
	PassParameters->SceneTextures = SceneTextures;

	// Global
	PassParameters->TLAS = View.RayTracingScene.RayTracingSceneRHI->GetShaderResourceView();
	PassParameters->ViewUniformBuffer = View.ViewUniformBuffer;

	// Sampling sequence
	PassParameters->HaltonIteration = CreateUniformBufferImmediate(HaltonIteration, EUniformBufferUsage::UniformBuffer_SingleDraw);
	PassParameters->HaltonPrimes = CreateUniformBufferImmediate(HaltonPrimes, EUniformBufferUsage::UniformBuffer_SingleDraw);
	PassParameters->BlueNoise = CreateUniformBufferImmediate(BlueNoise, EUniformBufferUsage::UniformBuffer_SingleDraw);

	ResolveSceneColor(RHICmdList);
	//FPooledRenderTargetDesc Desc = SceneContext.GetSceneColor()->GetDesc();

	//FRDGTextureRef OutTexture =	GraphBuilder.RegisterExternalTexture(VPLRT, TEXT("OutTexture"));
// 			FRDGTextureDesc::Create2DDesc(
// 				Desc.Extent,
// 				PF_FloatRGBA,
// 				FClearValueBinding::None,
// 				GFastVRamConfig.LPV,
// 				TexCreate_ShaderResource | TexCreate_UAV,
// 				false),
// 			TEXT("DynamicGITiled"));

	// Allocates denoiser inputs.
	TRefCountPtr<IPooledRenderTarget> SceneColorRT = SceneContext.GetSceneColor();

	PassParameters->HitList2 = GraphBuilder.CreateSRV(HitList);

	PassParameters->RWColor = GraphBuilder.CreateUAV(OutputColor);
	PassParameters->RWHitDistanceUAV = GraphBuilder.CreateUAV(OutputHitDistance);

// 	float Values[4] = { 0.0f, 0.0f, 0.0f, 1.0f };
// 	AddClearUAVPass(GraphBuilder, PassParameters->RWColor, Values);
// 	float Values2[4] = { -1.0f, 0.0f, -1.0f, 0.0f };
// 	AddClearUAVPass(GraphBuilder, PassParameters->RWHitDistanceUAV, Values2);

	FIntPoint LocalGatherPointsResolution = View.ViewRect.Size();
	FIntPoint RayTracingResolution = FIntPoint::DivideAndRoundUp(View.ViewRect.Size(), CVarRayTracingRenderVPLScaleFactor.GetValueOnAnyThread());

	GraphBuilder.AddPass(
		RDG_EVENT_NAME("GatherPoints %d%d", LocalGatherPointsResolution.X, LocalGatherPointsResolution.Y),
		PassParameters,
		ERDGPassFlags::Compute,
		[PassParameters, this, &View, RayGenerationShader, RayTracingResolution, LocalGatherPointsResolution, DimensionCount](FRHICommandList& RHICmdList)
	{
		FRHIRayTracingScene* RayTracingSceneRHI = View.RayTracingScene.RayTracingSceneRHI;
		FRayTracingShaderBindingsWriter GlobalResources;
		SetShaderParameters(GlobalResources, RayGenerationShader, *PassParameters);
		RHICmdList.RayTraceDispatch(View.RayTracingMaterialPipeline, RayGenerationShader.GetRayTracingShader(), RayTracingSceneRHI, GlobalResources, 512, 512);
	});

}

void FDeferredShadingSceneRenderer::DenoiseRayTracingVPL(FRHICommandListImmediate& RHICmdList, FRDGBuilder& GraphBuilder, FViewInfo& View, TRefCountPtr<IPooledRenderTarget>& OutTexture, FRDGTextureRef& OutputColor, FRDGTextureRef& OutputHitDistance)
{
	FSceneTextureParameters SceneTextureParams;
	SetupSceneTextureParameters(GraphBuilder, &SceneTextureParams);

	FSceneRenderTargets& SceneContext = FSceneRenderTargets::Get(RHICmdList);

	IScreenSpaceDenoiser::FDiffuseIndirectInputs DenoiserInputs;
	DenoiserInputs.Color = OutputColor;
	DenoiserInputs.RayHitDistance = OutputHitDistance;

	const IScreenSpaceDenoiser* DefaultDenoiser = IScreenSpaceDenoiser::GetDefaultDenoiser();
	const IScreenSpaceDenoiser* DenoiserToUse = DefaultDenoiser;

	RDG_EVENT_SCOPE(GraphBuilder, "%s%s(VPL) %dx%d",
		DenoiserToUse != DefaultDenoiser ? TEXT("ThirdParty ") : TEXT(""),
		DenoiserToUse->GetDebugName(),
		View.ViewRect.Width(), View.ViewRect.Height());

	IScreenSpaceDenoiser::FAmbientOcclusionRayTracingConfig RayTracingConfig;
	RayTracingConfig.ResolutionFraction = 0.5f;

	static IConsoleVariable* CVar = IConsoleManager::Get().FindConsoleVariable(TEXT("r.VPL.Denoiser.ReconstructionSamples"));

	int32 RayTracingGISamplesPerPixel = CVar->GetInt();
	RayTracingConfig.ResolutionFraction = 1.0f / CVarRayTracingRenderVPLScaleFactor.GetValueOnAnyThread();
	RayTracingConfig.RayCountPerPixel = RayTracingGISamplesPerPixel;

	int32 UpscaleFactor = int32(1.0 / RayTracingConfig.ResolutionFraction);

	IScreenSpaceDenoiser::FVPLGIOutput DenoiserOutputs;
	//DenoiserOutputs.VPLGI = SkyLightTexture;
	DenoiserOutputs = DenoiserToUse->DenoiseVPLGI(
		GraphBuilder,
		View,
		&View.PrevViewInfo,
		SceneTextureParams,
		DenoiserInputs,
		RayTracingConfig);

	GraphBuilder.QueueTextureExtraction(DenoiserOutputs.VPLGI, &OutTexture);
}

void FDeferredShadingSceneRenderer::InitRayTracingVPL(FRHICommandListImmediate& RHICmdList, FRDGBuilder& GraphBuilder, FViewInfo& View, TRefCountPtr<IPooledRenderTarget>& OutTexture, FRDGTextureRef& OutputColor, FRDGTextureRef& OutputHitDistance)
{
	FSceneRenderTargets& SceneContext = FSceneRenderTargets::Get(RHICmdList);
	//FRDGTextureUAV* AAA;
	FClearGIBuffer::FParameters* Parameters2 = GraphBuilder.AllocParameters<FClearGIBuffer::FParameters>();
	Parameters2->RWColor = GraphBuilder.CreateUAV(OutputColor);
	Parameters2->RWHitDistanceUAV = GraphBuilder.CreateUAV(OutputHitDistance);

	auto ComputeShader = View.ShaderMap->GetShader< FClearGIBuffer >();

	GraphBuilder.AddPass(
		RDG_EVENT_NAME("Clear GI UAV"),
		Parameters2,
		ERDGPassFlags::Compute,
		[Parameters2, &View, &SceneContext, ComputeShader](FRHICommandListImmediate& RHICmdList)
	{
		RHICmdList.SetComputeShader(RHICmdList.GetBoundComputeShader());

		SetShaderParameters(RHICmdList, ComputeShader, ComputeShader.GetComputeShader(), *Parameters2);
		FIntPoint RSMSize(SceneContext.GetSceneColor()->GetDesc().GetSize().X, SceneContext.GetSceneColor()->GetDesc().GetSize().Y);

		;
		DispatchComputeShader(RHICmdList, ComputeShader, FComputeShaderUtils::GetGroupCount(RSMSize, FClearGIBuffer::GetThreadGroupSize()).X, FComputeShaderUtils::GetGroupCount(RSMSize, FClearGIBuffer::GetThreadGroupSize()).Y, 1);
		UnsetShaderUAVs(RHICmdList, ComputeShader, ComputeShader.GetComputeShader());
	});

}

#endif // RHI_RAYTRACING
