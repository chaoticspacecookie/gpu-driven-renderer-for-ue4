
#include "GPUDrawDataCulling.h"
#include "GPUDrawDataCulling/CullingDefinitions.h"

#include "GlobalShader.h"
#include "SceneRendering.h"
#include "ShaderParameterUtils.h"
#include "ScenePrivate.h"

static TAutoConsoleVariable<bool> CVarPrimitivesDepthCulling(TEXT("r.PrimitivesDepthCulling"),true, TEXT("Enables render's primitive depth culling"));
static TAutoConsoleVariable<bool> CVarPrimitivesOrientationCulling(TEXT("r.PrimitivesOrientationCulling"),true, TEXT("Enables render's primitive orientation culling"));
static TAutoConsoleVariable<bool> CVarPrimitivesFrustumCulling(TEXT("r.PrimitivesFrustumCulling"),true, TEXT("Enables render's primitive frustum culling"));
static TAutoConsoleVariable<bool> CVarPrimitivesSmallSizeCulling(TEXT("r.PrimitivesSmallSizeCulling"),true, TEXT("Enables small render's primitive culling"));

static TAutoConsoleVariable<bool> CVarAllowUseManualVertexFetch(TEXT("r.AllowUseManualVertexFetch"),true, TEXT("Culling shader will try to use manual vertex fetch to get VF input"));
static TAutoConsoleVariable<bool> CVarUseStaticCullFlags(TEXT("r.UseStaticCullFlags"), true, TEXT("Use culling flags defined by material at shader compile-time"));
static TAutoConsoleVariable<bool> CVarUseMaskedFVInputUpdate(TEXT("r.UseMaskedFVInputUpdate"), true, TEXT("Update only per vertex attributes when fetching triangle"));

struct FCullFlags
{
	union
	{
		struct
		{
			uint32 InstanceInputsMask : MaxVertexElementCount;
			ERasterizerCullMode CullBackFace : 2;
			bool CullDepth : 1;
			bool CullSmallPrimitives : 1;
			bool CullFrustum : 1;
		};
		uint32 Data;
	};
};


struct FMeshDrawCallArguments
{
	uint32 FirstIndex;
	uint32 NumVertices;
	uint32 BaseVertexIndex;
	uint32 NumInstances;
};

struct FMeshIndirectDrawCallArguments
{	
	FShaderResourceViewRHIRef IndirectDrawArgumentsBufferView;
	uint32 IndirectDrawArgumentsBufferOffset;
	uint32 IndirectDrawArgumentsCount;
};

//template <bool bUseIndirect>
struct FMeshDrawParameters 
//	: TChooseClass < bUseIndirect, FMeshIndirectDrawCallArguments, FMeshDrawCallArguments>::Result
{
	union 
	{
		FMeshDrawCallArguments DrawArgs;
		FMeshIndirectDrawCallArguments IndirectDrawArgs;
	};

	uint32 NumPrimitives;
	uint32 NumInstances;
	
	FCullFlags CullFlags;
	int32 PrimIdsStreamIndex;
};

struct FDrawCallData
{
	int BaseVertexOffset;
	
	uint32 DrawCountBufferOffset;
	uint32 IndirectBufferOffset;
	uint32 Flags;
};

struct FSectionData
{
	uint32 InstanceId;
	uint32 DrawCallDataId;
	uint32 OutputIndexStart;
	
};

//	StructuredBuffer<FSectionData> SectionsData		: register(t0);
//	Buffer<uint> CullingSectionsIds					: register(t1);
//
//#if FETCH_BY_INDEX
//	Buffer<uint> IndexBuffer						: register(t2);
//#endif
//
//#if VF_USE_PRIMITIVE_SCENE_DATA
//	Buffer<uint> PrimitiveIdsBuffer;
//#endif
//
//	RWBuffer<uint> IndexAndDrawCountBuffer					: register(u0);
//	RWBuffer<uint> CulledIndexBuffer						: register(u1);
//	RWStructuredBuffer<FIntirectArgs> CulledIndirectArgs	: register(u2);

//BEGIN_SHADER_PARAMETER_STRUCT(FInputStreamsParameters, )
//	//SHADER_PARAMETER(uint, InstanceAndFlagsMask)
//	SHADER_PARAMETER_SRV(Buffer<uint>, IndexBuffer)
//END_SHADER_PARAMETER_STRUCT()

class FCullingShaderData
{
public:
	FRHIShaderResourceView* IndexBufferView;
	FSetElementId VertexInputStreamsId;
	int8 PrimitiveIdsStreamIndex;

	FMeshDrawShaderBindings MeshDrawShaderBindings;

	bool operator == (const FCullingShaderData& Rhs) const
	{
		return VertexInputStreamsId == Rhs.VertexInputStreamsId
		&& PrimitiveIdsStreamIndex == Rhs.PrimitiveIdsStreamIndex
		&& MeshDrawShaderBindings.MatchesForDynamicInstancing(Rhs.MeshDrawShaderBindings)
		&& IndexBufferView == Rhs.IndexBufferView;
	}
};


class FVertexInputStreamsCullingData
{
public:	

	TStaticArray<int8, MaxVertexElementCount> StreamIndexMapping;

	uint32 IsInstanceIndexMask;

	struct FInputStreamViewData
	{
		FRHIVertexBuffer* Buffer;
		EVertexElementType ElementType;
		uint8 AttributeIndex;

		FShaderResourceViewRHIRef View;

		bool operator == (const FInputStreamViewData& Rhs) const
		{
			return  Buffer == Rhs.Buffer
			&& ElementType == Rhs.ElementType
			&& AttributeIndex == Rhs.AttributeIndex;
		}
	};

	TArray<FInputStreamViewData> InputStreamViews;

	
	FVertexInputStreamsCullingData() = default;

	using FInitializerType = TPair<const FVertexInputStreamArray&, const FVertexDeclarationElementList&>;

	FVertexInputStreamsCullingData(const FInitializerType& Initializer)
		:	StreamIndexMapping(INDEX_NONE), IsInstanceIndexMask(0)
	{
		auto& InputStreamArray = Initializer.Key;
		auto& DeclarationElements = Initializer.Value;
		
		InputStreamViews.AddUninitialized(InputStreamArray.Num());
		for(int ArrayIndex = 0; InputStreamArray.Num(); ++ArrayIndex)
		{
			InputStreamViews[ArrayIndex].Buffer = InputStreamArray[ArrayIndex].VertexBuffer;
			StreamIndexMapping[InputStreamArray[ArrayIndex].StreamIndex] = ArrayIndex;
		}

		for(const auto& Element : DeclarationElements)
		{
			const int8 MappedIndex = StreamIndexMapping[Element.StreamIndex];
			checkf(MappedIndex != INDEX_NONE, L"No vertex input stream for declaration with StreamIndex=%d", Element.StreamIndex);

			InputStreamViews[MappedIndex].ElementType = Element.Type;
			InputStreamViews[MappedIndex].AttributeIndex = Element.AttributeIndex;

			EPixelFormat Format = GVertexElementTypeToPixelFormat[Element.Type.GetValue()];
			InputStreamViews[MappedIndex].View = RHICreateShaderResourceView(InputStreamArray[MappedIndex].VertexBuffer, Element.Stride, Format);

			IsInstanceIndexMask |= static_cast<uint32>(!!Element.bUseInstanceIndex) << Element.AttributeIndex;
		}
	}

	int8 GetAttributeIndexFromStreamIndex(uint8 StreamIndex)
	{
		checkf(StreamIndex < MaxVertexElementCount && StreamIndexMapping[StreamIndex] != INDEX_NONE, L"Invalid stream index");
		return StreamIndexMapping[StreamIndex];
	}
};

class FVertexInputStreamsCullingDataKeyFunc : public DefaultKeyFuncs<FVertexInputStreamsCullingData>
{
public:
	static bool Matches(const FVertexInputStreamsCullingData& A, const FVertexInputStreamsCullingData& B)
	{
		return A.InputStreamViews == B.InputStreamViews;
	}

	
	static bool Matches(const FVertexInputStreamsCullingData& A, const FVertexInputStreamsCullingData::FInitializerType& B)
	{
		auto& InputStreamArray = B.Key;
		auto& DeclarationElements = B.Value;

		if(InputStreamArray.Num() != A.InputStreamViews.Num())
		{
			return false;
		};

		for (const auto& Element : DeclarationElements)
		{
			const int8 MappedIndex = A.StreamIndexMapping[Element.StreamIndex];

			const bool Matches = MappedIndex != INDEX_NONE
			                     && A.InputStreamViews[MappedIndex].Buffer == InputStreamArray[MappedIndex].VertexBuffer
			                     && A.InputStreamViews[MappedIndex].ElementType == Element.Type
			                     && A.InputStreamViews[MappedIndex].AttributeIndex == Element.AttributeIndex;

			if (!Matches)
			{
				return false;
			}
		}

		return true;
	}

	static uint32 GetKeyHash(const FVertexInputStreamsCullingData& Key)
	{
		uint32 BuffersHash = 0;
		uint32 AttributeHash = 0;

		for(const auto& ViewData : Key.InputStreamViews)
		{
			BuffersHash = HashCombine(BuffersHash, PointerHash(ViewData.Buffer));
			AttributeHash = HashCombine(AttributeHash, GetTypeHash(ViewData.ElementType));
			AttributeHash = HashCombine(AttributeHash, GetTypeHash(ViewData.AttributeIndex));
		}
		
		return HashCombine(BuffersHash, AttributeHash);
	}

	static uint32 GetKeyHash(const FVertexInputStreamsCullingData::FInitializerType& Initializer)
	{
		uint32 BuffersHash = 0;
		uint32 AttributeHash = 0;
		
		auto& InputStreamArray = Initializer.Key;
		auto& DeclarationElements = Initializer.Value;

		for(const auto& InputStream : InputStreamArray)
		{
			BuffersHash = HashCombine(BuffersHash, PointerHash(InputStream.VertexBuffer));
		}

		for(const auto& Element : DeclarationElements)
		{
			AttributeHash = HashCombine(AttributeHash, GetTypeHash(static_cast<EVertexElementType>(Element.Type)));
			AttributeHash = HashCombine(AttributeHash, GetTypeHash(Element.AttributeIndex));
		}
		
		return HashCombine(BuffersHash, AttributeHash);
	}
};

TSet<FVertexInputStreamsCullingData, FVertexInputStreamsCullingDataKeyFunc> VertexInputStreamsCullingData;


class FInputAttributesParameters
{
	DECLARE_TYPE_LAYOUT(FInputAttributesParameters, NonVirtual);

	void Bind(const FShaderParameterMap& ParameterMap, const TCHAR* NamePrefix)
	{
		const FString NameBaseWithPrefix = FString(NamePrefix) + StaticGetTypeLayout().Name;
		NumBuffersBound = 0;
		for(int Index = 0; Index < MaxVertexElementCount; Index++)
		{
			AttrBuffer[Index].Bind(ParameterMap, ToCStr(NameBaseWithPrefix + FString::FromInt(Index)));
			NumBuffersBound += AttrBuffer->IsBound();
		}
	}

	bool IsBound() const
	{
		return NumBuffersBound > 0;
	}

	inline void Set(FRHICommandList& RHICmdList,  FRHIComputeShader* Shader, FVertexInputStreamsCullingData& Data) const
	{
		for(auto& ViewData : Data.InputStreamViews)
		{
			checkf(AttrBuffer[ViewData.AttributeIndex].IsBound(), L"The attribute on index %d is not bound", ViewData.AttributeIndex);
			SetSRVParameter(RHICmdList, Shader, AttrBuffer[ViewData.AttributeIndex], ViewData.View);
		}
	}

	inline void SetAt(FRHICommandList& RHICmdList,  FRHIComputeShader* Shader, int8 AttributeIndex, FShaderResourceViewRHIRef ResourceView) const
	{
		SetSRVParameter(RHICmdList, Shader, AttrBuffer[AttributeIndex], ResourceView);
	}

private:
	LAYOUT_ARRAY(FShaderResourceParameter, AttrBuffer, MaxVertexElementCount)

	unsigned NumBuffersBound = 0;
};


class FVertexInputAssemblyParameters
{
	DECLARE_TYPE_LAYOUT(FVertexInputAssemblyParameters, NonVirtual);
	

	void Bind(const FShaderParameterMap& ParameterMap)
	{
		InputAttributesParameters.Bind(ParameterMap, InputAttributesParameters.GetTypeLayout().Name);
		PrimitiveIdsBuffer.Bind(ParameterMap, PrimitiveIdsBuffer.GetTypeLayout().Name);
		IndexBuffer.Bind(ParameterMap, IndexBuffer.GetTypeLayout().Name);
	}

	void Set(FRHICommandList& RHICmdList,  FRHIComputeShader* Shader, FCullingShaderData& Data, FRHIShaderResourceView* PrimitiveIdsView = nullptr) const
	{
		if(Data.IndexBufferView)
		{
			SetSRVParameter(RHICmdList, Shader, IndexBuffer, Data.IndexBufferView);
		}
		if(Data.VertexInputStreamsId.IsValidId())
		{
			auto& VertexInputData = VertexInputStreamsCullingData[Data.VertexInputStreamsId];
			InputAttributesParameters.Set(RHICmdList, Shader, VertexInputStreamsCullingData[Data.VertexInputStreamsId]);
			if(PrimitiveIdsView && Data.PrimitiveIdsStreamIndex != INDEX_NONE)
			{
				const int8 AttributeIndex = VertexInputData.GetAttributeIndexFromStreamIndex(Data.PrimitiveIdsStreamIndex);
				InputAttributesParameters.SetAt(RHICmdList, Shader, AttributeIndex, PrimitiveIdsView);
			}
		}
		else if(PrimitiveIdsView)
		{
			SetSRVParameter(RHICmdList, Shader, PrimitiveIdsBuffer, PrimitiveIdsView);
		}
	}

private:
	
	LAYOUT_FIELD(FInputAttributesParameters, InputAttributesParameters)
	LAYOUT_FIELD(FShaderResourceParameter, PrimitiveIdsBuffer)
	LAYOUT_FIELD(FShaderResourceParameter, IndexBuffer)
		
};


class FSharedCullingData
{
	DECLARE_TYPE_LAYOUT(FSharedCullingData, NonVirtual);

	void Bind(const FShaderParameterMap& ParameterMap)
	{
		CulledIndexBuffer.Bind(ParameterMap, CulledIndexBuffer.GetTypeLayout().Name);
		CulledIndirectArgumentsBuffer.Bind(ParameterMap, CulledIndirectArgumentsBuffer.GetTypeLayout().Name);
		IndexAndDrawCountBuffer.Bind(ParameterMap, IndexAndDrawCountBuffer.GetTypeLayout().Name);

		CullingSectionsData.Bind(ParameterMap, CullingSectionsData.GetTypeLayout().Name);
	}
	

	LAYOUT_FIELD(FRWShaderParameter, CulledIndexBuffer)
	LAYOUT_FIELD(FRWShaderParameter, CulledIndirectArgumentsBuffer)
	LAYOUT_FIELD(FRWShaderParameter, IndexAndDrawCountBuffer)
	LAYOUT_FIELD(FRWShaderParameter, CullingSectionsData)


	static FIndexBufferRHIRef CulledIndexBufferRes;

	static FUnorderedAccessViewRHIRef CulledIndexBufferView;
	static FRWBuffer CulledIndirectArgumentsBufferResource;
	static FRWBuffer IndexAndDrawCountBufferView;											
	static FRWBuffer CullingSectionsDataView;
};


class FCullingShader : public FMeshMaterialShader
{
	class FFetchByIndex : SHADER_PERMUTATION_BOOL("FETCH_BY_INDEX");
public:

	DECLARE_SHADER_TYPE(FCullingShader, MeshMaterial)

	using FPermutationDomain = TShaderPermutationDomain<FFetchByIndex>;

	FCullingShader(){}
	
	FCullingShader(const FMeshMaterialShaderType::CompiledShaderInitializerType& Initializer):
		FMeshMaterialShader(Initializer)
	{
		
		CullingData.Bind(Initializer.ParameterMap);
		SectionsOffset.Bind(Initializer.ParameterMap, SectionsOffset.GetTypeLayout().Name);
	}

	static bool ShouldCompilePermutation(const FMeshMaterialShaderPermutationParameters& Parameters)
	{
		const bool bSupportedPlatform = RHISupportsComputeShaders(Parameters.Platform);
		const bool bSupportedMaterialType = Parameters.MaterialParameters.bIsDefaultMaterial || Parameters.MaterialParameters.bMaterialMayModifyMeshPosition;

		const bool bIsNiagara = Parameters.VertexFactoryType->GetFName().ToString().Contains("Niagara");
		return		bSupportedPlatform
				&&  bSupportedMaterialType
				&& !bIsNiagara;
	}

	static void AddCullingFlagsMacros(const FMeshMaterialShaderPermutationParameters& Parameters, FShaderCompilerEnvironment& OutEnvironment)
	{
		auto SetCullFlagMacro = [&](FString FlagName, uint32 FlagValue, bool bIsStaticFlag)
		{
			const FString CullFlagsMacroName = FlagName;
			const FString CullFlagsMacroValue = bIsStaticFlag ? "1" : FString::FromInt(FlagValue) + "& SectionData.CullFlags";
						
			OutEnvironment.SetDefine(ToCStr(CullFlagsMacroName), ToCStr(CullFlagsMacroValue));
		};

		const bool MaterialFullyOpaque = Parameters.MaterialParameters.bWritesEveryPixel && Parameters.MaterialParameters.BlendMode == BLEND_Opaque;
		const bool UseStaticCullFlags = CVarUseStaticCullFlags.GetValueOnAnyThread();

		if((MaterialFullyOpaque || !CVarUseStaticCullFlags.GetValueOnAnyThread()) && CVarPrimitivesDepthCulling.GetValueOnAnyThread ())
		{
;			FCullFlags Flags{};
			Flags.CullDepth = true;
			SetCullFlagMacro("CULL_DEPTH_FLAG", Flags.Data, UseStaticCullFlags);
		}
		if((!Parameters.MaterialParameters.bIsTwoSided  || !CVarUseStaticCullFlags.GetValueOnAnyThread())  && CVarPrimitivesOrientationCulling.GetValueOnAnyThread())
		{
			FCullFlags Flags{};
			Flags.CullBackFace = ERasterizerCullMode(~0);
			SetCullFlagMacro("CULL_BACKFACE_FLAG", Flags.Data, UseStaticCullFlags);
		}

		if(CVarPrimitivesSmallSizeCulling.GetValueOnAnyThread())
		{
			FCullFlags Flags{};
			Flags.CullSmallPrimitives = true;
			SetCullFlagMacro("CULL_SMALL_PRIMITIVES_FLAG", Flags.Data, true);
		}
		if(CVarPrimitivesFrustumCulling.GetValueOnAnyThread())
		{
			FCullFlags Flags{};
			Flags.CullFrustum = true;
			SetCullFlagMacro("CULL_FRUSTUM_FLAG", Flags.Data, true);
		}
	}
	
	static void ModifyCompilationEnvironment(const FMeshMaterialShaderPermutationParameters& Parameters, FShaderCompilerEnvironment& OutEnvironment)
	{
		
		OutEnvironment.CompilerFlags.AddUnique(CFLAG_WaveOperations);
		OutEnvironment.CompilerFlags.AddUnique(CFLAG_ForceDXC);
		
		AddCullingFlagsMacros(Parameters, OutEnvironment);

		OutEnvironment.SetDefine(L"USE_MASKED_INPUT_UPDATE", CVarUseMaskedFVInputUpdate.GetValueOnAnyThread());
		OutEnvironment.SetDefine(L"ALLOW_MANUAL_VERTEX_FETCH", CVarAllowUseManualVertexFetch.GetValueOnAnyThread());
	}

private:
	LAYOUT_FIELD(FSharedCullingData, CullingData)
	LAYOUT_FIELD(FShaderParameter, SectionsOffset)
};

IMPLEMENT_MATERIAL_SHADER_TYPE(, FCullingShader, TEXT("/Engine/Private/CustomRenderer/GPUDrawDataCulling.usf"), TEXT("PrimitivesCullingCS"), SF_Compute);


enum ECullingDataProcessor
{
	ClearCullingCounters,
	RemoveDrawCommands,
	CompactDrawCommands,
	RegisterDrawCommands,
	RegisterDrawCommandsIndirect,
};

const TMap<ECullingDataProcessor, const TCHAR*> EntryPoints = {
	{ClearCullingCounters, L"ClearCountersCS"},
	{RemoveDrawCommands, L"RemoveDrawCommandsCS"},
	{CompactDrawCommands, L"CompactDrawCommandCS"},
	{RegisterDrawCommands, L"RegisterDrawCommandsCS"},
	{RegisterDrawCommandsIndirect, L"RegisterDrawCommandsIndirectCS"},
};
static const TCHAR* CullingShaderFilename = L"/Engine/Private/CustomRenderer/GPUDrawDataCulling.usf";

class FCullingDataProcessorShaderBase : public FGlobalShader
{
	DECLARE_SHADER_TYPE(FCullingDataProcessorShaderBase, Global)

	FCullingDataProcessorShaderBase(){}

	FCullingDataProcessorShaderBase(const ShaderMetaType::CompiledShaderInitializerType& Initializer)
		: FGlobalShader(Initializer)
	{}

protected:
	static bool ShouldCompilePermutation(const FGlobalShaderPermutationParameters& Parameters)
	{
		return 0;//ShouldCompileCullingPermutation(Parameters.Platform);
	}

	static void ModifyCompilationEnvironment(const FGlobalShaderPermutationParameters& Parameters, FShaderCompilerEnvironment& OutEnvironment)
	{
	}

	LAYOUT_FIELD(FSharedCullingData, CullingData)
};

class FAddSectionsCS : public FCullingDataProcessorShaderBase
{
	DECLARE_SHADER_TYPE(FAddSectionsCS, Global)

	using FCullingDataProcessorShaderBase::FCullingDataProcessorShaderBase;

	void SetParameters( FRHICommandList& RHICmdList, FRHIShaderResourceView* AddedCullingSectionsDataView )
	{
		FRHIComputeShader* ShaderRHI = RHICmdList.GetBoundComputeShader();
		
		SetSRVParameter(RHICmdList, ShaderRHI, AddedCullingSectionsData, AddedCullingSectionsDataView );
		CullingData.CullingSectionsData.SetBuffer(RHICmdList, ShaderRHI, FSharedCullingData::CulledIndirectArgumentsBufferResource);
	}

	void UnsetParameters(FRHICommandList& RHICmdList)
	{
		CullingData.CullingSectionsData.UnsetUAV(RHICmdList, RHICmdList.GetBoundComputeShader());
	}

private:
	LAYOUT_FIELD(FShaderResourceParameter, AddedCullingSectionsData)
};

class FAddIndirectSectionsCS : public FCullingDataProcessorShaderBase
{
	DECLARE_SHADER_TYPE(FAddIndirectSectionsCS, Global)

	using FCullingDataProcessorShaderBase::FCullingDataProcessorShaderBase;

	void SetParameters( FRHICommandList& RHICmdList, FMeshIndirectDrawCallArguments& Arguments )
	{
		FRHIComputeShader* ShaderRHI = RHICmdList.GetBoundComputeShader();
		
		SetSRVParameter(RHICmdList, ShaderRHI, IndirectDrawBuffer, Arguments.IndirectDrawArgumentsBufferView );
		SetShaderValue(RHICmdList, ShaderRHI, IndirectDrawBufferOffset, Arguments.IndirectDrawArgumentsBufferOffset );

		CullingData.CullingSectionsData.SetBuffer(RHICmdList, ShaderRHI, FSharedCullingData::CulledIndirectArgumentsBufferResource);
	}

	void UnsetParameters(FRHICommandList& RHICmdList)
	{
		CullingData.CullingSectionsData.UnsetUAV(RHICmdList, RHICmdList.GetBoundComputeShader());
	}

private:
	
	LAYOUT_FIELD(FShaderResourceParameter, IndirectDrawBuffer);
	LAYOUT_FIELD(FShaderParameter, IndirectDrawBufferOffset);
};

//IMPLEMENT_SHADER_TYPE(, FCullingDataProcessor<ClearCullingCounters>, CullingShaderFilename, FCullingDataProcessor<ClearCullingCounters>::EntryPoint, SF_Compute);
//IMPLEMENT_SHADER_TYPE(, FCullingDataProcessor<RemoveDrawCommands>, CullingShaderFilename,   FCullingDataProcessor<RemoveDrawCommands>::EntryPoint, SF_Compute);
//IMPLEMENT_SHADER_TYPE(, FCullingDataProcessor<CompactDrawCommands>, CullingShaderFilename,  FCullingDataProcessor<CompactDrawCommands>::EntryPoint, SF_Compute);

inline uint32 GetTypeHash(const FShaderResourceViewInitializer& Initializer)
{

#if 0
	using InitType = FShaderResourceViewInitializer::EType;
	using HashTuple = TTuple<FRHIResource*, uint32, uint32>;

	uint32 Hash = 0;
	switch (Initializer.GetType())
	{
	case InitType::IndexBufferSRV:
		Hash = GetTypeHash(HashTuple(Initializer.AsIndexBufferSRV().IndexBuffer, Initializer.AsIndexBufferSRV().NumElements, Initializer.AsIndexBufferSRV().StartOffsetBytes));
		break;
	case InitType::VertexBufferSRV:
		Hash = GetTypeHash(HashTuple(Initializer.AsVertexBufferSRV().VertexBuffer, Initializer.AsVertexBufferSRV().NumElements, Initializer.AsVertexBufferSRV().StartOffsetBytes));
		Hash = HashCombine(Hash, Initializer.AsVertexBufferSRV().Format);
		break;
	case InitType::StructuredBufferSRV:
		Hash = GetTypeHash(HashTuple(Initializer.AsVertexBufferSRV().VertexBuffer, Initializer.AsVertexBufferSRV().NumElements, Initializer.AsVertexBufferSRV().StartOffsetBytes));
		break;
	}
	return HashCombine(Hash, GetTypeHash(Initializer.GetType()));

#else
	return static_cast<uint32>(CityHash64(reinterpret_cast<const char*>(&Initializer), sizeof(FShaderResourceViewInitializer)));
#endif
}

FShaderResourceViewRHIRef FindOrCreateShaderResourceView(const FShaderResourceViewInitializer& Initializer)
{
	static TMap<const FShaderResourceViewInitializer, FShaderResourceViewRHIRef> BuffersSRV = {};

	FShaderResourceViewRHIRef& ResourceView = BuffersSRV.FindOrAdd(Initializer);
	if ( !ResourceView.IsValid() )
	{
		ResourceView = RHICreateShaderResourceView(Initializer);
	}
	return ResourceView;
}

struct FCullingCommand
{
	const FComputeShaderRHIRef Shader;
	const FCullingShaderData CullingShaderBindings;

	const TArray<uint32>IndirectAndDrawOffsets;
};

struct FCullingCommandKeyFunc : DefaultKeyFuncs<FCullingCommand>
{
	static bool Matches(const FCullingCommand &A, const FCullingCommand &B)
	{
		return A.Shader == B.Shader
			&& A.CullingShaderBindings == B.CullingShaderBindings;
	}
	
	static bool Matches(const FCullingCommand &A, const FMeshCullingContext& B)
	{
		return A.Shader == B.MaterialShader.GetComputeShader()
			&& A.CullingShaderBindings.VertexInputStreamsId == B.VertexInputStreamsCullingDataId
			&& A.CullingShaderBindings.MeshDrawShaderBindings.MatchesForDynamicInstancing(B.MeshShaderBindings);
	}

	static uint32 HashHelper(const FRHIShader* Shader, const FRHIShaderResourceView* IndexBufferView, const FSetElementId& VertexInputStreamsId,
		const FMeshDrawShaderBindings& MeshDrawShaderBindings, uint8 PrimitiveIdsStreamIndex)
	{
		struct FHashKey
		{
			uint32 ShaderHsh;
			uint32 IndexBuffer;
		    uint32 VertexStreamsId;
			uint32 DynamicInstancingHash;
			uint8 PrimitiveIdStreamIndex;
		} HashKey;

		HashKey.ShaderHsh = PointerHash(Shader);
		HashKey.IndexBuffer = PointerHash(IndexBufferView);
		HashKey.VertexStreamsId = GetTypeHash(VertexInputStreamsId.AsInteger());
		HashKey.DynamicInstancingHash = MeshDrawShaderBindings.GetDynamicInstancingHash();
		HashKey.PrimitiveIdStreamIndex = PrimitiveIdsStreamIndex;
		
		return static_cast<uint32>(CityHash64(reinterpret_cast<char*>(&HashKey), sizeof(FHashKey)));
	}
	
	static uint32 GetKeyHash(const FCullingCommand& Key)
	{
		return HashHelper(
			Key.Shader, 
			Key.CullingShaderBindings.IndexBufferView, 
			Key.CullingShaderBindings.VertexInputStreamsId,
			Key.CullingShaderBindings.MeshDrawShaderBindings, 
			Key.CullingShaderBindings.PrimitiveIdsStreamIndex
		);
	}
	
	static uint32 GetKeyHash(const FMeshCullingContext& Key)
	{
		return HashHelper(
			Key.MaterialShader.GetComputeShader(), 
			Key.IndexBufferView, 
			Key.VertexInputStreamsCullingDataId,
			Key.MeshShaderBindings, 
			Key.PrimitiveIdsStreamIndex
		);
	}
};



FCullingMaterialData FGPUDrawDataCuller::CreateCullingMaterialData (
	const FMaterial& MeshPassMaterial, 
	const FMaterialRenderProxy& MaterialRenderProxy)
{
	const bool MaterialFullyOpaque = MeshPassMaterial.WritesEveryPixel()
									&& MeshPassMaterial.GetBlendMode() == BLEND_Opaque;

	const bool bGetDefaultMaterial = !MeshPassMaterial.IsDefaultMaterial()
	                                 && !MeshPassMaterial.MaterialModifiesMeshPosition_RenderThread();
	if(bGetDefaultMaterial)
	{
		const FMaterialRenderProxy* DefaultRenderProxy = UMaterial::GetDefaultMaterial(MD_Surface)->GetRenderProxy();
		const FMaterial* DefaultMaterial = DefaultRenderProxy->GetMaterialNoFallback(MeshPassMaterial.GetFeatureLevel());
		if(DefaultMaterial)
		{
			return FCullingMaterialData{DefaultMaterial, DefaultRenderProxy, MaterialFullyOpaque};
		}
	}
	return FCullingMaterialData{&MeshPassMaterial, &MaterialRenderProxy, MaterialFullyOpaque};
}



FMeshCullingContext FGPUDrawDataCuller::CreateMeshCullingContext(
	const FMaterial* CullingMaterial,
	const EBlendMode& MeshDrawBlendMode,
	const FVertexFactory* VertexFactory,
	const FGraphicsMinimalPipelineStateInitializer& PipelineState,
	const FMeshDrawCommand& MeshDrawCommand)
{
	const FCullingShader::FPermutationDomain Permutation(MeshDrawCommand.IndexBuffer != nullptr);
	
	FMeshCullingContext CullingContext{};

	CullingContext.MaterialShader = CullingMaterial->GetShader<FCullingShader>(VertexFactory->GetType(), Permutation, false);	
	if(!CullingContext.MaterialShader.IsValid())
	{
		return {};
	}

	FCullFlags CullFlags{};
	const auto FeatureLevel = CullingMaterial->GetFeatureLevel();

	if(!VertexFactory->SupportsManualVertexFetch(FeatureLevel))
	{
		FVertexDeclarationElementList VertexElements;
		if (!PipelineState.BoundShaderState.VertexDeclarationRHI->GetInitializer(VertexElements))
		{
			return {};
		}
		 
		const FVertexInputStreamsCullingData::FInitializerType Initializer = FVertexInputStreamsCullingData::FInitializerType{MeshDrawCommand.VertexStreams, VertexElements};

		CullingContext.VertexInputStreamsCullingDataId = VertexInputStreamsCullingData.FindId(Initializer);

		if(!CullingContext.VertexInputStreamsCullingDataId.IsValidId())
		{
			CullingContext.VertexInputStreamsCullingDataId = VertexInputStreamsCullingData.Add(FVertexInputStreamsCullingData(Initializer));
		}
	}
	CullingContext.IndexBufferView = MeshDrawCommand.IndexBuffer ? FindOrCreateShaderResourceView(MeshDrawCommand.IndexBuffer) : nullptr;
	CullingContext.PrimitiveIdsStreamIndex = MeshDrawCommand.PrimitiveIdStreamIndex;

	FMeshProcessorShaders Shaders{};
	Shaders.ComputeShader = CullingContext.MaterialShader;
	CullingContext.MeshShaderBindings.Initialize(Shaders);

	FRasterizerStateInitializerRHI RasterizerState;
	PipelineState.RasterizerState->GetInitializer(RasterizerState);

	//CullingContext.CullFlags.SmallPrimitiveCulling	= CullingParameters.bFrustumCullingEnabled;
	//CullingContext.CullFlags.FrustumCulling			= CullingParameters.bSmallPrimitiveCullingEnabled;
	CullFlags.CullBackFace		= RasterizerState.CullMode.GetValue();
	CullFlags.CullDepth			= RasterizerState.FillMode == FM_Solid && MeshDrawBlendMode;

	//Parameters.CullFlagsData = CullFlags.Data;
	
	return CullingContext;
}


TSet<FCullingCommand, FCullingCommandKeyFunc> CullingCommands;
TArray<FMeshCullingContext> CullingContexts;


TResourceArray<FMeshDrawCallArguments> DrawCallArgumentsArray;

bool GrabDrawData(FMeshCullingContext &CullingContext, FMeshDrawCommand& MeshDrawCommand)
{
	if(CullingContext.bUsesIndirectDraw)
	{
		FMeshIndirectDrawCallArguments Arguments =
		{
			FindOrCreateShaderResourceView({MeshDrawCommand.IndirectArgs.DrawArgumentsBuffer, PF_R32_SINT, }),
			MeshDrawCommand.IndirectArgs.DrawArgumentsBufferOffset,
			FMath::Max(MeshDrawCommand.IndirectArgs.MaxMultiDrawCount, 1u)
		};

		if(MeshDrawCommand.IndirectArgs.MaxMultiDrawCount == UINT32_MAX)
		{
			if(!MeshDrawCommand.IndirectArgs.DrawCountBuffer)
			{
			}
		}

		//NQUEUE_UNIQUE_RENDER_COMMAND(AddIndirectSectionsCommand, 
		//
		//		
		//		auto Shader = GetGlobalShaderMap(GMaxRHIFeatureLevel)->GetShader<FAddIndirectSectionsCS>();
		//		RHICmdList.SetComputeShader(Shader.GetComputeShader());
		//		Shader->SetParameters(RHICmdList, Arguments);
		//		DispatchComputeShader(RHICmdList, Shader, Arguments.IndirectDrawArgumentsCount, 1, 1);
		//)
	}
	else
	{
		DrawCallArgumentsArray.Add(
		{  MeshDrawCommand.FirstIndex,
				MeshDrawCommand.VertexParams.NumVertices,
				MeshDrawCommand.VertexParams.BaseVertexIndex,
				MeshDrawCommand.NumInstances }
			);
	}
}

void FGPUDrawDataCuller::SetupCullingOverrides(FMeshCullingContext && CullingContext, FMeshDrawCommand& MeshDrawCommand) const
{
	if(CullingContext.IsValid())
	{
		GrabDrawData(CullingContext, MeshDrawCommand);

		// tmpTODO: Refactor "if indirect" conditions on NumPrimitives
		if((CullingContext.bUsesIndirectDraw = MeshDrawCommand.NumPrimitives == 0))
		{
			MeshDrawCommand.IndirectArgs.MaxMultiDrawCount = 
				MeshDrawCommand.IndirectArgs.MaxMultiDrawCount <= 1 ? // 0 if not multi draw draw buffer 
				MeshDrawCommand.NumInstances	// each instance will be unrolled to it's own draw command
			    : UINT32_MAX; // counting number of total instances from indirect buffer is unnecessary expensive
		}

		MeshDrawCommand.IndexBuffer = FSharedCullingData::CulledIndexBufferRes;
		MeshDrawCommand.IndirectArgs.DrawArgumentsBuffer = FSharedCullingData::CulledIndirectArgumentsBufferResource.Buffer;
	}
}

#if 0
void FGPUDrawDataCuller::UpdateIndirectDrawList()
{
	check(GPUUpdateData.TotalIndirectArguments)
	GPUUpdateData.IndirectDrawStartIndices.Reset(GPUUpdateData.TotalIndirectArguments);
	uint32 ReservedIndicesNum = 0;
	for(auto & SectionCount : GPUUpdateData.ReservedIndexCounts)
	{
		if(SectionCount > 0)
		{
			GPUUpdateData.SectionsStartIndex.Add(ReservedIndicesNum);
			ReservedIndicesNum += SectionCount;
		}
	}

	uint32 IndirectArgOffset = 0;
	for(auto & DrawCal : GPUUpdateData.DrawCalls)
	{
		int32 Index = (*DrawCal.IndirectArgumentsOffset) / sizeof(FIndirectParameters);
		GPUUpdateData.SectionsStartIndex.Append(&GPUUpdateData.ReservedIndexCounts[Index], DrawCal.Count);
		IndirectArgOffset += DrawCal.Count * sizeof(FIndirectParameters);
	}
}

void FGPUDrawDataCuller::UpdateGPUResources_OLD(FRHICommandListImmediate& RHICmdList)
{
	if(GPUUpdateData.bNeedsIndirectBufferUpdate)
	{
		UpdateIndirectDrawList();

		const uint32 OptimalElementsNum = MaxGroupThreadsCount * FMath::DivideAndRoundUp(GPUUpdateData.TotalIndirectArguments, MaxGroupThreadsCount);
		const uint32 NewSize = OptimalElementsNum * sizeof(FIndirectParameters);
		if(NewSize > SceneCulledData.CulledIndirectDrawBuffer->GetSize())
		{
			FRHIResourceCreateInfo CreateInfo;
			FRHIResourceUpdateInfo UpdateInfo;
			UpdateInfo.Type = FRHIResourceUpdateInfo::UT_VertexBuffer;
			UpdateInfo.VertexBuffer.SrcBuffer = RHICreateVertexBuffer(NewSize, BUF_DrawIndirect|BUF_UnorderedAccess, CreateInfo);
			UpdateInfo.VertexBuffer.DestBuffer = SceneCulledData.CulledIndirectDrawBuffer;
			RHIUpdateRHIResources(&UpdateInfo, 1, true);
		}

		// tmpTODO: Get rid of this
		const uint32 Size = GPUUpdateData.IndirectDrawStartIndices.GetResourceDataSize();
		FRHIResourceCreateInfo CreateInfo(&GPUUpdateData.IndirectDrawStartIndices);
		FIndexBufferRHIRef ElementsIndexStart = RHICreateIndexBuffer(sizeof(uint32), Size, BUF_ShaderResource, CreateInfo);
		FShaderResourceViewRHIRef UpdateDataSRV = RHICreateShaderResourceView(ElementsIndexStart);

		const TShaderMapRef<FIndirectPassCS<true>> UpdateShader(GetGlobalShaderMap(GMaxRHIFeatureLevel));
		FRHIComputeShader * RHIUpdateShader = UpdateShader->GetComputeShader();

		RHICmdList.SetComputeShader( RHIUpdateShader );

		SetSRVParameter(RHICmdList, RHIUpdateShader, UpdateShader->ElementsIndexStart, UpdateDataSRV);
		SetUAVParameter(RHICmdList, RHIUpdateShader, UpdateShader->CulledIndirectArguments, SceneCulledData.CullingIndirectDrawBufferUAV);

		const uint32 ThreadsNeeded = GPUUpdateData.IndirectDrawStartIndices.Num();
		RHICmdList.DispatchComputeShader( FMath::DivideAndRoundUp(ThreadsNeeded, MaxGroupThreadsCount), 1, 1 );

		UnsetShaderUAVs(RHICmdList, &UpdateShader, RHIUpdateShader);

		RHICmdList.TransitionResource(EResourceTransitionAccess::ERWBarrier, EResourceTransitionPipeline::EComputeToCompute, SceneCulledData.CullingIndirectDrawBufferUAV);
	}

	// Resize Culled Index Buffer
	if(GPUUpdateData.TotalCullingIndicesNum > SceneCulledData.CulledIndexBuffer->GetSize() / SceneCulledData.CulledIndexBuffer->GetStride())
	{
		const uint32 NewStride = GPUUpdateData.TotalCullingIndicesNum > MAX_uint16 ? sizeof(uint32) : sizeof(uint16);
		const uint32 NewSize = GPUUpdateData.TotalCullingIndicesNum * NewStride;
		FRHIResourceCreateInfo CreateInfo;
		FRHIResourceUpdateInfo UpdateInfo;
		UpdateInfo.Type = FRHIResourceUpdateInfo::UT_IndexBuffer;
		UpdateInfo.IndexBuffer.SrcBuffer = RHICreateIndexBuffer(NewStride, NewSize, BUF_UnorderedAccess, CreateInfo);
		UpdateInfo.IndexBuffer.DestBuffer = SceneCulledData.CulledIndexBuffer;
		RHIUpdateRHIResources(&UpdateInfo, 1, true);
	}
}

void FGPUDrawDataCuller::CullRegisteredMeshes_OLD(FRHICommandListImmediate &RHICmdList,
												  const FMeshCommandOneFrameArray &VisibleMeshDrawCommands)
{
	FPerFrameCullingHandler::UpdateGPUResourcesIfNeeded(RHICmdList, VisibleMeshDrawCommands, CullingCommands);

	FPerFrameCullingHandler::DispatchCullingGraphAsync(RHICmdList, VisibleMeshDrawCommands, CullingCommands);


	//PrePass(RHICmdList, View);

	TArray<FRHIUnorderedAccessView*> PassUAVs;

	for (int PrimitiveIndex = 0; PrimitiveIndex < RegisteredForCulling.Num(); ++PrimitiveIndex)
	{
		FPrimitiveCullingData& RegisteredData = RegisteredForCulling[PrimitiveIndex];

		uint32 PrimitiveId = RegisteredData.PrimitiveSceneProxy->GetPrimitiveSceneInfo()->GetIndex();

		const int PrimitiveLODIndex = RegisteredData.PrimitiveSceneProxy->GetLOD(View);
		const int CulledLODIndex = RegisteredData.RegisteredLODsIndex[PrimitiveLODIndex];
		
		if (CulledLODIndex == INDEX_NONE)
		{
			continue;
		}

		for (int CommandIndex : PrimitiveLODCommands)
		{
			auto& Command = Commands[CommandIndex];
			check(Command.Shader);

			RHICmdList.SetComputeShader(Command.Shader);

			Command.MeshShaderBindings.SetOnCommandListForCompute(RHICmdList, Command.Shader);

			RHICmdList.DispatchComputeShader(Command.DispatchArguments[0], Command.DispatchArguments[2], Command.DispatchArguments[2]);
			
			RHICmdList.TransitionResource(EResourceTransitionAccess::ERWBarrier, EResourceTransitionPipeline::EComputeToGfx, Command.Resources->UAV);
			RHICmdList.TransitionResource(EResourceTransitionAccess::ERWBarrier, EResourceTransitionPipeline::EComputeToGfx, Command.Resources->CulledIndexBufferUAV);
		}
	}

}

#endif