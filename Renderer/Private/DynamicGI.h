#pragma once

#include "CustomRenderer/DynamicGIGlobals.h"
#include "ShaderParameterMacros.h"
#include "RenderGraphResources.h"

class FViewInfo;
class FProjectedShadowInfo;

namespace FDynamicGlobalIllumination
{
	BEGIN_SHADER_PARAMETER_STRUCT(FRSMParameters, )
		SHADER_PARAMETER_STRUCT_REF(FSceneTexturesUniformParameters,	SceneTextures)
		SHADER_PARAMETER_STRUCT_REF(FViewUniformShaderParameters,		ViewUniformBuffer)
		SHADER_PARAMETER_RDG_TEXTURE(Texture2D<float3>,					RSMNormalTexture)
		SHADER_PARAMETER_RDG_TEXTURE(Texture2D<float3>,					RSMBaseColorTexture)
		SHADER_PARAMETER_RDG_TEXTURE(Texture2D<float3>,					RSMSpecularTexture)
		SHADER_PARAMETER_RDG_TEXTURE(Texture2D<float>,					RSMDepthTexture)
		SHADER_PARAMETER(FMatrix,	RSMToWorld)
		SHADER_PARAMETER(uint32,	RSMSize)
	END_SHADER_PARAMETER_STRUCT()

	BEGIN_SHADER_PARAMETER_STRUCT(FLightParameters, )
		SHADER_PARAMETER(FVector,	LightPosition)
		SHADER_PARAMETER(float,		LightInvRadius)
		SHADER_PARAMETER(FVector,	LightColor)
		SHADER_PARAMETER(float,		LightIntensity)
		SHADER_PARAMETER(FVector,	LightDirection)
		SHADER_PARAMETER(FVector2D, LightSpotAngles)
	END_SHADER_PARAMETER_STRUCT()

	void SetupRSMParameters(
		FRDGBuilder& GraphBuilder,
		const FViewInfo& View, 
		const FProjectedShadowInfo& ProjectedShadowInfo,
		FRSMParameters &OutParameters);

	void SetupLightParameters(const FLightSceneProxy* LightSceneProxy, FLightParameters& OutParameters);
}