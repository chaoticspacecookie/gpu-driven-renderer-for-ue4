#include "VSGL.h"
#include "DynamicGI.h"
#include "HAL/IConsoleManager.h"
#include "GlobalShader.h"
#include "ShaderParameterStruct.h"
#include "SceneRendering.h"
#include "SceneRenderTargets.h"
#include "ShadowRendering.h"
#include "ScenePrivate.h"

static TAutoConsoleVariable<int32> CVarVSGLSupported(
	TEXT("r.VSGL.Supported"),
	0,
	TEXT("Whether to compile VSGL shaders (cannot be changed at runtime)."),
	ECVF_ReadOnly
);

// Currently limited to 32 to fit in a single threadgroup of 1024 threads, larger RSM would need multiple passes to sum the texture.
static const int32 MaxRSMResolution = 32;

bool FVirtualSphericalGaussianLight::IsSupported()
{
	return false;//Romain temp disabled to fix steam
	return CVarVSGLSupported.GetValueOnAnyThread() != 0;
}

class FVSGLLightTypeDim : SHADER_PERMUTATION_BOOL("LIGHT_TYPE");

class FVSGLGenerateCS : public FGlobalShader
{
	DECLARE_GLOBAL_SHADER(FVSGLGenerateCS)
	SHADER_USE_PARAMETER_STRUCT(FVSGLGenerateCS, FGlobalShader)

	BEGIN_SHADER_PARAMETER_STRUCT(FParameters, )
		SHADER_PARAMETER_STRUCT_INCLUDE(FDynamicGlobalIllumination::FRSMParameters,		RSMParameters)
		SHADER_PARAMETER_STRUCT_INCLUDE(FDynamicGlobalIllumination::FLightParameters,	LightParameters)
		SHADER_PARAMETER_RDG_BUFFER_UAV(RWStructuredBuffer<FSphericalGaussianLight>,	RWSGLights)
	END_SHADER_PARAMETER_STRUCT()
	
	using FPermutationDomain = TShaderPermutationDomain<FVSGLLightTypeDim>;

	static bool ShouldCompilePermutation(const FGlobalShaderPermutationParameters& Parameters)
	{
		return IsFeatureLevelSupported(Parameters.Platform, ERHIFeatureLevel::SM5) && FVirtualSphericalGaussianLight::IsSupported();
	}

	static FIntPoint GetThreadGroupSize()
	{
		return FIntPoint(MaxRSMResolution, MaxRSMResolution);
	}

	static void ModifyCompilationEnvironment(const FGlobalShaderPermutationParameters& Parameters, FShaderCompilerEnvironment& OutEnvironment)
	{
		FGlobalShader::ModifyCompilationEnvironment(Parameters, OutEnvironment);
		OutEnvironment.SetDefine(TEXT("THREADGROUP_SIZE_X"), GetThreadGroupSize().X);
		OutEnvironment.SetDefine(TEXT("THREADGROUP_SIZE_Y"), GetThreadGroupSize().Y);
	}
};

IMPLEMENT_GLOBAL_SHADER(FVSGLGenerateCS, "/Engine/Private/CustomRenderer/VSGLGenerate.usf", "GenerateCS", SF_Compute);

class FVSGLApplyCS : public FGlobalShader
{
	DECLARE_GLOBAL_SHADER(FVSGLApplyCS)
	SHADER_USE_PARAMETER_STRUCT(FVSGLApplyCS, FGlobalShader)

	BEGIN_SHADER_PARAMETER_STRUCT(FParameters, )
		SHADER_PARAMETER_STRUCT_REF(FViewUniformShaderParameters,					ViewUniformBuffer)
		SHADER_PARAMETER_STRUCT_REF(FSceneTexturesUniformParameters,				SceneTextures)
		SHADER_PARAMETER_RDG_BUFFER_SRV(StructuredBuffer<FSphericalGaussianLight>,	SGLights)
		SHADER_PARAMETER_RDG_TEXTURE_UAV(RWTexture2D<float3>,						RWSceneColorTexture)
	END_SHADER_PARAMETER_STRUCT()

	static bool ShouldCompilePermutation(const FGlobalShaderPermutationParameters& Parameters)
	{
		return IsFeatureLevelSupported(Parameters.Platform, ERHIFeatureLevel::SM5) && FVirtualSphericalGaussianLight::IsSupported();
	}

	static FIntPoint GetThreadGroupSize()
	{
		return FIntPoint(FComputeShaderUtils::kGolden2DGroupSize, FComputeShaderUtils::kGolden2DGroupSize);
	}

	static void ModifyCompilationEnvironment(const FGlobalShaderPermutationParameters& Parameters, FShaderCompilerEnvironment& OutEnvironment)
	{
		FGlobalShader::ModifyCompilationEnvironment(Parameters, OutEnvironment);
		OutEnvironment.SetDefine(TEXT("THREADGROUP_SIZE_X"), GetThreadGroupSize().X);
		OutEnvironment.SetDefine(TEXT("THREADGROUP_SIZE_Y"), GetThreadGroupSize().Y);
	}
};

IMPLEMENT_GLOBAL_SHADER(FVSGLApplyCS, "/Engine/Private/CustomRenderer/VSGLApply.usf", "ApplyCS", SF_Compute);

void FVirtualSphericalGaussianLight::GenerateVSGL(FRDGBuilder& GraphBuilder, const FViewInfo& View, FRDGBufferRef SGLights, ELightType LightType, const FProjectedShadowInfo* ProjectedShadowInfo)
{
	FVSGLGenerateCS::FPermutationDomain PermutationVector;
	PermutationVector.Set<FVSGLLightTypeDim>(static_cast<bool>(LightType));

	TShaderMapRef<FVSGLGenerateCS> ComputeShader(View.ShaderMap, PermutationVector);

	FVSGLGenerateCS::FParameters* Parameters = GraphBuilder.AllocParameters<FVSGLGenerateCS::FParameters>();

	FDynamicGlobalIllumination::SetupRSMParameters(GraphBuilder, View, *ProjectedShadowInfo, Parameters->RSMParameters);
	FDynamicGlobalIllumination::SetupLightParameters(ProjectedShadowInfo->GetLightSceneInfo().Proxy, Parameters->LightParameters);	

	Parameters->RWSGLights = GraphBuilder.CreateUAV(SGLights);

	FComputeShaderUtils::AddPass(
		GraphBuilder,
		RDG_EVENT_NAME("VSGL Generate %s", LightType == ELightType::Diffuse ? TEXT("Diffuse") : TEXT("Specular")),
		ComputeShader,
		Parameters,
		FIntVector(1, 1, 1));
}


void FVirtualSphericalGaussianLight::ApplyVSGL(FRDGBuilder& GraphBuilder, const FViewInfo& View, FRDGBufferRef SGLights)
{
	TShaderMapRef<FVSGLApplyCS> ComputeShader(View.ShaderMap);

	FVSGLApplyCS::FParameters* Parameters = GraphBuilder.AllocParameters<FVSGLApplyCS::FParameters>();

	Parameters->ViewUniformBuffer	= View.ViewUniformBuffer;
	Parameters->SceneTextures		= CreateSceneTextureUniformBufferSingleDraw(GraphBuilder.RHICmdList, ESceneTextureSetupMode::All, View.FeatureLevel);
	Parameters->SGLights			= GraphBuilder.CreateSRV(SGLights);

	FSceneRenderTargets& SceneContext				= FSceneRenderTargets::Get(GraphBuilder.RHICmdList);
	TRefCountPtr<IPooledRenderTarget> SceneColorRT	= SceneContext.GetSceneColor();
	FRDGTextureRef SceneColorTexture				= GraphBuilder.RegisterExternalTexture(SceneColorRT, TEXT("SceneColorTexture"));
	Parameters->RWSceneColorTexture					= GraphBuilder.CreateUAV(SceneColorTexture);

	FComputeShaderUtils::AddPass(
		GraphBuilder,
		RDG_EVENT_NAME("VSGL Apply"),
		ComputeShader,
		Parameters,
		FComputeShaderUtils::GetGroupCount(View.ViewRect.Size(), FVSGLApplyCS::GetThreadGroupSize())
	);
}

void FVirtualSphericalGaussianLight::RenderLights(FRDGBuilder& GraphBuilder, const FViewInfo& View, const FSortedShadowMaps& ShadowMaps)
{
	return;//Romain disabled because of steam crash
	if (ShadowMaps.RSMAtlases.Num() >= 1)
	{
		for (int i = 0; i < ShadowMaps.RSMAtlases[0].Shadows.Num(); i++)
		{
			FRDGBufferDesc BufferDesc = FRDGBufferDesc::CreateStructuredDesc(sizeof(FVector4) * 3, static_cast<uint32>(ELightType::Num));
			FRDGBufferRef SGLights = GraphBuilder.CreateBuffer(BufferDesc, TEXT("SGLights"));

			GenerateVSGL(GraphBuilder, View, SGLights, ELightType::Diffuse, ShadowMaps.RSMAtlases[0].Shadows[i]);
			GenerateVSGL(GraphBuilder, View, SGLights, ELightType::Specular, ShadowMaps.RSMAtlases[0].Shadows[i]);

			if (View.Family->EngineShowFlags.VisualizeVPL)
			{
				FSceneViewState* SceneViewState = static_cast<FSceneViewState*>(View.State);
				GraphBuilder.QueueBufferExtraction(SGLights, &SceneViewState->SGLights, FRDGResourceState::EAccess::Read, FRDGResourceState::EPipeline::Graphics);
			}
			
			ApplyVSGL(GraphBuilder, View, SGLights);
		}
	}
}

int32 FVirtualSphericalGaussianLight::GetMaxRSMResolution()
{
	return MaxRSMResolution;
}
