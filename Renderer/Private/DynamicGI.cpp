#include "DynamicGI.h"
#include "DeferredShadingRenderer.h"
#include "VPLGI.h"
#include "VSGL.h"
#include "ProfilingDebugging/RealtimeGPUProfiler.h"
#include "Postprocess/SceneRenderTargets.h"
#include "ShadowRendering.h"
#include "ScenePrivate.h"
#include "SceneTextureParameters.h"

DECLARE_GPU_STAT(DynamicGI);


void FDeferredShadingSceneRenderer::ApplyVSGLRaytraced(FRDGBuilder& GraphBuilder, FViewInfo& View, FRDGBufferRef SGLights)
{
#if RHI_RAYTRACING
	TRefCountPtr<IPooledRenderTarget> OutTexture;

	bool bBuffersCreated = false;

	if (View.RayTracingScene.RayTracingSceneRHI)
	{
		FSceneTextureParameters SceneTextures;
		SetupSceneTextureParameters(GraphBuilder, &SceneTextures);
		FSceneRenderTargets& SceneContext = FSceneRenderTargets::Get(GraphBuilder.RHICmdList);
		TRefCountPtr<IPooledRenderTarget> SceneColorRT = SceneContext.GetSceneColor();

		FRDGTextureDesc Desc = SceneContext.GetSceneColor()->GetDesc();
		Desc.Format = PF_FloatRGBA;
		Desc.Flags &= ~(TexCreate_FastVRAM | TexCreate_Transient);
		FRDGTextureRef OutputData = GraphBuilder.CreateTexture(Desc, TEXT("OutputData"));

		FRDGTextureRef SceneColorTexture = GraphBuilder.RegisterExternalTexture(SceneColorRT, TEXT("SceneColorTexture"));
		auto* SceneViewState = static_cast<FSceneViewState*>(View.State);

		RenderRayTracingVSGL(GraphBuilder, SceneTextures, View, SGLights, &OutputData);
		CompositeRayTracingVSGL(GraphBuilder, OutputData);
		//DenoiseRayTracingVPL(RHICmdList, GraphBuilder, View, OutTexture, RayColor, RayHitDistance);
	}

	if (OutTexture)
	{
		//CompositeRayTracingVPL(RHICmdList, OutTexture);
	}
#endif
}

void FDeferredShadingSceneRenderer::RenderDynamicGlobalIllumination(FRHICommandListImmediate& RHICmdList)
{
	static bool bDIsableGIForTesting = true;
	if (bDIsableGIForTesting)
		return;
	SCOPED_GPU_STAT(RHICmdList, DynamicGI);

	switch (FDynamicGlobalIllumination::GetType())
	{
	case EDynamicGlobalIlluminationType::VPLRasterized:
	{
		FRDGBuilder GraphBuilder(RHICmdList);

		for (FViewInfo& View : Views)
		{
			FVirtualPointLight::RenderLightsRasterized(GraphBuilder, View, SortedShadowsForShadowDepthPass);
		}

		GraphBuilder.Execute();
		break;
	}
#if RHI_RAYTRACING
	case EDynamicGlobalIlluminationType::VPLRaytraced:
	{
		TRefCountPtr<IPooledRenderTarget> OutTexture;

		FRDGTextureRef RayColor;
		FRDGTextureRef RayHitDistance;

		bool bBuffersCreated = false;

		for (FViewInfo& View : Views)
		{
			if (View.RayTracingScene.RayTracingSceneRHI)
			{
				const TArray<FSortedShadowMapAtlas, SceneRenderingAllocator>& RSMAtlases = SortedShadowsForShadowDepthPass.RSMAtlases;
				FRDGBuilder GraphBuilder(RHICmdList);

				//ResolveSceneColor(RHICmdList);
				FSceneRenderTargets& SceneContext = FSceneRenderTargets::Get(RHICmdList);
				if (bBuffersCreated == false)
				{
					bBuffersCreated = true;
					FRDGTextureDesc Desc = SceneContext.GetSceneColor()->GetDesc();
					Desc.Format = PF_FloatRGBA;
					Desc.Flags &= ~(TexCreate_FastVRAM | TexCreate_Transient);
					RayColor = GraphBuilder.CreateTexture(Desc, TEXT("RayTracingVPLHit"));

					FRDGTextureDesc Desc2 = SceneContext.GetSceneColor()->GetDesc();
					Desc2.Format = PF_G16R16;
					Desc2.Flags &= ~(TexCreate_FastVRAM | TexCreate_Transient);
					RayHitDistance = GraphBuilder.CreateTexture(Desc2, TEXT("RayTracingVPLHitDistance"));

					InitRayTracingVPL(RHICmdList, GraphBuilder, View, OutTexture, RayColor, RayHitDistance);
				}

				FRDGTextureDesc Desc = SceneContext.GetSceneColor()->GetDesc();
				Desc.Format = PF_FloatRGBA;
				Desc.Flags &= ~(TexCreate_FastVRAM | TexCreate_Transient);
				FRDGTextureRef OutputData = GraphBuilder.CreateTexture(Desc, TEXT("OutputData"));

				// Create VPLs
				for (const auto& RSMAtlas : RSMAtlases)
				{
					for (const auto* ProjectedShadowInfo : RSMAtlas.Shadows)
					{
						INC_DWORD_STAT(STAT_NumReflectiveShadowMapLights);
						const FLightSceneInfo& LightSceneInfo = ProjectedShadowInfo->GetLightSceneInfo();
						check(LightSceneInfo.Proxy->AffectsDynamicIndirectLighting())

							//UpdateVPLList(GraphBuilder, ProjectedShadowInfo, View);
					}
				}

				// Copy SceneColor
				FRDGTextureUAVRef SceneColorCopyTextureUAV = FVirtualPointLight::CopySceneColor(GraphBuilder, View);

				// Render VPLs
				auto* SceneViewState = static_cast<FSceneViewState*>(View.State);

				RenderRayTracingVPL(RHICmdList, GraphBuilder, View, OutTexture, RayColor, RayHitDistance);
				DenoiseRayTracingVPL(RHICmdList, GraphBuilder, View, OutTexture, RayColor, RayHitDistance);
				if (OutTexture)
				{
					CompositeRayTracingVPL(GraphBuilder, OutTexture);
				}

				GraphBuilder.Execute();
			}
		}

		break;
	}
#endif
	case EDynamicGlobalIlluminationType::VSGL:
	{
		FRDGBuilder GraphBuilder(RHICmdList);

		for (FViewInfo& View : Views)
		{
			FVirtualSphericalGaussianLight::RenderLights(GraphBuilder, View, SortedShadowsForShadowDepthPass);
		}

		GraphBuilder.Execute();
		break;
	}
#if RHI_RAYTRACING
	case EDynamicGlobalIlluminationType::VSGLRaytraced:
	{
		FRDGBuilder GraphBuilder(RHICmdList);
		for (FViewInfo& View : Views)
		{

			if (SortedShadowsForShadowDepthPass.RSMAtlases.Num() >= 1)
			{
				for (int i = 0; i < SortedShadowsForShadowDepthPass.RSMAtlases[0].Shadows.Num(); i++)
				{
					//Render VPL Lights
					FVirtualSphericalGaussianLight::RenderLights(GraphBuilder, View, SortedShadowsForShadowDepthPass);
					FRDGBufferDesc BufferDesc = FRDGBufferDesc::CreateStructuredDesc(sizeof(FVector4) * 3, static_cast<uint32>(ELightType::Num));
					FRDGBufferRef SGLights = GraphBuilder.CreateBuffer(BufferDesc, TEXT("SGLights"));

					FVirtualSphericalGaussianLight::GenerateVSGL(GraphBuilder, View, SGLights, ELightType::Diffuse, SortedShadowsForShadowDepthPass.RSMAtlases[0].Shadows[i]);
					FVirtualSphericalGaussianLight::GenerateVSGL(GraphBuilder, View, SGLights, ELightType::Specular, SortedShadowsForShadowDepthPass.RSMAtlases[0].Shadows[i]);
					FVirtualSphericalGaussianLight::ApplyVSGL(GraphBuilder, View, SGLights);

					if (View.Family->EngineShowFlags.VisualizeVPL)
					{
						FSceneViewState* SceneViewState = static_cast<FSceneViewState*>(View.State);
						GraphBuilder.QueueBufferExtraction(SGLights, &SceneViewState->SGLights, FRDGResourceState::EAccess::Read, FRDGResourceState::EPipeline::Graphics);
					}

					//Compute ray traced occlusion for VSGLs
					ApplyVSGLRaytraced(GraphBuilder, View, SGLights);
				}
			}
		}
		GraphBuilder.Execute();
		break;
	}
#endif
	}
}

void FDynamicGlobalIllumination::SetupRSMParameters(FRDGBuilder& GraphBuilder, const FViewInfo& View, const FProjectedShadowInfo& ProjectedShadowInfo, FRSMParameters& OutParameters)
{
	check(ProjectedShadowInfo.RenderTargets.ColorTargets[0]);
	check(ProjectedShadowInfo.RenderTargets.ColorTargets[1]);
	check(ProjectedShadowInfo.RenderTargets.ColorTargets[2]);

	const auto* SceneViewState = static_cast<FSceneViewState*>(View.State);

	FRDGTextureRef RSMNormalTexture				= GraphBuilder.RegisterExternalTexture(ProjectedShadowInfo.RenderTargets.ColorTargets[0], TEXT("RSMNormalTexture"));
	FRDGTextureRef RSMBaseColorTexture			= GraphBuilder.RegisterExternalTexture(ProjectedShadowInfo.RenderTargets.ColorTargets[1], TEXT("RSMBaseColorTexture"));
	FRDGTextureRef RSMSpecularTexture			= GraphBuilder.RegisterExternalTexture(ProjectedShadowInfo.RenderTargets.ColorTargets[2], TEXT("RSMSpecularTexture"));
	FRDGTextureRef RSMDepthTexture				= GraphBuilder.RegisterExternalTexture(ProjectedShadowInfo.RenderTargets.DepthTarget, TEXT("RSMDepthTexture"));

	OutParameters.RSMNormalTexture				= RSMNormalTexture;
	OutParameters.RSMBaseColorTexture			= RSMBaseColorTexture;
	OutParameters.RSMSpecularTexture			= RSMSpecularTexture;
	OutParameters.RSMDepthTexture				= RSMDepthTexture;

	const FLightSceneProxy* LightSceneProxy		= ProjectedShadowInfo.GetLightSceneInfo().Proxy;

	const FMatrix WorldToShadow = FTranslationMatrix(ProjectedShadowInfo.PreShadowTranslation) * ProjectedShadowInfo.SubjectAndReceiverMatrix;
	OutParameters.RSMToWorld = WorldToShadow.Inverse();
	OutParameters.RSMSize						= ProjectedShadowInfo.ResolutionX;
	OutParameters.ViewUniformBuffer				= View.ViewUniformBuffer;
	OutParameters.SceneTextures					= CreateSceneTextureUniformBufferSingleDraw(GraphBuilder.RHICmdList, ESceneTextureSetupMode::All, View.FeatureLevel);
}

void FDynamicGlobalIllumination::SetupLightParameters(const FLightSceneProxy* LightSceneProxy, FDynamicGlobalIllumination::FLightParameters& OutParameters)
{
	FLightShaderParameters LightShaderParameters;
	LightSceneProxy->GetLightShaderParameters(LightShaderParameters);

	OutParameters.LightPosition = LightShaderParameters.Position;
	OutParameters.LightInvRadius = LightShaderParameters.InvRadius;
	OutParameters.LightDirection = LightShaderParameters.Direction;
	OutParameters.LightSpotAngles = LightShaderParameters.SpotAngles;
	OutParameters.LightColor = LightShaderParameters.Color;
	OutParameters.LightIntensity = LightSceneProxy->GetIndirectLightingScale();
}