// This file provides precompiler macros and CVar definitions for culling

#pragma once

#define ENABLE_VF_INPUT_DEDUCTION 1
#define ENABLE_PER_SECTION_CULLING_FLAGS 1
