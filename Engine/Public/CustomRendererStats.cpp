
#include "CustomRendererStats.h"
#include "Logging/LogMacros.h"


DEFINE_LOG_CATEGORY(LogCustomRendererStats);

TCustomRendererTrafficLightScopedCPU::TCustomRendererTrafficLightScopedCPU(FName InName)
	: Name(InName)
{
	StartCycles = FPlatformTime::Cycles();
}

TCustomRendererTrafficLightScopedCPU::~TCustomRendererTrafficLightScopedCPU()
{
	double Result = FPlatformTime::ToMilliseconds(FPlatformTime::Cycles() - StartCycles);

	UE_LOG(LogCustomRendererStats, Verbose, TEXT("Traffic Light %s: %.6f"), *(Name.GetPlainNameString()), float(Result));

	FCustomRendererDiagnosticsCore::RecordSample(Name, float(Result));
}
