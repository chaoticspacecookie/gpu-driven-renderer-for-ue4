
#include "CustomRenderer/CollisionMapVolume.h"
#include "Engine/CollisionProfile.h"
#include "Engine/World.h"
#include "Components/BrushComponent.h"

ACollisionMapVolume::ACollisionMapVolume(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	GetBrushComponent()->SetCollisionProfileName(UCollisionProfile::NoCollision_ProfileName);
	GetBrushComponent()->bAlwaysCreatePhysicsState = true;

	if (GetWorld() && GetWorld()->Scene)
	{
		GetWorld()->Scene->RemoveCollisionMapVolume(this);
		GetWorld()->Scene->AddCollisionMapVolume(this);
	}

	bEnabled = true;
}

void ACollisionMapVolume::BeginPlay()
{
	Super::BeginPlay();
	
	if (GetWorld() && GetWorld()->Scene)
	{
		GetWorld()->Scene->RemoveCollisionMapVolume(this);
		GetWorld()->Scene->AddCollisionMapVolume(this);
	}
}

void ACollisionMapVolume::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	if (GetWorld() && GetWorld()->Scene)
	{
		GetWorld()->Scene->RemoveCollisionMapVolume(this);
		GetWorld()->Scene->AddCollisionMapVolume(this);
	}

}

#if WITH_EDITOR
void ACollisionMapVolume::Destroyed()
{
	Super::Destroyed();

	UWorld* World = GetWorld();
}

void ACollisionMapVolume::PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent)
{
	Super::PostEditChangeProperty(PropertyChangedEvent);

	if (GetWorld() && GetWorld()->Scene)
	{
		GetWorld()->Scene->RemoveCollisionMapVolume(this);
		GetWorld()->Scene->AddCollisionMapVolume(this);
	}

}

void ACollisionMapVolume::PostEditMove(bool bFinished)
{
	Super::PostEditMove(bFinished);
	if (bFinished)
	{
		GetWorld()->Scene->RemoveCollisionMapVolume(this);
		GetWorld()->Scene->AddCollisionMapVolume(this);
	}
}
#endif // WITH_EDITOR
