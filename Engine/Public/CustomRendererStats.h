#pragma once

#include "Engine/Engine.h"

ENGINE_API DECLARE_LOG_CATEGORY_EXTERN(LogCustomRendererStats, Log, All);

class ENGINE_API TCustomRendererTrafficLightScopedCPU
{
public:

	TCustomRendererTrafficLightScopedCPU(FName InName);

	~TCustomRendererTrafficLightScopedCPU();

private:

	uint32 StartCycles;
	FName Name;
};

// Default CPU Traffic light, accumulates
#define CPU_TRAFFIC_LIGHT( InName) static FName PREPROCESSOR_JOIN(_CUSTOM_RENDERER_Scoped_Name_, __LINE__)(InName); TCustomRendererTrafficLightScopedCPU PREPROCESSOR_JOIN(_CUSTOM_RENDERER_Scoped_, __LINE__)(PREPROCESSOR_JOIN(_CUSTOM_RENDERER_Scoped_Name_, __LINE__))


