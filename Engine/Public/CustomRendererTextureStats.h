#pragma once

#include "CoreMinimal.h"
#include "CustomRenderer/CustomRendererCoreDefines.h"
#include "Misc/ScopeLock.h"
#include "HAL/CriticalSection.h"

struct FTextureBestMips
{
	FString TextureName;
	int32 TextureSizeX;
	int32 TextureSizeY;
	int32 MipCount = 0;
	int32 BestVisibleMip = 0;
	int32 BestHiddenMip = 0;
};

class ENGINE_API FBestTextureMipCalculator
{
public:
	using TextureMipMap = TMap <FString, FTextureBestMips>;

	FBestTextureMipCalculator():
		CurrentLevel(TEXT(""))
	{
		
	}

	static inline FBestTextureMipCalculator& Get()
	{
		return SingletonInstance;
	}

	inline void AddTextureMip(const FString& InTexturePathName, const int32 InTextureSizeX, const int32 InTextureSizeY, const int32 InMipCount, const int32 InBestVisibleMip, const int32 InBestHiddenMip)
	{
		FScopeLock Lock(&MapAccessCS);

		FTextureBestMips NewElement;
		NewElement.TextureName = InTexturePathName;
		NewElement.TextureSizeX = InTextureSizeX;
		NewElement.TextureSizeY = InTextureSizeY;
		NewElement.MipCount = InMipCount;
		NewElement.BestVisibleMip = InBestVisibleMip;
		NewElement.BestHiddenMip = InBestHiddenMip;

		if (AllMips.Contains(NewElement.TextureName))
		{
			if (NewElement.BestVisibleMip > AllMips[NewElement.TextureName].BestVisibleMip)
			{
				AllMips[NewElement.TextureName].BestVisibleMip = NewElement.BestVisibleMip;
			}
			if (NewElement.BestHiddenMip > AllMips[NewElement.TextureName].BestHiddenMip)
			{
				AllMips[NewElement.TextureName].BestHiddenMip = NewElement.BestHiddenMip;
			}
		}
		else
		{
			AllMips.Add(NewElement.TextureName, NewElement);
		}
	}

	inline void Empty()
	{
		FScopeLock Lock(&MapAccessCS);

		AllMips.Empty();
	}

	inline const TextureMipMap& GetData() const
	{
		return AllMips;
	}

	inline const FString& GetCurrentLevel() const
	{
		return CurrentLevel;
	}

	inline void SetCurrentLevel(const FString& InLevelName)
	{
		CurrentLevel = InLevelName;
	}

	FCriticalSection &GetMapAccessCS() { return MapAccessCS; }

private:
	TextureMipMap AllMips;
	FString CurrentLevel;

	FCriticalSection MapAccessCS;
	static FBestTextureMipCalculator SingletonInstance;
};