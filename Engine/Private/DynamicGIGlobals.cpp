#include "CustomRenderer/DynamicGIGlobals.h"
#include "HAL/IConsoleManager.h"

int32 GDynamicGIType = 3;

FAutoConsoleVariableRef CVarDynamicGIType(
	TEXT("r.DynamicGIType"),
	GDynamicGIType,
	TEXT("Dynamic GI Type")
	TEXT("0: off")
	TEXT("1: VPLGI Rasterized (Requires r.VPLGI.Supported to be enabled)")
	TEXT("2: VPLGI Raytraced (Requires r.VPLGI.Supported to be enabled & raytracing)")
	TEXT("3: VSGL (Requires r.VSGL.Supported to be enabled)")
	TEXT("4: VSGL Raytraced (Requires r.VSGL.Supported to be enabled & raytracing)"),
	ECVF_Scalability | ECVF_RenderThreadSafe
);

EDynamicGlobalIlluminationType FDynamicGlobalIllumination::GetType()
{
	return static_cast<EDynamicGlobalIlluminationType>(GDynamicGIType);
}